package com.bottin.classboard.models;

import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.BreakEvent;
import com.bottin.classboard.models.events.Event;
import com.bottin.classboard.models.events.Exercise;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.bottin.classboard.models.Feedback.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Lesson.java
 * Description : Model for lessons
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String subject;

    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdDate;

    private LocalDateTime endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Teacher teacher;

    // Students access
    private String entryEndpoint;
    private String entryCode;
    private boolean open;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="lesson", orphanRemoval = true)
    private List<Question> questions;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="lesson", orphanRemoval = true)
    private List<Student> students;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="lesson", orphanRemoval = true)
    private List<Feedback> feedbacks;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="lesson", orphanRemoval = true)
    private List<Event> events;

    public Lesson(Teacher teacher, String subject, LocalDateTime endDate) {
        this.students = new ArrayList<>();
        this.questions = new ArrayList<>();
        this.feedbacks = new ArrayList<>();
        this.events = new ArrayList<>();

        this.subject = subject;
        this.teacher = teacher;
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lesson)) return false;
        return this.id != null && this.id.equals(((Lesson) o).getId());
    }

    public String getDirectLink() {
        return "join/" + entryEndpoint + "?code=" + entryCode;
    }

    public String getTimeFrame() {

        int dom = createdDate.getDayOfMonth();
        String dayAndMonth = "" + dom;
        if (dom == 1 || dom == 21 || dom == 31) {
            dayAndMonth += "st of ";
        } else if (dom == 2 || dom == 22) {
            dayAndMonth += "nd of ";
        } else if (dom == 3 || dom == 23) {
            dayAndMonth += "rd of ";
        } else {
            dayAndMonth += "th of ";
        }
        dayAndMonth += createdDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault());
        String hour = createdDate.getHour() < 10 ?
                "0" + createdDate.getHour() :
                ""  + createdDate.getHour();
        String minute = createdDate.getMinute() < 10 ?
                "0" + createdDate.getMinute() :
                ""  + createdDate.getMinute();

        return createdDate.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.getDefault())
                + ", the " + dayAndMonth + ", " + hour + ":" + minute;
    }

    /*public int[] getFeedbackScores() {
        int[] scores = {0, 0, 0, 0};

        if(students.isEmpty()) {
            Arrays.fill(scores, START_FEEDBACK);
        }

        // For each student we compute their feedback score
        for(Student student : students) {
            int[] studentScores = {START_FEEDBACK, START_FEEDBACK,
                    START_FEEDBACK, START_FEEDBACK};

            for(Feedback feedback : student.getFeedbacks()) {
                switch(feedback.getType()) {
                    case MOTIVATION:
                        studentScores[0] += feedback.isPositive() ? POS_FEEDBACK : NEG_FEEDBACK;
                        break;
                    case VITALITY:
                        studentScores[1] += feedback.isPositive() ? POS_FEEDBACK : NEG_FEEDBACK;
                        break;
                    case CLARITY:
                        studentScores[2] += feedback.isPositive() ? POS_FEEDBACK : NEG_FEEDBACK;
                        break;
                    case INTERACTIVITY:
                        studentScores[3] += feedback.isPositive() ? POS_FEEDBACK : NEG_FEEDBACK;
                        break;
                }
            }

            // Compute overall score
            for(int i = 0; i < scores.length; i++) {
                if(studentScores[i] > MAX_FEEDBACK_VALUE) {
                    scores[i] += MAX_FEEDBACK_VALUE / students.size();
                } else if (studentScores[i] > MIN_FEEDBACK_VALUE) {
                    scores[i] += studentScores[i] / students.size();
                }
            }
        }

        return scores;
    }*/

    public Event getOngoingEvent() {
        for(Event event : events) {
            if (event.isOnGoing())
                return event;
        }

        return null;
    }

    @Override
    public String toString() {
        return "Lesson" + id;
    }
}

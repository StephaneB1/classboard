package com.bottin.classboard.controllers;

import com.bottin.classboard.utils.Utils;
import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.services.interfaces.LessonService;
import com.bottin.classboard.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonEntryController.java
 * Description : Controller to handle arrival of new students when they
 *               first "login" with the lesson's code
 */
@Controller
@SessionAttributes("student_id")
public class LessonEntryController {

    private final LessonService lessonService;
    private final StudentService studentService;
    private final AuthenticationProvider authProvider;
    private final PasswordEncoder encoder;

    @Lazy
    AuthenticationManager authManager;

    @Autowired
    public LessonEntryController(AuthenticationManager authManager, PasswordEncoder encoder, AuthenticationProvider authProvider,
                                 LessonService lessonService, StudentService studentService) {
        this.authManager = authManager;
        this.encoder = encoder;
        this.authProvider = authProvider;
        this.lessonService = lessonService;
        this.studentService = studentService;
    }

    @GetMapping("/join/{lesson_endpoint}")
    public String joinLessonForm(@PathVariable String lesson_endpoint,
                                 @RequestParam(required = false) String code,
                                 Model model) {

        // Direct access
        try {
            if(!code.isEmpty()) {
                Student student = login(lesson_endpoint, code);

                // If student is valid, open welcome page
                if(student != null) {
                    model.addAttribute("student_id", student.getId());
                    return "redirect:/welcome/" + lesson_endpoint;
                }
            }
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }

        model.addAttribute("endpoint", lesson_endpoint);

        return "join-lesson";
    }

    @GetMapping("/welcome/{lesson_endpoint}")
    public String welcomeStudent(@PathVariable String lesson_endpoint,
                                 Model model) {

        try {
            // Verify the student actually exist before redirecting
            String id = (String) model.getAttribute("student_id");
            Student student = studentService.findById(id);
            model.addAttribute("student", student);
            return "welcome";
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }

        return "error";
    }

    @PostMapping("/join/{lesson_endpoint}")
    public String joinLesson(@RequestParam String code,
                             @PathVariable String lesson_endpoint,
                             Model model) {

        try {
            Student student = login(lesson_endpoint, code);

            if(student != null) {
                model.addAttribute("student_id", student.getId());
                return "redirect:/student/";
            }
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }

        return "error";
    }

    private Student login(String endpoint, String code) throws Exception {

        Lesson lesson = lessonService.findByEndpoint(endpoint);

        if(!lesson.isOpen()) {
            throw new Exception("The lesson is closed, to enter please contact the teacher to open it.");
        }

        if(!code.equals(lesson.getEntryCode())) {
            throw new Exception("Wrong code");
        }

        String tmpPassword = Utils.getRandomString(10);

        Student student = studentService.save(new Student(lesson,
                Utils.getRandomAdjective(),
                Utils.getRandomAnimal(),
                encoder.encode(tmpPassword)));

        // If the number of students is even, then we assign the arriving
        // student a new neighbour. Otherwise the student will sit alone
        // until another student joins the class
        if(lesson.getStudents().size() % 2 == 0) {
            for (Student s : lesson.getStudents())
                if (s.getNeighbour() == null) {
                    s.setNeighbour(student);
                    student.setNeighbour(s);
                    studentService.update(s);
                    studentService.update(student);
                }
        }

        // Creating a guest session for the student (auto-login)
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(student.getId(), tmpPassword);
        Authentication authentication = authManager.authenticate(token);
        if (authentication.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return student;
        }

        return null;
    }
}

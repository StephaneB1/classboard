package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonService.java
 * Description : Service interface for lessons
 */
@Service
public interface LessonService {

    // Creation
    Lesson save(Lesson l) throws Exception;

    // Update
    void update(Lesson l) throws Exception;

    // Search
    Lesson findById(Long id) throws Exception;
    Lesson findByEndpoint(String endpoint) throws Exception;
    List<Lesson> findByUser(Teacher teacher) throws Exception;
    List<Lesson> findAll();

    // Deletion
    void delete(Long id) throws Exception;
    void deleteAll();

    // Utils
    Long getTotalLessons();
    List<String> getAttendanceList(Lesson l);
}

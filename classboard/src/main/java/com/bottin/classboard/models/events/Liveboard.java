package com.bottin.classboard.models.events;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Liveboard.java
 * Description : Model for liveboards (free drawing board) in a lesson
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class Liveboard extends Event {

    private String name;
    private String imagePath;

    public Liveboard(Lesson lesson) {
        super(lesson, EventType.BOARD);
    }

    @PostPersist
    private void updateName() {
        this.name = this.getLesson().getSubject().replaceAll("\\s", "") + "Board_" +
                this.getStartDate().getYear() + this.getStartDate().getMonthValue() +
                this.getStartDate().getDayOfMonth() + "_" + this.getStartDate().getHour() +
                this.getStartDate().getMinute() + this.getStartDate().getSecond();
    }

    @Override
    public List<Log> getLogs() {
        List<Log> logs = new ArrayList<>();

        // Start
        logs.add(new Log(getStartDate(),
                "BOARD STARTED",
                "Board " + this.name + " started",
                "var(--creationLogColor)"));

        // End
        if(getEndDate() != null) {
            logs.add(new Log(getStartDate(),
                    "BOARD ENDED",
                    "Board " + this.name + " ended",
                    "var(--endExerciseLogColor)"));
        }

        return logs;
    }

    @Override
    public String getResponse() {
        return null;
    }
}

// in this file you can append custom step methods to 'I' object

function makeRandomString(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function makeEmail(){
  return makeRandomString(4).concat("@").concat(makeRandomString(4))
}

class User {
  constructor(){
    this.username = makeRandomString(5);
    this.firstName = makeRandomString(6);
    this.lastName = makeRandomString(6);
    this.password = makeRandomString(5);
    this.wrongPassword = makeRandomString(6);
  }
}

let users = [];

module.exports = function() {
  return actor({

    // Define custom steps here, use 'this' to access default methods of this.
    // It is recommended to place a general 'login' function here.

    testLoginAndRegister(){
      let randomUser = new User()

      // login not found account
      this.amOnPage('/login')
      this.fillField('#form-input-username', randomUser.username)
      this.fillField('#pass1',secret(randomUser.password))
      this.click('Sign in')
      this.waitForText('Wrong password or username')
    },

    generateString(length){
      return makeRandomString(length)
    },

    generateEmail(){
      return makeEmail()
    }

  });
}

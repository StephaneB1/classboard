package com.bottin.classboard.repositories.exercises;

import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseTrueFalseRepository.java
 * Description : Repository for true false exercises
 */
@Repository
public interface ExerciseMultipleChoiceRepository extends JpaRepository<ExerciseMultipleChoice, Long> {
    /*@Query("SELECT COUNT(a) FROM StudentAnswer a WHERE a.exercise.id = ?1 AND a.answer = ?2")
    int getTotalCorrectAnswers(Long id, String solution);

    @Query("SELECT COUNT(a) FROM StudentAnswer a WHERE a.exercise.id = ?1 AND a.answer <> ?2")
    int getTotalWrongAnswers(Long id, String solution);*/

    @Query("SELECT a.answer FROM StudentAnswer a WHERE a.exercise.id = ?1")
    List<String> getAnswers(Long id);
}

package com.bottin.classboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ClassboardApplication.java
 * Description : Main application for classboard
 */
@SpringBootApplication
@EnableConfigurationProperties
public class ClassboardApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClassboardApplication.class, args);
	}
}

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : classroom-dashboard.js
 * Description : Client-side handler for the classroom panel for students
 */

const endDate = document.getElementById('break-end-date').value;

function init() {
    // Connect to the app's websockets
    connect().then(function () {

        const endTime = new Date(endDate);
        showBreakInProgress(endTime.getHours() + ":" + endTime.getMinutes()
            + ":" + endTime.getSeconds(), function () {
            document.getElementById('countdown-panel').style.display = "none";
            document.getElementById('break-feedback-panel').style.display = "flex";
        });

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

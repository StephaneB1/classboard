package com.bottin.classboard.models.events;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ActivityType.java
 * Description : Enumeration of all activity types
 */
public enum ActivityType {
    VIDEO("Video"),
    PEER_INSTRUCTION("Peer Instruction");

    String title;

    ActivityType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}

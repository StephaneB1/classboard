package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.events.BreakEvent;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : BreakService.java
 * Description : Service interface for breaks
 */
@Service
public interface BreakService {

    // Creation

    BreakEvent save(BreakEvent b) throws Exception;

    // Update

    BreakEvent update(BreakEvent b) throws Exception;

    // Search

    BreakEvent findById(Long id) throws Exception;
    BreakEvent findOnGoingBreak(Lesson lesson) throws Exception;
    List<BreakEvent> findAll();

    // Deletion

    void delete(Long id) throws Exception;

    void deleteAll();
}

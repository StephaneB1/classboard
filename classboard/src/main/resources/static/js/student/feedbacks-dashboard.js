/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : feedbacks-dashboard.js
 * Description : Client-side handler for the students' feedbacks panel
 */

const FEEDBACK_TIMEOUT_S = 5;

const feedback_panel = {
    container: document.getElementById('feedbacks-container'),
    // For every positive there is a negative
    feedbacks: [
        // Vitality
        {text: "Just... fascinating.",      scales:["V"], pos: true},
        {text: "I'm bored...",              scales:["V"], pos: false},
        {text: "Can we go a bit faster..?", scales:["V"], pos: false},
        {text: "Speed is a-okay.",          scales:["V"], pos: true},

        // Clarity
        {text: "Ok, this is interesting.", scales:["C"], pos: true},
        {text: "Yep, I'm lost.",           scales:["C"], pos: false},

        // Motivation
        {text: "Keep it up, I love it.",  scales:["M"], pos: true},
        {text: "I'm just not motivated.", scales:["M"], pos: false},

        // Interactivity
        {text: "Please stop with the interactions.", scales:["I"], pos: false},
        {text: "Love the interactivity.",            scales:["I"], pos: true},
        {text: "Just too much theory.",              scales:["I"], pos: false},
        {text: "Just enough exercises to get it.",   scales:["I"], pos: true},
    ]
}

function init() {
    // Connect to the app's websockets
    connect().then(function () {

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";

        displayFeedbacks();
    });
}

function displayFeedbacks() {
    let feedbackArray = feedback_panel.feedbacks;
    shuffleArray(feedbackArray);

    for (const feedback of feedbackArray) {
       const feedbackBtn = document.createElement("button");
       const coolDownProgressBar = document.createElement("div");
       feedbackBtn.innerText = feedback.text;
       feedbackBtn.classList.add(feedback.pos ? "pos" : "neg");
       feedbackBtn.onclick = function() {
           feedbackBtn.disabled = true;
           coolDownProgressBar.style.visibility = "visible";
           coolDownProgressBar.style.width = "0%";
           sendFeedbacks(feedback.scales, feedback.pos);
           // 30s Cooldown
           setTimeout(function() {
               coolDownProgressBar.style.visibility = "visible";
               coolDownProgressBar.style.width = "100%";
               feedbackBtn.disabled = false;
           }, FEEDBACK_TIMEOUT_S * 1000);
       };
       coolDownProgressBar.classList.add("progress-bar");
       coolDownProgressBar.style.visibility = "hidden";
       feedbackBtn.appendChild(coolDownProgressBar);
       feedback_panel.container.appendChild(feedbackBtn);
    }
}

/*https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array*/
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function debug_feedback() {

    const TOTAL_FEEDBACK_PER_TICK = 5;
    const SCALES_LIST = ["M", "V", "C", "I"];

    setInterval(function() {
        setTimeout(() => {
            const scales = [];
            let i;
            for(i = 0; i < TOTAL_FEEDBACK_PER_TICK; i++)
                scales.push(SCALES_LIST[Math.floor(Math.random() * SCALES_LIST.length)])
            //sendFeedbacks(scales, Math.random() < 0.5);
            sendFeedbacks(["M"], true);
        }, Math.floor(Math.random() * 500));
    }, 1000);
}
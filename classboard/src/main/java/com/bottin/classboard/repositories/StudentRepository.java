package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentRepository.java
 * Description : Repository for students
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, String> {
}

package com.bottin.classboard.utils;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ClassroomGroupsUtils.java
 * Description : Utils for students' groups generation in a lesson
 */
public class ClassroomGroupsUtils {

    public static String getStudentGroups(Lesson lesson) {

        // For every student id, check if already in a group or not
        Map<String, Boolean> groupCheck = new HashMap<>();
        JSONArray groups = new JSONArray();

        for(Student student : lesson.getStudents()) {
            // Add student if not present
            if(!groupCheck.containsKey(student.getId())) {
                groupCheck.put(student.getId(), false);
            }

            if(!groupCheck.get(student.getId())) {
                // Not already in a group
                if(student.getNeighbour() != null) {
                    // The student is part of a group
                    JSONArray group = new JSONArray();

                    // Add the first student and mark it as "in a group"
                    group.put(getStudentClassroomJson(student));
                    groupCheck.put(student.getId(), true);

                    // Add the neighbour if not present
                    if(!groupCheck.containsKey(student.getNeighbour().getId())) {
                        groupCheck.put(student.getNeighbour().getId(), false);
                    }

                    // Add the second student and mark it as "in a group"
                    group.put(getStudentClassroomJson(student.getNeighbour()));
                    groupCheck.put(student.getNeighbour().getId(), true);
                    groups.put(group);
                } else {
                    // The student is alone
                    JSONArray group = new JSONArray();
                    group.put(getStudentClassroomJson(student));
                    groupCheck.put(student.getId(), true);
                    groups.put(group);
                }
            }
        }

        return groups.toString();
    }

    private static JSONObject getStudentClassroomJson(Student student) {
        JSONObject result = new JSONObject();
        result.put("name", student.getFullName());
        return result;
    }

}

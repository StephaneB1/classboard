package com.bottin.classboard.unit;

import com.bottin.classboard.models.*;
import com.bottin.classboard.models.events.Liveboard;
import org.apache.tomcat.jni.Local;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiveboardModelTests {
    @Test
    public void canCreateALiveboard() {
        LocalDateTime now = LocalDateTime.now();
        Liveboard liveboard = new Liveboard(
                new Lesson(new Teacher(), "Muggle Art", now.plusMinutes(30))
        );

        assertEquals("Muggle Art", liveboard.getLesson().getSubject());

        // TODO : in integration tests
        /*assertEquals("MuggleArtBoard_" + now.getYear() +
                now.getMonthValue() + now.getMinute() + "_" + now.getHour() +
                now.getMinute() + now.getSecond(), liveboard.getName());*/
    }
}

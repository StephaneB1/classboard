/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : profile.js
 * Description : Client-side handler for the teachers' profile
 */

const MAX_HOUR_PER_LESSON = 6;

// New lesson
const startHour = document.getElementById('today-hour');
const startMinute = document.getElementById('today-minute');
const endHour = document.getElementById('end-hour');
const endMinute = document.getElementById('end-minute');
const startButton = document.getElementById('start-lesson-button');
const errorMsg = document.getElementById('time-error-message');

const finalHourInput = document.getElementById('endHourInput');
const finalMinuteInput = document.getElementById('endMinuteInput');

let isTomorrow = false;

function openNewLessonPopup() {
    let today = new Date();

    // Show start time
    startHour.innerText = getFormatDoubleDigit(today.getHours());
    startMinute.innerText =  getFormatDoubleDigit(today.getMinutes());

    // Set the time input to now for easier readability
    updateHourInput(today.getHours());
    const upperFifthValue = Math.ceil(Math.abs(today.getMinutes()) / 5) * 5;
    if(upperFifthValue >= 60) {
        updateMinuteInput(0);
        addHour();
    } else {
        updateMinuteInput(upperFifthValue);
    }

    // Show popup
    document.getElementById('new-lesson-popup').style.display = 'flex';

    // Update display times every seconds (like a digital clock)
    setInterval(function() {
        let now = new Date();
        // Update start time
        startHour.innerText = getFormatDoubleDigit(now.getHours())
        startMinute.innerText = getFormatDoubleDigit(now.getMinutes());

        // Update end time if it's the same as the start time
        if(getCurrentHour() === now.getHours() && getCurrentMinutes() === now.getMinutes()) {
            addMinutes();
        }
    }, 1000);
}

function addHour() {
    hideErrorMessage();

    const now = new Date();
    const h = getCurrentHour();

    if(h - now.getHours() > MAX_HOUR_PER_LESSON) {
        excessiveTimeError();
        return;
    }

    if(h >= 23) {
        isTomorrow = true;
        document.getElementById('to-label').innerText = 'To (tomorrow)';
        updateHourInput(0);
    } else {
        updateHourInput(h + 1);
    }
}

function removeHour() {
    hideErrorMessage();

    const now = new Date();
    const h = getCurrentHour();
    const m = getCurrentMinutes();

    if (h > 0) {
        if (now.getHours() < h || isTomorrow) {
            if(now.getHours() === (h - 1) && now.getMinutes() > m) {
                invalidTimeError();
            } else {
                updateHourInput(h - 1);
            }
        } else {
            invalidTimeError();
        }
    } else {
        if(now.getMinutes() > m && now.getHours() <= h) {
            isTomorrow = false;
            document.getElementById('to-label').innerText = 'To';
            updateHourInput(23);
        } else {
            invalidTimeError();
        }
    }

    updateDiffTime();
}

function addMinutes() {
    hideErrorMessage();

    const now = new Date();
    const h = getCurrentHour();
    const m = getCurrentMinutes();

    if (m < 55) {
        updateMinuteInput(m + 5);
    } else {
        if(h - now.getHours() < MAX_HOUR_PER_LESSON) {
            updateMinuteInput(0);
            addHour();
        } else {
            excessiveTimeError();
        }
    }

    updateDiffTime();
}

function removeMinutes() {
    hideErrorMessage();

    const now = new Date();
    const h = getCurrentHour();
    const m = getCurrentMinutes();

    if (m > 0) {
        if (now.getHours() === h && now.getMinutes() > (m - 5)) {
            invalidTimeError();
        } else {
            updateMinuteInput(m - 5);
        }
    } else {
        if(h > now.getHours()) {
            updateMinuteInput(55);
            removeHour();
        } else {
            invalidTimeError();
        }
    }
}

function updateHourInput(h) {
    endHour.innerText = getFormatDoubleDigit(h);
    finalHourInput.value = h;
    updateDiffTime();
}

function updateMinuteInput(m) {
    endMinute.innerText = getFormatDoubleDigit(m);
    finalMinuteInput.value = m;
    updateDiffTime();
}


function updateDiffTime() {
    let today = new Date();

    const h = getCurrentHour();
    const m = getCurrentMinutes();

    // Compute diff in minutes
    let endDate = new Date(today.getFullYear(), today.getMonth(), isTomorrow ? today.getDate() + 1 : today.getDate(), h, m);

    let diffMs = (endDate - today); // milliseconds between now & end date
    let diffMins = Math.round(diffMs / 60000); // minutes

    if(diffMins <= 0) {
        startButton.innerText = "Invalid time";
        startButton.disabled = true;
    } else {
        const hours = Math.floor(diffMins / 60);
        const minutes = diffMins % 60;
        if(hours > 0) {
            if(minutes > 0) {
                startButton.innerText = "Start " + hours + "h" +
                    (minutes < 10 ? "0" : "") + minutes + " lesson";
            } else {
                startButton.innerText = "Start " + hours + "h lesson";
            }
        } else {
            startButton.innerText = "Start " + minutes + "m lesson";
        }

        startButton.disabled = false;
    }
}

function getCurrentHour() {
    return parseInt(endHour.innerText);
}

function getCurrentMinutes() {
    return parseInt(endMinute.innerText);
}

function getFormatDoubleDigit(x) {
    return (x < 10 ? "0" : "") + (x);
}

function excessiveTimeError() {
    displayErrorMessage("The lesson can't last more than " + MAX_HOUR_PER_LESSON + "  hours");
}

function invalidTimeError() {
    displayErrorMessage("The lesson can't end in the past!");
}

function hideErrorMessage() {
    errorMsg.style.display = 'none';
}

function displayErrorMessage(message) {
    errorMsg.innerText = message;
    errorMsg.style.display = 'block';
}
package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Student;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentService.java
 * Description : Service interface for students
 */
@Service
public interface StudentService {

    // Creation

    Student save(Student u) throws Exception;

    // Update

    void update(Student u) throws Exception;

    // Search

    Student findById(String id) throws Exception;

    List<Student> findAll();

    // Deletion

    void delete(String id) throws Exception;

    void deleteAll();

    // Utils

    void leaveLesson(String id) throws Exception;

}

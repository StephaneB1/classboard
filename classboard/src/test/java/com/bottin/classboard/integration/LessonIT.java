package com.bottin.classboard.integration;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.LessonRepository;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.implementations.LessonServiceImpl;
import com.bottin.classboard.services.implementations.StudentServiceImpl;
import com.bottin.classboard.services.interfaces.LessonService;
import com.bottin.classboard.services.interfaces.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
//@ActiveProfiles("dev") // For local tests only
class LessonIT {

	@TestConfiguration
	static class OrganisationServiceImplTestContextConfiguration {

		@Bean
		public LessonService lessonService(LessonRepository lessonRepository, TeacherRepository teacherRepository) {
			return new LessonServiceImpl(lessonRepository, teacherRepository);
		}

		@Bean
		public StudentService studentService(StudentRepository studentRepository) {
			return new StudentServiceImpl(studentRepository);
		}
	}

	@Autowired
	private LessonRepository lessonRepository;

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private StudentService studentService;

	@Autowired
	private LessonService lessonService;

	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(teacherRepository).isNotNull();
		assertThat(lessonRepository).isNotNull();
		assertThat(lessonService).isNotNull();
		assertThat(studentService).isNotNull();
	}

	@Test
	void canInsertLessonInDatabase() {
		Teacher teacher = new Teacher("is", "this", "living", "free");
		Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

		assertDoesNotThrow(() -> lessonService.save(lesson));
	}

	@Test
	void canFindLessonInDatabase() {
		Teacher teacher = teacherRepository.save(new Teacher("is", "this", "living", "free"));
		Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

		assertDoesNotThrow(() -> lessonService.save(lesson));
		assertDoesNotThrow(() -> lessonService.findByEndpoint(lesson.getEntryEndpoint()));
		assertDoesNotThrow(() -> lessonService.findById(lesson.getId()));
	}

	@Test
	void newLessonHasNoStudents() {
		Teacher teacher = new Teacher("is", "this", "living", "free");
		Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

		assertEquals(0, lessonService.getAttendanceList(lesson).size());
	}

	@Test
	void canBeJoinedByStudents() throws Exception {
		Teacher teacher = teacherRepository.save(new Teacher("is", "this", "living", "free"));
		Lesson lesson = lessonService.save(new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1)));
		int totalStudents = new Random().nextInt(50);
		for(int i = 0; i < totalStudents; i++)
			studentService.save(new Student(lesson, "Magnificent", "Lizard", "pas$w0rd"));

		assertEquals(totalStudents, lessonService.getAttendanceList(lesson).size());
	}
}

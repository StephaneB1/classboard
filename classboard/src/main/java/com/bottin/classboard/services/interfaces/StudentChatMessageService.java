package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentChatMessage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentChatMessageService.java
 * Description : Service interface for students' chat messages
 */
@Service
public interface StudentChatMessageService {

    // Creation

    StudentChatMessage sendMessage(Student from, Student to, String message) throws Exception;

    // Update

    void update(StudentChatMessage u) throws Exception;

    // Search

    StudentChatMessage findById(String id) throws Exception;

    List<StudentChatMessage> findAll();

    // Deletion

    void delete(String id) throws Exception;

    void deleteAll();

}

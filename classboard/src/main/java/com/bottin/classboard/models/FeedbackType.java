package com.bottin.classboard.models;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackType.java
 * Description : Enumeration of all types of feedback (MVCI)
 */
public enum FeedbackType {
    MOTIVATION,
    VITALITY,
    CLARITY,
    INTERACTIVITY
}

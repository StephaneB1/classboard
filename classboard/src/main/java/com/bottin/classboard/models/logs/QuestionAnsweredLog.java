package com.bottin.classboard.models.logs;

import com.bottin.classboard.models.Question;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : QuestionAnsweredLog.java
 * Description : Model for logging all answers to a question
 */
public class QuestionAnsweredLog extends Log {
    public QuestionAnsweredLog(Question question) {
        super(question.getLastUpdatedDate(),
                "QUESTION ANSWERED",
                "<" + question.getQuestion() + "> got answered by <" +
                question.getAnswer() + ">",
                "var(--questionAnsweredLogColor)");
    }

}

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : liveboard.js
 * Description : Client-side handler for liveboards
 */

const board_items = {
    id: document.getElementById('id'),
    canvas: document.getElementById('myCanvas'),
    end_popup: document.getElementById('end-board-popup')
}

let drawing = false;

function init() {
    // Init dark or light mode
    initTheme();

    // Connect to the app's websockets
    connect().then(function () {

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function sendDrawPathSegmentsToStudents(pathJson) {
    sendMessageToStudentsFromTeacher("BOARD_PATH", JSON.stringify({
        id: board_items.id.value,
        path: pathJson
    }));
}

function sendEndSessionToStudents(saving, svg) {
    sendMessageToStudentsFromTeacher("BOARD_END", JSON.stringify({
        id: board_items.id.value,
        save: saving,
        svg: svg
    }));
}

function changePenColor(color) {
    document.getElementById('currentColor').value = color;
}

function updatePenSize() {
    const value = document.getElementById('penSize').value;
    document.getElementById('penSizeValue').innerText = value + "px";

    let actualPenSize = document.getElementById('actualPenSize');
    actualPenSize.style.height = value + "px";
    actualPenSize.style.width = value + "px";
}

function openExitPopup() {
    board_items.end_popup.style.display = 'flex';
    board_items.canvas.style.display = 'none';
}

function closeExitPopup() {
    board_items.end_popup.style.display = 'none';
    board_items.canvas.style.display = 'block';
}

function closeAndDiscardBoard() {
    finishBoardSession(false, "");
}

function closeAndSaveBoard() {
    finishBoardSession(true, board_funcs.getSVG());
}

function displayDrawingBoard() {
    location.reload();

    drawing = true;
}

function resize() {
    window.dispatchEvent(new Event('resize'));
}

function startNewBoardSession() {
    sendMessageToStudentsFromTeacher("BOARD_START", "{}");
}

function finishBoardSession(saving, svg) {
    // Tell students the session is over
    sendEndSessionToStudents(saving, svg);

    // Go back to the lesson dashboard
    //window.open('/dashboards/' + lesson_id,"_self");
}

window.onbeforeunload = function() {
    if(drawing)
        return "Your board's data will be lost if you refresh or leave the page, are you sure?";
};
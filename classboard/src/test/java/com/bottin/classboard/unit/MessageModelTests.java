package com.bottin.classboard.unit;

import com.bottin.classboard.models.Message;
import com.bottin.classboard.models.MessageType;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageModelTests {

    private final String content = "The medium is the message.";
    private final MessageType type = MessageType.CHAT_MESSAGE;
    private final String sender_id = "SENDER_ID";
    private final String recipient_id = "RECIPIENT_ID";
    private final LocalDateTime date = LocalDateTime.now();

    @Test
    public void canCreateAMessage() {
        Message message = new Message(content, type, sender_id, recipient_id, date);

        assertEquals(message.getContent(), content);
        assertEquals(message.getType(), type);
        assertEquals(message.getSender_id(), sender_id);
        assertEquals(message.getRecipient_id(), recipient_id);
        assertEquals(message.getDate(), date);
    }

    @Test
    public void canConvertAMessageToJson() {
        Message message = new Message(content, type, sender_id, recipient_id, date);
        assertEquals("{\"date\":\"" + date + "\"," +
                "\"type\":\"" + type + "\"," +
                "\"content\":\"" + content + "\"," +
                "\"sender_id\":\"" + sender_id + "\"," +
                "\"recipient_id\":\"" + recipient_id +"\"}", message.toJson());
    }
}

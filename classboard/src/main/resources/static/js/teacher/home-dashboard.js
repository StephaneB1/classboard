/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : teacher-dashboard.js
 * Description : Client-side handler for teachers' dashboards
 */

const GRAPH_REFRESH_RATE_S = 1; // Can't be faster than one second

// Students map (to avoid duplicates)
let students_map = new Map();
let cleanGroups;

// Questions index
let questions = [];
let questionIndex = 0;

const questions_panel = {
    leftArrow: document.getElementById('left-arrow'),
    rightArrow: document.getElementById('right-arrow'),
    container: document.getElementById('questions-list'),
    noQuestions: document.getElementById('no-questions'),
    footer: document.getElementById('question-answer-input'),
    footerInput: document.getElementById('q-answer-input'),
    footerSendBtn: document.getElementById('question-send-btn'),
    header: document.getElementById('questions-header'),
};

function init() {
    // Init dark or light mode
    initTheme();

    // Connect to the app's websockets
    connect().then(function () {
        // Init on going questions
        initQuestions();

        // Init Classroom display
        initClassroom();

        // Init feedback chart
        initChart();

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function initClassroom() {
    cleanGroups = groups_db.replaceAll("&quot;",'"');
    updateClassroomDisplay(JSON.parse(cleanGroups), true);
}

function initChart() {
    // Update feedback chart every seconds
    updateFeedbackChart(lesson_id + "/now");

    let chartUpdateLoop = setInterval(() => {
        updateFeedbackChart(lesson_id + "/now");
    }, GRAPH_REFRESH_RATE_S * 1000);
}

function initQuestions() {
    // Parse questions
    let cleanedJSON = questions_db.replaceAll("&quot;",'"');
    questions = JSON.parse(cleanedJSON);
    updateQuestionsDisplay();
}

function getQuestionHTML(student, question, timestamp, visible) {
    let containerDiv = document.createElement("div");
    let questionDiv = document.createElement("div");
    questionDiv.className = "question";
    questionDiv.style.display = visible ? "block" : "none";
    questionDiv.innerHTML = question +
        " <div class='question-author'>- " + student + "</div>" +
        " <i>(" + timestamp + ")</i>";
    containerDiv.appendChild(questionDiv);
    return containerDiv;
}

function updateQuestionsDisplay() {
    // Reset the questions list
    questions_panel.container.innerHTML = "";

    console.log("Updating question : " + questionIndex + " / " + questions.length);

    // Display the specific question's details
    if(questions.length > 0 && questionIndex < questions.length) {
        const question = questions[questionIndex];
        console.log(question);
        questions_panel.container.appendChild(
            getQuestionHTML(question.name, question.question,
                question.timestamp, true));

        // Answered or not displays
        if(question.answered) {
            questions_panel.footerInput.disabled = true;
            questions_panel.footerInput.value = "Waiting confirmation...";
            questions_panel.footerSendBtn.style.display = "none";
        } else {
            questions_panel.footerInput.disabled = false;
            questions_panel.footerInput.value = "";
            questions_panel.footerSendBtn.style.display = "flex";
        }
    }

    // Other displays
    questions_panel.noQuestions.style.display = questions.length > 0 ? "none" : "flex";
    questions_panel.footer.style.visibility = questions.length > 0 ? "visible" : "hidden";

    questions_panel.leftArrow.style.display = questions.length > 1 ? "block" : "none";
    questions_panel.rightArrow.style.display = questions.length > 1 ? "block" : "none";
    questions_panel.header.innerText = questions.length > 0 ?
        "Questions (" + (questionIndex + 1) + "/" + questions.length + ")" : "Questions";
}

function openShortcut() {
    const code = document.getElementById('entry-code').value;
    copyToClipboard(window.location.host + "/" + direct_link);
    sendMessageToSelf("SWITCH_ACCESS", "{}");
}

/**
 * Adds a new student to the lesson
 * @param id        : id of the student
 * @param name      : name of the student
 */
function addNewStudent(id, name) {

    console.log(students_map);

    // Avoid unnecessary messages / updates
    if(students_map.has(id)) {
       return;
    }

    console.log("ADDING NEW STUDENT : " + name);
    students_map.set(id, name);

    // Add the student name into the groups
    let groups = JSON.parse(cleanGroups);
    let added = false;
    groups.forEach(group => {
        if(added === false && group.length === 1) {
            // Student is alone = one available space
            group.push({name: name});
            added = true;
        }
    });

    // No groups available, we create one
    if(added === false) {
        groups.push([{name: name}]);
    }

    // Send classroom update to everyone
    cleanGroups = JSON.stringify(groups);
    sendMessageToStudentsFromTeacher("STUDENT_UPDATE", JSON.stringify({
        groups: groups
    }));
}

function removeStudent(id) {

    // Only remove if present
    if(!students_map.has(id)) {
        return;
    }

    const name = students_map.get(id);
    students_map.delete(id);
    //updateStudentsTable();

    // Remove students from the groups
    let groups = JSON.parse(cleanGroups);
    let i;
    for(i = 0; i < groups.length; i++) {
        const group = groups[i];
        console.log("Checking : " + group);
        if(group.length === 2) {
            if(group[0].name === name) {
                groups[i] = [group[1]];
            } else if (group[1].name === name) {
                groups[i] = [group[0]];
            }
        } else {
            console.log("Is the lonely " + group[0].name + " our guy?");
            if(group[0].name === name) {
                console.log("Setting empty group");
                groups[i] = [];
            }
        }
    }

    console.log(groups);

    cleanGroups = JSON.stringify(groups);
    // Send classroom update to everyone
    sendMessageToStudentsFromTeacher("STUDENT_UPDATE", cleanGroups);
}

function addNewQuestion(question) {
    questions.push(question);
    updateQuestionsDisplay();
}

function goToNextQuestion() {
    if(questions.length > 0) {
        if(questionIndex < questions.length - 1) {
            questionIndex++;
        } else {
            questionIndex = 0;
        }
    }

    updateQuestionsDisplay();
}

function goToPreviousQuestion() {
    if(questions.length > 0) {
        if(questionIndex > 0) {
            questionIndex--;
        } else {
            questionIndex = questions.length - 1;
        }
    }

    updateQuestionsDisplay();
}

function answerQuestion() {

    const answeredQuestion = questions[questionIndex];
    const answer = questions_panel.footerInput.value;

    // Send to students the question that has been answered
    sendMessageToStudentsFromTeacher("QUESTION_ANSWERED", JSON.stringify({
        id: answeredQuestion.question_id,
        question: answeredQuestion.question,
        answer: answer
    }));

    // Send to specific student to have feedback
    sendMessageToStudent(lesson_id, answeredQuestion.student_id,
    "QUESTION_ANSWERED", JSON.stringify({
        id: answeredQuestion.question_id,
        question: answeredQuestion.question,
        answer: answer
    }));

    // Clear input
    questions_panel.footerInput.value = "";

    // Waiting for the student to confirm the answer was understood
    questions[questionIndex].answered = true;
    updateQuestionsDisplay();
}

// Feedback chart
let ctx = document.getElementById('feedbackChart').getContext('2d');
let feedbackChart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
            label: 'Motivation',
            fill: false,
            borderColor: '#EF932D',
            backgroundColor: '#EF932D',
            cubicInterpolationMode: 'monotone',
            tension: 0.4
        },{
            label: 'Vitality',
            fill: false,
            borderColor: '#C631F0',
            backgroundColor: '#C631F0',
            cubicInterpolationMode: 'monotone',
            tension: 0.4
        },{
            label: 'Clarity',
            fill: false,
            borderColor: '#23A0EB',
            backgroundColor: '#23A0EB',
            cubicInterpolationMode: 'monotone',
            tension: 0.4
        },{
            label: 'Interactivity',
            fill: false,
            borderColor: '#76D513',
            backgroundColor: '#76D513',
            cubicInterpolationMode: 'monotone',
            tension: 0.4
        }]
    },

    // Configuration options go here
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: { enabled: false },
        hover: { mode: null },
        elements: {
            point:{
                radius: 0
            }
        },
        plugins: {
            decimation: {
                enabled: false,
                algorithm: 'lttb',
                samples: 500
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 100
                },
                gridLines: {
                    drawOnChartArea: false
                }
            }],
            xAxes: [{
                ticks: {
                    autoSkip: false
                },
                gridLines: {
                    drawOnChartArea: false
                }
            }]
        }
    }
});

// Data request for feedback data
function updateFeedbackChart(endpoint) {

    // TODO : If error for more than X tries in a row :
    //  show popup lost connection or something

    $.ajax({
        url: "/feedbacks/" + endpoint,
        type: "GET",
        dataType: "json",
        success: function(data) {
            feedbackChart.data.labels = data.labels;

            // Display data from all 4 feedbacks
            feedbackChart.data.datasets[0].data = data.data_motivation;
            feedbackChart.data.datasets[1].data = data.data_vitality;
            feedbackChart.data.datasets[2].data = data.data_clarity;
            feedbackChart.data.datasets[3].data = data.data_interactivity;

            // Display last point of data only
            let points = new Array(data.labels.length).fill(0);
            points[data.data_motivation.length - 1] = 5;
            feedbackChart.options.elements.point.radius = points;

            feedbackChart.update();
        },
        error: function(data) {
            console.log(data);
        }
    });
}

/**
 * Copies a string to clipboard
 * @param str : string
 */
function copyToClipboard(str) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
        document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }
}
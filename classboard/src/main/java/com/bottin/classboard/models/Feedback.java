package com.bottin.classboard.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Feedback.java
 * Description : Model for feedbacks in a lesson
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lesson lesson;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

    private FeedbackType type;
    private boolean isPositive;

    public Feedback(Lesson lesson, Student student, FeedbackType type, boolean isPositive) {
        this.lesson = lesson;
        this.student = student;
        this.type = type;
        this.isPositive = isPositive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Feedback)) return false;
        return this.id != null && this.id.equals(((Feedback) o).getId());
    }

    @Override
    public String toString() {
        return "Feedback" + id;
    }
}

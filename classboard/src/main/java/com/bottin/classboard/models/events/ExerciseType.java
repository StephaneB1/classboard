package com.bottin.classboard.models.events;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseType.java
 * Description : Enumeration of all exercise types
 */
public enum ExerciseType {
    TRUE_FALSE("True / False"),
    MULTIPLE_CHOICE("Multiple Choice");

    String title;

    ExerciseType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}

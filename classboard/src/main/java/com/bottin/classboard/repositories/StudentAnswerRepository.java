package com.bottin.classboard.repositories;

import com.bottin.classboard.models.StudentAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentAnswerRepository.java
 * Description : Repository for students' answers
 */
@Repository
public interface StudentAnswerRepository extends JpaRepository<StudentAnswer, Long> {
}

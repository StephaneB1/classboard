package com.bottin.classboard.integration;

import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.implementations.TeacherServiceImpl;
import com.bottin.classboard.services.interfaces.TeacherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
//@ActiveProfiles("dev") // For local tests only
class TeacherIT {

	@TestConfiguration
	static class OrganisationServiceImplTestContextConfiguration {

		@Bean
		public TeacherService teacherService(TeacherRepository teacherRepository) {
			return new TeacherServiceImpl(teacherRepository);
		}
	}

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private TeacherService teacherService;

	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(teacherRepository).isNotNull();
		assertThat(teacherService).isNotNull();
	}

	@Test
	void canInsertTeacherInDatabase() throws Exception {
		Teacher teacher = new Teacher("is", "this", "living", "free");
		assertDoesNotThrow(() -> teacherService.save(teacher));
	}

	@Test
	public void whenDeleteAllFromService_thenRepositoryShouldBeEmpty() {
		teacherService.deleteAll();
		assertThat(teacherRepository.count()).isEqualTo(0);
	}
}

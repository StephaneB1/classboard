package com.bottin.classboard.models.logs;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Log.java
 * Description : Model for logging all lesson activity
 */
@Data
public class Log {
    private LocalDateTime date;
    private String type;
    private String description;
    private String displayColor;

    public Log(LocalDateTime date, String type, String description, String displayColor) {
        this.date = date;
        this.type = type;
        this.description = description;
        this.displayColor = displayColor;
    }
}

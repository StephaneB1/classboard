package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Feedback;
import com.bottin.classboard.models.FeedbackType;
import com.bottin.classboard.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackRepository.java
 * Description : Repository for feedbacks
 */
@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    //@Query("select f from Feedback f where f.type = ?1 and f.student.id = ?2 and f.createdDate = (select max(f2.createdDate) from Feedback f2 where f2.id=f.id and f2.type = ?1 and f2.student.id = ?2)")
    @Query("select f from Feedback f where f.type = ?1 and f.student.id = ?2 and f.createdDate = (select max(f2.createdDate) from Feedback f2)")
    List<Feedback> findLastFromStudent(FeedbackType type, String studentId);

    @Query("select f from Feedback f where f.lesson.id = ?1 order by f.createdDate asc")
    List<Feedback> getFeedbacksByDate(Long lessonId);

}

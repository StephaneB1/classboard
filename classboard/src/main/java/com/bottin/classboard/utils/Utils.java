package com.bottin.classboard.utils;

import com.bottin.classboard.models.Message;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Utils.java
 * Description : Main utils for generic functions
 */
public class Utils {

    private static List<String> adjectives = new ArrayList<>();
    private static List<String> animals = new ArrayList<>();

    public static float getNextFeedbackValue(float previous, boolean positive) {
        return positive ?
                Math.min(1.0f, previous + 1 / (2 + previous)) :
                Math.max(-1.0f, previous - 1 / (2 - previous));
    }

    public static String getTimeLabelFrom(LocalDateTime ref, LocalDateTime date, boolean shortForm) {
        String result = "";

        // Years check
        int yearsAgo = ref.getYear() - date.getYear();
        if(yearsAgo > 0) {
            return (yearsAgo == 1 ? "A year ago" : (yearsAgo + (shortForm ? "y" : " years") + " ago"));
        }

        // Same year : months check
        int monthsAgo = ref.getMonth().getValue() - date.getMonth().getValue();
        if(monthsAgo > 0) {
            return (monthsAgo == 1 ? "A month ago" : (monthsAgo + (shortForm ? "m" : " months") + " ago"));
        }

        // Same year and same month : day check
        int daysAgo = ref.getDayOfMonth() - date.getDayOfMonth();
        if(daysAgo > 0) {
            return (daysAgo == 1 ? "Yesterday" : (daysAgo + (shortForm ? "d" : " days") + " ago"));
        }

        // Same year, same month and same day : hour check
        int hoursAgo = ref.getHour() - date.getHour();
        if(hoursAgo > 0) {
            return (hoursAgo == 1 ? "An hour ago" : (hoursAgo + (shortForm ? "h" : " hours") + " ago"));
        }

        // Same year, same month,... : minute check
        int minutesAgo = ref.getMinute() - date.getMinute();
        if(minutesAgo > 0) {
            return (minutesAgo == 1 ? "A minute ago" : (minutesAgo + (shortForm ? "m" : " minutes") + " ago"));
        }

        // Same everything to the minute, it's now
        return "Now";
    }

    public static String getRandomString(int len) {
        return getRandomString(0, 255, len);
    }

    public static String getRandomString(int start, int end, int len) {
        Random random = new Random();
        return random.ints(start, end + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static Message getBasicResponse(Message message) {
        Message response = new Message();
        response.setType(message.getType());
        response.setSender_id(message.getSender_id());
        response.setDate(message.getDate());
        return response;
    }

    public static String getRandomAdjective() {
        Random rand = new Random();
        return adjectives.get(rand.nextInt(adjectives.size()));
    }

    public static String getRandomAnimal() {
        Random rand = new Random();
        return animals.get(rand.nextInt(animals.size()));
    }

    public static void addNewAnimal(String animal) {
        animals.add(animal);
    }

    public static void addNewAdjective(String adjective) {
        adjectives.add(adjective);
    }
}

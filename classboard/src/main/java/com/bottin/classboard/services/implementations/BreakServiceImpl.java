package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.events.BreakEvent;
import com.bottin.classboard.repositories.BreakRepository;
import com.bottin.classboard.services.interfaces.BreakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : BreakServiceImpl.java
 * Description : Service implementation for breaks
 */
@Service
public class BreakServiceImpl implements BreakService {

    private final BreakRepository breakRepository;

    @Autowired
    public BreakServiceImpl(BreakRepository breakRepository) {
        this.breakRepository = breakRepository;
    }

    @Override
    public BreakEvent save(BreakEvent b) throws Exception {

        if(b.getDurationMinutes() <= 0)
            throw new Exception("Break can't have a negative or null duration");

        return breakRepository.save(b);
    }

    @Override
    public BreakEvent update(BreakEvent b) throws Exception {
        if(breakRepository.findById(b.getId()).isEmpty())
            throw new Exception("Break <" + b.getId() + "> doesn't exist.");

        return breakRepository.save(b);
    }

    @Override
    public BreakEvent findById(Long id) throws Exception {
        if(breakRepository.findById(id).isEmpty())
            throw new Exception("Break <" + id + "> doesn't exist.");

        return breakRepository.findById(id).get();
    }

    @Override
    public BreakEvent findOnGoingBreak(Lesson lesson) throws Exception {
        if(breakRepository.findOnGoingBreak(lesson.getId()).isEmpty())
            throw new Exception("Lesson <" + lesson.getId() + "> has no on going breaks.");

        return breakRepository.findOnGoingBreak(lesson.getId()).get();
    }

    @Override
    public List<BreakEvent> findAll() {
        return breakRepository.findAll();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(breakRepository.findById(id).isEmpty())
            throw new Exception("Break <" + id + "> doesn't exist.");

        breakRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        breakRepository.deleteAll();
    }

}

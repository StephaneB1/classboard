package com.bottin.classboard.utils;

import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : NameCleanApp.java
 * Description : App to clean data files for procedurally generated names of
 *               students
 */
public class NameCleanApp {

    private static List<String> adjectives = new ArrayList<>();
    private static List<String> animals = new ArrayList<>();

    public static void main(String[] args) {
        // Resource file of all adjectives (cleaned and organized)
        InputStream adjectivesIs = null;
        InputStream animalsIs = null;
        try {
            adjectivesIs = new ClassPathResource("static/adjectives.txt").getInputStream();
            animalsIs = new ClassPathResource("static/animals.txt").getInputStream();

            BufferedReader brAdj = new BufferedReader(new InputStreamReader(adjectivesIs, StandardCharsets.UTF_8));
            BufferedReader brAni = new BufferedReader(new InputStreamReader(animalsIs, StandardCharsets.UTF_8));

            // Save each adjectives separately from the reader
            String adjective;
            while ((adjective = brAdj.readLine()) != null) {
                adjectives.add(adjective);
            }

            // Save each animals separately from the reader
            String animal;
            while ((animal = brAni.readLine()) != null) {
                animals.add(animal);
            }

            File animalFile = new ClassPathResource("static/animals.txt").getFile();

            // Remove duplicates and names with more than one word
            animals = animals.stream()
                    .distinct()
                    .filter(name -> !name.contains(" "))
                    .sorted()
                    .collect(Collectors.toList());

            // Remove duplicates and names with more than one word
            adjectives = adjectives.stream()
                    .distinct()
                    .filter(name -> !name.contains(" "))
                    .sorted()
                    .collect(Collectors.toList());

            // Write into the file with clean and organized data
            FileWriter writer = new FileWriter(animalFile.getAbsolutePath(), true);
            for(String animalName : animals) {
                writer.write(animalName.trim() + "\n");
            }

            writer.close();

            // Latex format
            FileWriter writerLatex = new FileWriter("latexFormat.txt", true);
            int LINES = 100;
            int COLUMNS_ADJ = 6;
            int COLUMNS_ANIMAL = 6;
            int animalIndex = 0;
            int adjIndex = 0;

            for(int i = 0; i < LINES; i++) {
                String adjs = "";
                for(int j = 0; j < COLUMNS_ADJ; j++) {
                    adjs += ((j == 0) ? "" : " & ") + (adjIndex < adjectives.size() ? adjectives.get(adjIndex) : "");
                    adjIndex++;
                }
                writerLatex.write(adjs + "\\\\\n");
                /*String anims = "";
                for(int j = 0; j < COLUMNS_ANIMAL; j++) {
                    anims += ((j == 0) ? "" : " & ") + (animalIndex < animals.size() ? animals.get(animalIndex) : "");
                    animalIndex++;
                }
                writerLatex.write(anims + "\\\\\n");*/
            }

            writerLatex.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

const {setHeadlessWhen} = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
    tests: '../src/test/java/com/bottin/classboard/end2end/*.js',
    output: './output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:8080',
            show: true,
            windowSize: '1200x900',
            chrome: {
                args: ["--headless", "--no-sandbox"]
            }
        }
    },
    include: {
        I: './steps_file.js'
    },
    bootstrap: null,
    mocha: {},
    name: 'e2e',
    plugins: {
        pauseOnFail: {},
        retryFailedStep: {
            enabled: true
        },
        screenshotOnFail: {
            enabled: false
        }
    },
}
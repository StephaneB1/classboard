/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : classroom-dashboard.js
 * Description : Client-side handler for the classroom panel for students
 */

function init() {
    // Connect to the app's websockets
    connect().then(function () {
        // Update classroom groups
        let cleanGroups = groups_db.replaceAll("&quot;",'"');
        updateClassroomDisplay(JSON.parse(cleanGroups), false);

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

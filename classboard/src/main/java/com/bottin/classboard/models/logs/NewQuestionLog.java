package com.bottin.classboard.models.logs;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Question;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : NewQuestionLog.java
 * Description : Model for logging new questions
 */
public class NewQuestionLog extends Log {
    public NewQuestionLog(Question question) {
        super(question.getSendDate(),
                "NEW QUESTION",
                question.getStudent().getFullName() + " asked <" +
                question.getQuestion() + ">",
                "var(--creationLogColor)");
    }

}

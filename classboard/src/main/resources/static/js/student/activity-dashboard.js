/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : activity-dashboard.js
 * Description : Client-side handler for the students' activity panel
 */

// Video progress per students
let videoProgressMap = new Map();

const common_items = {
    sendBtn: document.getElementById('sendBtn'),
    id: document.getElementById('id').value,
    totalStudents: document.getElementById('total-participants').value,
};

const video_panel = {
    link: document.getElementById('link'),
    updateIntervalId: -1,
    progress: -1
};

const pi_panel = {
    title: document.getElementById('pi-title'),
    messages: document.getElementById('chat-messages'),
}

// Multiple choice for peer instruction
const multiplechoice_panel = {
    optionsList: document.getElementById('multiplechoice-options-results')
};
let answer = [];

function init() {
    connect().then(function() {

        if(typeof video_panel.link !== undefined) {
            //player.playVideo();

            //playVideo(video_panel.link.value);
        }

        // Remove loader
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}


// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
let player;
function onYouTubeIframeAPIReady() {
    const line_value = video_panel.link.value;
    const video_id = line_value.substring(line_value.indexOf("?v=") + 3);
    player = new YT.Player('player', {
        height: '400',
        width: '400',
        videoId: video_id,
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    console.log("Player ready");

    const line_value = video_panel.link.value;
    const video_id = line_value.substring(line_value.indexOf("?v=") + 3);
    //event.target.playVideo();
    playVideo(video_id);
}

function onPlayerStateChange(event) {
    console.log("State changed");
}

function playVideo(id) {
    console.log("playing: " + id);
    player.cueVideoById(id, 0, "large");
    video_panel.updateIntervalId = window.setInterval(sendVideoProgress, 1000);
}

function sendVideoProgress() {
    const currentTime    = player.getCurrentTime();
    const duration       = player.getDuration();

    // Dismiss unloaded or paused videos
    if(duration <= 0 || player.getPlayerState() === YT.PlayerState.PAUSED)
        return;

    // Send video progress when there's a change
    const percentageSeen = Math.round(currentTime / duration * 100);
    if(percentageSeen > 0 && percentageSeen <= 20 && video_panel.progress !== 1) {
        video_panel.progress = 1;
        sendMessageToStudents(lesson_id, student_id,"ACTIVITY_STUDENT_UPDATE", JSON.stringify({
            id: common_items.id,
            progressStep: 1
        }));
    } else if(percentageSeen > 40 && percentageSeen <= 60 && video_panel.progress !== 2) {
        video_panel.progress = 2;
        sendMessageToStudents(lesson_id, student_id,"ACTIVITY_STUDENT_UPDATE", JSON.stringify({
            id: common_items.id,
            progressStep: 2
        }));
    } else if(percentageSeen > 90 && percentageSeen <= 100 && video_panel.progress !== 3) {
        video_panel.progress = 3;
        sendMessageToStudents(lesson_id, student_id,"ACTIVITY_STUDENT_UPDATE", JSON.stringify({
            id: common_items.id,
            progressStep: 3
        }));
    }
}

function updateMultipleChoiceAnswer() {
    const options = multiplechoice_panel.optionsList.children;

    // Reset answers before updating
    answer = [];

    let i;
    for(i = 0; i < options.length; i++) {
        answer.push(document.getElementById('option' + i + '-check').checked);
    }
}

function sendAnswer() {
    if(answer.length <= 0)
        return;

    sendMessageToStudents(lesson_id, student_id, "ACTIVITY_STUDENT_UPDATE", JSON.stringify({
        id: common_items.id,
        answers: answer
    }));

    common_items.sendBtn.disabled = true;
    common_items.sendBtn.innerText = "Your answer has been sent! The results will appear once the exercise is over.";
    disableMultipleChoiceCheckboxes();
}

function updateActivity(type, answers) {
    console.log("Handling : " + type + " / " + answers);
    switch(type) {
        case "VIDEO":
            const obj = (JSON.parse(answers));
            videoProgressMap.set(answers.student, answers.progress);

            // reset widths
            let progressBar;
            for(progressBar = 1; progressBar <= 3; progressBar++) {
                document.getElementById('yt-prog-' + progressBar).style.width = '0';
            }

            videoProgressMap.forEach(function (value, key) {
                const currentWidth = parseInt(document.getElementById('yt-prog-' + value).style.width);
                document.getElementById('yt-prog-' + value).style.width =
                    (currentWidth + (100 / activity_results.totalStudents)) + "%";
            });
            break;
        case "PEER_INSTRUCTION":
            addMultipleChoiceAnswers(JSON.parse(answers));
            break;
    }
}

function restartActivity(type) {
    switch(type) {
        case "PEER_INSTRUCTION":
            pi_panel.title.innerText = "Peer Instruction (again!)";
            common_items.sendBtn.disabled = false;
            common_items.sendBtn.innerText = "Send answer";
            resetMultipleChoiceCheckboxes();
            break;
    }
}

function addMultipleChoiceAnswers(answers) {
    if(answers.length !== multiplechoice_panel.optionsList.children.length)
        return // todo : display errors

    console.log("Updating " + answers);

    let i;
    for(i = 0; i < answers.length; i++) {
        if(answers[i]) {
            const totalText = document.getElementById('multiplechoice-total-option-' + i);
            const total = parseInt(totalText.innerText) + 1;
            const percentage = 100 * total / common_items.totalStudents;
            totalText.innerText = "" + total;
            document.getElementById('multiplechoice-total-option-bar-' + i)
                .style.width = percentage + "%";
        }
    }
}

function endActivity(type) {
    switch(type) {
        case "VIDEO":
            player.stopVideo();
            clearInterval(video_panel.updateIntervalId);
            break;
    }
}

function disableMultipleChoiceCheckboxes() {
    const options = multiplechoice_panel.optionsList.children;
    let i;
    for(i = 0; i < options.length; i++) {
        document.getElementById('option' + i + '-check').disabled = true;
    }
}

function resetMultipleChoiceCheckboxes() {
    const options = multiplechoice_panel.optionsList.children;
    let i;
    for(i = 0; i < options.length; i++) {
        const checkbox_i = document.getElementById('option' + i + '-check');
        checkbox_i.disabled = false;
        checkbox_i.checked = false;
    }
}

function showNewMessage(message, asSender) {
    // Update HTML chat
    let messageDiv = document.createElement("div");
    messageDiv.className = asSender ? "message-right" : "message-left";
    messageDiv.innerHTML = message;
    pi_panel.messages.prepend(messageDiv);
}

function sendChatMessageToDiscussionGroup() {
    let messageInput = document.getElementById('chat-text-input');
    const msg = messageInput.value;

    // Update HTML chat
    showNewMessage(msg, true);
    messageInput.value = "";

    // Send to student
    sendMessageToStudent(student_id, null, 'PI_CHAT_MESSAGE', JSON.stringify({
        id: common_items.id,
        msg: msg
    }));
}
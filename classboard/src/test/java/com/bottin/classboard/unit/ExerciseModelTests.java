package com.bottin.classboard.unit;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExerciseModelTests {

    private static final String EXERCISE_TRUE_FALSE_JSON = "{" +
            "question: \"This test will pass.\"," +
            "solution: true" +
            "}";

    private static final String EXERCISE_MULTIPLE_CHOICE_JSON = "{" +
            "question: \"Option A and C are the solutions.\"," +
            "options: [\"Option A\", \"Option B\", \"Option C\"]," +
            "solutions: [true, false, true]" +
            "}";

    // Test data
    private static final Teacher teacher = new Teacher("John123", "123", "John", "Doe");
    private static final Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

    @Test
    public void canCreateATrueFalseExercise() throws Exception {
        ExerciseTrueFalse trueFalse = new ExerciseTrueFalse(lesson, new JSONObject(EXERCISE_TRUE_FALSE_JSON));

        assertEquals(lesson, trueFalse.getLesson());
        assertEquals("This test will pass.", trueFalse.getQuestion());
        assertTrue(trueFalse.getSolution());
    }

    @Test
    public void canCreateAMultipleChoiceExercise() throws Exception {
        ExerciseMultipleChoice multipleChoice = new ExerciseMultipleChoice(lesson, new JSONObject(EXERCISE_MULTIPLE_CHOICE_JSON));

        assertEquals(lesson, multipleChoice.getLesson());
        assertEquals("Option A and C are the solutions.", multipleChoice.getQuestion());
        JSONArray options = new JSONArray(multipleChoice.getOptions());
        JSONArray solutions = new JSONArray(multipleChoice.getSolutions());
        assertEquals("Option A", options.get(0));
        assertEquals(true,       solutions.get(0));
        assertEquals("Option B", options.get(1));
        assertEquals(false,      solutions.get(1));
        assertEquals("Option C", options.get(2));
        assertEquals(true,       solutions.get(2));
    }
}

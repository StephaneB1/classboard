/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : common-utils.js
 * Description : Utils for the web application (client side)
 */

// Break in progress
const com_break_in_progress = {
    time_left: document.getElementById('time-left'),
};

const classroom_panel = {
    empty_classroom: document.getElementById('empty-classroom'),
    students_grid: document.getElementById('student-grid')
}

/**
 * Update the classroom display for students (can't click on any students to talk to them)
 * @param classroom : Array of groups from the classroom
 * @param teacher   : Boolean if from teacher dashboard
 */
function updateClassroomDisplay(classroom, teacher) {
    console.log(classroom);
    let groups = classroom;
    let classroomHtml = "";

    if(groups.length === 0 && teacher) {
        classroom_panel.empty_classroom.style.display = 'flex';
    } else {
        if(teacher) {
            classroom_panel.empty_classroom.style.display = 'none';
        }

        groups.forEach(group => {
            if(group.length === 1) {
                classroomHtml += "<div class='students-pair-alone'>";
                classroomHtml += getStudentHtml(group[0]);
            } else if(group.length === 2) {
                classroomHtml += "<div class='students-pair'>";
                classroomHtml += getStudentHtml(group[0]);
                classroomHtml += getStudentHtml(group[1]);
            }
            classroomHtml += "</div>"
        });
    }

    // Update classroom display if in the right tab
    if(classroom_panel.students_grid !== null)
        classroom_panel.students_grid.innerHTML = classroomHtml;
}

function getStudentHtml(s) {
    return (typeof student_fullname !== 'undefined' && student_fullname === s.name ?
        "<div id='me' class='student' title='" + s.name + "'>" + getInitials(s.name) + "</div>" :
        "<div         class='student' title='" + s.name + "'>" + getInitials(s.name) + "</div>");
}

function getInitials(name) {
    const fullName = name.split(' ');
    const initials = fullName.shift().charAt(0) + fullName.pop().charAt(0);
    return initials.toUpperCase();
}

/**
 * Show the "break in progress" panel
 * @param endTime     : time the break ends
 * @param endFunction : function called at the end of the break
 */
function showBreakInProgress(endTime, endFunction) {
    // Parse end time into 3 variables (hour, minute, second)
    const endHour = parseInt(endTime.substring(0, endTime.indexOf(":")));
    endTime = endTime.substring(endTime.indexOf(":") + 1); // HH:mm:ss => mm:ss
    const endMinute = parseInt(endTime.substring(0, endTime.indexOf(":")));
    endTime = endTime.substring(endTime.indexOf(":") + 1); // mm:ss => ss
    const endSecond = parseInt(endTime);

    // Transform time input in js date
    const today = new Date();
    const countDownTime = new Date(today.getFullYear(), today.getMonth(), today.getDate(),
        endHour, endMinute, endSecond).getTime();

    // Countdown
    updateBreakTimeText(countDownTime);
    let countdown = setInterval(() => {
        const values = updateBreakTimeText(countDownTime);
        if(values[0] <= 0 && values[1] <= 0) {
            clearInterval(countdown);
            endFunction();
        }
    }, 1000);
}

function updateBreakTimeText(end) {
    let now = new Date().getTime();
    let distance = end - now;

    // Time format
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    com_break_in_progress.time_left.innerText =
        (minutes < 10 ? "0" : "") + minutes + ":" +
        (seconds < 10 ? "0" : "") + seconds;

    return [minutes, seconds];
}

function sendMessageToStudent(sender_id, recipient_id, type, content) {
    // Send to student
    stompChatClient.send(chat_channel_out, {}, JSON.stringify({
        'content': content,
        'type': type,
        'sender_id': sender_id,
        'recipient_id': recipient_id,
        'date': new Date()
    }));
}

function sendMessageToStudents(lesson_id, student_id, type, content) {
    stompStudentsClient.send(student_channel_out + lesson_id, {}, JSON.stringify({
        'content': content,
        'type': type,
        'date': new Date(),
        'sender_id': student_id
    }));
}

function initTheme() {
    const currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;

    if (currentTheme) {
        document.documentElement.setAttribute('data-theme', currentTheme);
    }
}

function addNewMultipleChoiceOption(text, isChecked, editable) {
    const optionsContainer = document.getElementById('multiplechoice-options');

    // Option container
    const newOptionIndex = optionsContainer.children.length + 1;
    const newOptionId = "option" + newOptionIndex;
    const newOption = document.createElement('div');
    newOption.id = newOptionId;
    newOption.classList.add("multiplechoice-option");
    if(!editable)
        newOption.classList.add("student-options");


    // Checkbox
    newOption.appendChild(generateCheckboxHTML(newOptionId, isChecked));

    if(editable) {
        // Input text
        const inputText = document.createElement("input");
        inputText.id = newOptionId + "-text";
        inputText.type = "text";
        inputText.placeholder = "Option " + newOptionIndex;
        inputText.autocomplete = "off";
        if(text !== "")
            inputText.value = text;
        inputText.required = true;
        newOption.appendChild(inputText);

        // Delete button
        const deleteButton = document.createElement("button");
        deleteButton.onclick = function () {
            deleteMultipleChoiceOption(newOptionId);
        }
        const deleteIcon = document.createElement("img");
        deleteIcon.classList.add("delete-icon");
        deleteIcon.style.height = "40px";
        deleteIcon.style.width = "40px";
        deleteIcon.src = "/imgs/ic_cross.png";
        deleteIcon.alt = "Delete";
        deleteButton.appendChild(deleteIcon);
        newOption.appendChild(deleteButton);
    } else {
        // Text
        const textDiv = document.createElement("div");
        textDiv.id = newOptionId + "-text";
        textDiv.innerText = text;
        newOption.appendChild(textDiv);
    }

    optionsContainer.prepend(newOption);
}

function generateCheckboxHTML(id, checked) {
    const label = document.createElement("label");
    label.style.left = "20px";
    label.style.top = "-10px";
    label.style.right = "unset";
    label.classList.add("checkbox");
    label.classList.add("bounce");
    const checkbox = document.createElement("input");
    checkbox.id = id + "-check";
    checkbox.type = "checkbox";
    checkbox.checked = checked;

    label.appendChild(checkbox);
    label.appendChild(generateCheckboxDesignHTML());
    return label;
}

function generateCheckboxDesignHTML() {
    const checkmark = document.createElementNS("http://www.w3.org/2000/svg","svg");
    checkmark.setAttribute("viewBox", "0 0 40 40");
    const checkline = document.createElementNS("http://www.w3.org/2000/svg","polyline");
    checkline.setAttribute("points", "10 20 17 28.5 32 12");
    checkmark.appendChild(checkline);

    return checkmark;
}

// Modified version of bvgheluwe's date conversion to keep original timezone
// This is to disable auto-conversion to timezone-free format from JSON.stringify
Date.prototype.toJSON = function () {
    let timezoneOffsetInHours = -(this.getTimezoneOffset() / 60); //UTC minus local time

    //It's a bit unfortunate that we need to construct a new Date instance
    //(we don't want _this_ Date instance to be modified)
    let correctedDate = new Date(this.getFullYear(), this.getMonth(),
        this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds(),
        this.getMilliseconds());
    correctedDate.setHours(this.getHours() + timezoneOffsetInHours);

    return correctedDate.toISOString().replace('Z', '');
}
package com.bottin.classboard.models.logs;

import com.bottin.classboard.models.Lesson;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonCreationLog.java
 * Description : Model for logging all lesson creation
 */
public class LessonCreationLog extends Log {
    public LessonCreationLog(Lesson lesson) {
        super(lesson.getCreatedDate(),
                "LESSON CREATION",
                "Lesson " + lesson.getSubject() + " started.",
                "var(--creationLogColor)");
    }

}

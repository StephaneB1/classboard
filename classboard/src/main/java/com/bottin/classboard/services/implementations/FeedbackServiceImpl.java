package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Feedback;
import com.bottin.classboard.models.FeedbackType;
import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.repositories.FeedbackRepository;
import com.bottin.classboard.repositories.LessonRepository;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.interfaces.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackServiceImpl.java
 * Description : Service implementation for feedbacks
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    // Repositories
    private final LessonRepository lessonRepository;
    private final TeacherRepository teacherRepository;
    private final FeedbackRepository feedbackRepository;

    @Autowired
    public FeedbackServiceImpl(FeedbackRepository feedbackRepository,
                               LessonRepository lessonRepository,
                               TeacherRepository teacherRepository) {
        this.feedbackRepository = feedbackRepository;
        this.lessonRepository = lessonRepository;
        this.teacherRepository = teacherRepository;
    }

    @Override
    public Feedback save(Feedback f) throws Exception {
        return feedbackRepository.save(f);
    }

    @Override
    public Feedback update(Feedback f) throws Exception {
        if(feedbackRepository.findById(f.getId()).isEmpty())
            throw new Exception("Feedback <" + f.getId() + "> doesn't exist.");

        return feedbackRepository.save(f);
    }

    @Override
    public Feedback findById(Long id) throws Exception {
        if(feedbackRepository.findById(id).isEmpty())
            throw new Exception("Feedback <" + id + "> doesn't exist.");

        return feedbackRepository.findById(id).get();
    }

    @Override
    public List<Feedback> findAll() {
        return feedbackRepository.findAll();
    }

    @Override
    public float getLastFeedbackValueFromStudent(FeedbackType type, String studentId) throws Exception {
        if(feedbackRepository.findLastFromStudent(type, studentId).size() == 0)
            return 0;

        List<Feedback> results = feedbackRepository.findLastFromStudent(type, studentId);

        for(Feedback f : results) {
            //System.out.println("Feedback : " + f.getValue() + " / " + f.getCreatedDate());
        }

        return 0.f;// results.get(0).getValue();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(lessonRepository.findById(id).isEmpty())
            throw new Exception("Lesson <" + id + "> doesn't exist.");

        lessonRepository.deleteById(id);
    }

    @Override
    public List<Feedback> getFeedbacksByDate(Lesson lesson) {
        return feedbackRepository.getFeedbacksByDate(lesson.getId());
    }
}

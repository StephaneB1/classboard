package com.bottin.classboard.models;

import com.bottin.classboard.models.events.Activity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.json.JSONObject;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentActivityContent.java
 * Description : Model for a student's content participation for an activity
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class StudentActivityContent {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String content;

    @CreationTimestamp
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY)
    private Activity activity;

    public StudentActivityContent(Student student, Activity activity, String content) {
        this.student  = student;
        this.activity = activity;
        this.content  = content;
    }

    public String getContentResponse() {
        // We send the answer to everyone
        JSONObject result = new JSONObject();
        result.put("student_id", student.getId());
        result.put("type", activity.getSubtype());
        result.put("content", content);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentActivityContent)) return false;
        return this.id != null && this.id.equals(((StudentActivityContent) o).getId());
    }

    @Override
    public String toString() {
        return "StudentActivityContents" + id;
    }
}

package com.bottin.classboard.services.implementations;

import com.bottin.classboard.utils.Utils;
import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.LessonRepository;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.interfaces.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonServiceImpl.java
 * Description : Service implementation for lessons
 */
@Service
public class LessonServiceImpl implements LessonService {

    // Repositories
    private final LessonRepository lessonRepository;
    private final TeacherRepository teacherRepository;

    @Autowired
    public LessonServiceImpl(LessonRepository lessonRepository, TeacherRepository teacherRepository) {
        this.lessonRepository = lessonRepository;
        this.teacherRepository = teacherRepository;
    }

    @Override
    public Lesson save(Lesson l) throws Exception {

        if(l.getSubject().isEmpty())
            throw new Exception("Subject can't be empty");

        // Setting a unique entry endpoint
        String endpoint = "";
        do {
            endpoint = Utils.getRandomString(48 /* '0' */, 122/* 'z' */, 8);
        } while(lessonRepository.doesEndpointExist(endpoint));
        l.setEntryEndpoint(endpoint);

        // Setting an entry code (doesn't need to be unique since the endpoint already is)
        // Code regex : 0-9 A-Z A-Z 0-9 0-9
        String code = Utils.getRandomString(48 /* '0' */, 57 /* '9' */, 1);
        code += Utils.getRandomString(65 /* 'A' */, 90 /* 'Z' */, 2);
        code += Utils.getRandomString(48 /* '0' */, 57 /* '9' */, 2);
        l.setEntryCode(code); // TODO : encryption

        return lessonRepository.save(l);
    }

    @Override
    public void update(Lesson l) throws Exception {
        if(lessonRepository.findById(l.getId()).isEmpty())
            throw new Exception("Lesson <" + l.getId() + "> doesn't exist.");

        lessonRepository.save(l);
    }

    @Override
    public Lesson findById(Long id) throws Exception {
        if(lessonRepository.findById(id).isEmpty())
            throw new Exception("Lesson <" + id + "> doesn't exist.");

        return lessonRepository.findById(id).get();
    }

    @Override
    public Lesson findByEndpoint(String endpoint) throws Exception {
        if(lessonRepository.findByEndpoint(endpoint).isEmpty())
            throw new Exception("Lesson with endpoint <" + endpoint + "> doesn't exist.");

        return lessonRepository.findByEndpoint(endpoint).get();
    }

    @Override
    public List<Lesson> findByUser(Teacher teacher) throws Exception {
        if(teacherRepository.findById(teacher.getUsername()).isEmpty())
            throw new Exception("User <" + teacher.getUsername() + "> doesn't exist.");

        return lessonRepository.findByUser(teacher.getUsername());
    }

    @Override
    public List<Lesson> findAll() {
        return lessonRepository.findAll();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(lessonRepository.findById(id).isEmpty())
            throw new Exception("Lesson <" + id + "> doesn't exist.");

        lessonRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        lessonRepository.deleteAll();
    }

    @Override
    public Long getTotalLessons() {
        return lessonRepository.count();
    }

    @Override
    public List<String> getAttendanceList(Lesson l) {
        return lessonRepository.getAttendanceList(l.getId());
    }

}

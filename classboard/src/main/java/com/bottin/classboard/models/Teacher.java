package com.bottin.classboard.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Teacher.java
 * Description : Model for teachers
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class Teacher {

    @Id
    private String username;

    @Column(nullable = false)
    private String password;

    private String firstName;
    private String lastName;

    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="teacher", orphanRemoval = true)
    private List<Lesson> lessons;

    public Teacher(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        return this.username != null && this.username.equals(((Teacher) o).getUsername());
    }

    @Override
    public String toString() {
        return username;
    }
}

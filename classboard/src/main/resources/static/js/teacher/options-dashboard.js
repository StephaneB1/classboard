/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : options-dashboard.js
 * Description : Client-side handler for options' dashboards
 */

const options = [
    // Access Panel
    {
        self: document.getElementById('option-access'),
        btn:document.getElementById('access-btn')
    },
    // Students list
    {
        self: document.getElementById('option-students-list'),
        btn: document.getElementById('students-list-btn')
    },
    // Logs
    {
        self: document.getElementById('option-logs'),
        btn: document.getElementById('logs-btn')
    }
];

function init() {
    // Init dark or light mode
    initTheme();

    connect().then(function() {

        // QR Code generator
        const link = "https://" + window.location.host + "/" + direct_link;
        new QRCode("qrcode", {
            text: link,
            width: 250,
            height: 250,
            colorDark : "#fff",
            colorLight : "#ffffff00"
        });

        // Remove loader
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function switchTheme() {

    const nightMode = !(localStorage.getItem('theme') === 'night');

    document.documentElement.setAttribute(
        'data-theme', nightMode ? 'night' : 'light');

    // Store for user preferences
    localStorage.setItem('theme', nightMode ? 'night' : 'light');
}


function switchAccessLesson() {
    options[0].self.style.display = "none";
    document.getElementById('loader').style.display = "flex";
    sendMessageToSelf("SWITCH_ACCESS", "{}");
}

function addNewStudent(name, joinedDate, neighbour) {
    // TODO : Responsive data
}

function openPanel(index) {
    let i;
    for(i = 0; i < options.length; i++) {
        if(i === index) {
            options[i].self.style.display = 'flex';
            options[i].btn.classList.add('option-selected');
        } else {
            options[i].self.style.display = "none";
            options[i].btn.classList.remove("option-selected");
        }
    }
}
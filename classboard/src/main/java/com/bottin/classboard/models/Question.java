package com.bottin.classboard.models;

import com.bottin.classboard.utils.Utils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.UpdateTimestamp;
import org.json.JSONObject;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Question.java
 * Description : Model for questions in a lesson
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class Question implements Communicable {

    // Parameters for the front end
    private static final String JSON_QUESTION_KEY = "question";
    private static final String JSON_ANON_KEY = "anon";
    private static final String JSON_DISPLAY_NAME_KEY = "name";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean anonymous;
    private boolean answered;
    private String question;
    private String answer;
    private String displayName;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime sendDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lesson lesson;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

    public Question(Lesson lesson, Student student, LocalDateTime sendDate) {
        this.lesson = lesson;
        this.student = student;
        this.sendDate = sendDate;
        this.answered = false;
    }

    public Question(Lesson lesson, Student student,
                    LocalDateTime sendDate, JSONObject content) throws Exception {
        this(lesson, student, sendDate);

        if(!content.has(JSON_QUESTION_KEY) || !(content.get(JSON_QUESTION_KEY) instanceof String) ||
           !content.has(JSON_ANON_KEY) || !(content.get(JSON_ANON_KEY) instanceof Boolean) ||
           !content.has(JSON_DISPLAY_NAME_KEY) || !(content.get(JSON_DISPLAY_NAME_KEY) instanceof String))
            throw new Exception("Invalid JSON");

        this.question = content.getString(JSON_QUESTION_KEY);
        this.displayName = content.getString(JSON_DISPLAY_NAME_KEY);
        this.anonymous = content.getBoolean(JSON_ANON_KEY);
    }

    public Question(Lesson lesson, Student student, String question,
                    LocalDateTime sendDate, boolean anonymous) {
        this(lesson, student, sendDate);
        this.question = question;
        this.anonymous = anonymous;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        return this.id != null && this.id.equals(((Question) o).getId());
    }

    @Override
    public String toString() {
        return "" + id;
    }

    public String getResponse() {
        JSONObject response = new JSONObject();
        response.put("question", question);
        response.put("student_id", student.getId());
        response.put("answered", answered);
        response.put("name", anonymous ? "Anonymous" : displayName);
        response.put("timestamp", Utils.getTimeLabelFrom(LocalDateTime.now(), sendDate, true));
        response.put("question_id", id);
        return response.toString();
    }
}

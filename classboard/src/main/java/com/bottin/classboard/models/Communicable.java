package com.bottin.classboard.models;

public interface Communicable {
    String getResponse();
}

package com.bottin.classboard.services.implementations;

import com.bottin.classboard.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentDetailsService.java
 * Description : Service for students session authentication (automatic and
 *               anonymous)
 */
@Service
public class StudentDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private StudentService studentService;

    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {

        String password = "";

        try {
            password = studentService.findById(id).getSessionToken();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(password.isEmpty())
            throw new UsernameNotFoundException("Student not found");

        return org.springframework.security.core.userdetails.User.builder()
                .username(id)
                .password(password)
                .roles("STUDENT")
                .build();
    }

}

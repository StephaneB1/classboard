package com.bottin.classboard.models.events;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.StudentActivityContent;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Activity.java
 * Description : Model for activities in a lesson
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public abstract class Activity extends Event {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "activity", orphanRemoval = true)
    private List<StudentActivityContent> answers;

    private ActivityType subtype;

    public Activity(Lesson lesson, ActivityType type) {
        super(lesson, EventType.ACTIVITY);
        this.answers = new ArrayList<>();
        this.subtype = type;
    }

    public abstract String getEndResponse();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Activity)) return false;
        return this.getId() != null && this.getId().equals(((Activity) o).getId());
    }

    @Override
    public List<Log> getLogs() {
        List<Log> logs = new ArrayList<>();

        // Start
        logs.add(new Log(getStartDate(),
                "ACTIVITY STARTED",
                "Activity " + subtype.getTitle() + " started",
                "var(--creationLogColor)"));

        // Student content
        // TODO

        // End
        if(getEndDate() != null) {
            logs.add(new Log(getEndDate(),
                    "ACTIVITY ENDED",
                    "Activity " + subtype.getTitle() + " ended",
                    "var(--endExerciseLogColor)"));
        }

        return logs;
    }
}

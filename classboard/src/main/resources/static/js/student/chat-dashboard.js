/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : questions-dashboard.js
 * Description : Client-side handler for the students' questions panel
 */

const chat_panel = {
    messages: document.getElementById('chat-messages'),
}

function init() {
    // Connect to the app's websockets
    connect().then(function () {

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function showNewMessage(message, asSender) {
    // Update HTML chat
    let messageDiv = document.createElement("div");
    messageDiv.className = asSender ? "message-right" : "message-left";
    messageDiv.innerHTML = message;
    chat_panel.messages.prepend(messageDiv);
}

function sendChatMessageToNeighbour() {
    let messageInput = document.getElementById('chat-text-input');
    const msg = messageInput.value;

    // Update HTML chat
    showNewMessage(msg, true);
    messageInput.value = "";

    // Send to student
    sendMessageToStudent(student_id, null, 'CHAT_MESSAGE', msg);
}
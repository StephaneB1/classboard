package com.bottin.classboard.listeners;

import com.bottin.classboard.models.Message;
import com.bottin.classboard.models.MessageType;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.services.interfaces.LessonService;
import com.bottin.classboard.services.interfaces.StudentService;
import com.bottin.classboard.services.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;

@Component
public class WebSocketEventListener implements ApplicationListener {

    private final StudentService studentService;
    private final TeacherService teacherService;
    private final LessonService lessonService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    // Sessions (with timeout timer)
    private Map<String, Timer> endingSessions = new HashMap<>();

    @Autowired
    public WebSocketEventListener(StudentService studentService, TeacherService teacherService,
                                  LessonService lessonService, SimpMessagingTemplate simpMessagingTemplate) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.lessonService = lessonService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if(applicationEvent instanceof SessionConnectedEvent) {
            // A new connection has been made through a websocket
            Principal user = ((SessionConnectedEvent) applicationEvent).getUser();
            if(user == null) {
                return;
            }

            // Student :
            try {
                Student student = studentService.findById(user.getName());

                // Add the student to the local sessions if not already
                connectToEndSessionStatus(student.getId());
            } catch (Exception ignored) { }

        } else if (applicationEvent instanceof SessionDisconnectEvent) {
            // Someone disconnected from a websocket
            Principal user = ((SessionDisconnectEvent) applicationEvent).getUser();
            if(user == null) {
                return;
            }

            // Student :
            try {
                Student student = studentService.findById(user.getName());

                // Add the student to the local sessions if not already
                disconnectToEndSessionStatus(student.getId());
            } catch (Exception ignored) { }
        }
    }

    /**
     * Renew a session if it was ending
     * @param id : id of the user
     */
    synchronized void connectToEndSessionStatus(String id) {
        if(endingSessions.containsKey(id)) {
            // An ending session has been renewed before the session timeout
            endingSessions.get(id).cancel();
            endingSessions.remove(id);
        }
    }

    /**
     * End a session (disconnection) and start the timeout timer
     * @param id : id of user
     */
    synchronized void disconnectToEndSessionStatus(String id) {
        if(!endingSessions.containsKey(id)) {
            // An ending session has been renewed before the session timeout
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // Session timeout : we remove the student from the lesson
                    endingSessions.remove(id);
                    try {
                        // Soft delete to keep data from past exercises etc...
                        // TODO : studentService.leaveLesson(id); (not reliable enough)
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Notify teacher that the student left
                    Message notification = new Message();
                    notification.setType(MessageType.STUDENT_LEFT);
                    notification.setDate(LocalDateTime.now());
                    notification.setSender_id(id);
                    simpMessagingTemplate.convertAndSend(
                            "/lesson/teacher",
                            notification.toJson());
                    this.cancel();
                }
            }, 10 * 1000); // 10 seconds
            endingSessions.put(id, timer);
        }
    }
}

package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : QuestionRepository.java
 * Description : Repository for questions
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}

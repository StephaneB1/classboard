package com.bottin.classboard.utils;

import com.bottin.classboard.models.Feedback;

import java.util.Comparator;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackDateComparator.java
 * Description : Utils to compare dates between feedbacks
 */
public class FeedbackDateComparator implements Comparator<Feedback> {
    @Override
    public int compare(Feedback o1, Feedback o2) {
        return o1.getCreatedDate().compareTo(o2.getCreatedDate());
    }
}
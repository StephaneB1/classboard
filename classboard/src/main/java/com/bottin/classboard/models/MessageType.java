package com.bottin.classboard.models;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : MessageType.java
 * Description : Enumeration of all types of messages
 */
public enum MessageType {
    // Teacher messages
    SWITCH_ACCESS,
    STUDENT_JOINED,
    STUDENT_LEFT,
    FEEDBACK,
    QUESTION,
    QUESTION_CONFIRMATION,
    ANSWER,
    ACTIVITY_STUDENT_UPDATE,
    ACTIVITY_TEACHER_UPDATE,
    ACTIVITY_RESTART,
    // Students messages
    STUDENT_UPDATE,
    QUESTION_ANSWERED,
    BREAK_START,
    BREAK_END,
    EXERCISE_START,
    EXERCISE_END,
    BOARD_START,
    BOARD_PATH,
    BOARD_END,
    ACTIVITY_START,
    ACTIVITY_END,
    // Chat
    CHAT_MESSAGE,
    PI_CHAT_MESSAGE
}

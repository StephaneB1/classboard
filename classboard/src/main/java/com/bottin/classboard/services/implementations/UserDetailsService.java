package com.bottin.classboard.services.implementations;

import com.bottin.classboard.services.interfaces.StudentService;
import com.bottin.classboard.services.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : UserDetailsService.java
 * Description : Service for teachers' authentication (manual and known)
 */
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final TeacherService teacherService;
    private final StudentService studentService;

    @Autowired
    public UserDetailsService(TeacherService teacherService, StudentService studentService) {
        this.teacherService = teacherService;
        this.studentService = studentService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        String password = "";
        UserDetails loggedInUser = null;

        try {
            password = teacherService.findByUsername(username).getPassword();
            loggedInUser = User.builder()
                    .username(username)
                    .password(password)
                    .roles("TEACHER")
                    .build();
        } catch (Exception ignored) {}

        // Teacher not found, now looking if student
        if(password.isEmpty()) {
            try {
                password = studentService.findById(username).getSessionToken();

                loggedInUser = User.builder()
                        .username(username)
                        .password(password)
                        .roles("STUDENT")
                        .build();
            } catch (Exception e) {
                // Student not found : not a user
                e.printStackTrace();
                throw new UsernameNotFoundException("User not found");
            }
        }

        return loggedInUser;
    }
}

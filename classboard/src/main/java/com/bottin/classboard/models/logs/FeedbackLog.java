package com.bottin.classboard.models.logs;

import com.bottin.classboard.models.Feedback;
import com.bottin.classboard.models.FeedbackType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackLog.java
 * Description : Model for logging all feedbacks
 */
public class FeedbackLog extends Log{

    public FeedbackLog(Feedback feedback) {
        super(feedback.getCreatedDate(),
                "FEEDBACK",
                (feedback.isPositive() ? "Positive" : "Negative") +
                        " feedback of type " + feedback.getType(),
                "var(--feedbackLogColor)");
    }

}

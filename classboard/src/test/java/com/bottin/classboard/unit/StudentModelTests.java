package com.bottin.classboard.unit;

import com.bottin.classboard.models.Feedback;
import com.bottin.classboard.models.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StudentModelTests {
    private final String firstName = "Wonderful";
    private final String lastName  = "Zebra";

    @Test
    public void canCreateAStudentProfile() {
        Student student = new Student(firstName, lastName);

        assertEquals(student.getFirstName(), firstName);
        assertEquals(student.getLastName(), lastName);
        assertEquals(student.getFullName(), firstName + " " + lastName);
        assertNotNull(student.getId());
        assertFalse(student.getId().isEmpty());
    }

    @Test
    public void studentsWithSameNamesAreNotEqual() {
        Student student1 = new Student(firstName, lastName);
        Student student2 = new Student(firstName, lastName);

        assertNotEquals(student1, student2);
    }
}

package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : TeacherService.java
 * Description : Service interface for teachers
 */
@Service
public interface TeacherService {

    // Creation

    /**
     * Save a user into the database
     * @param u : user
     * @throws Exception if something went wrong
     */
    void save(Teacher u) throws Exception;

    // Update

    /**
     * Update an existing user in the database
     * @param u : user
     * @throws Exception if something went wrong
     */
    void update(Teacher u) throws Exception;

    // Search

    /**
     * Find a user by its username (id)
     * @param username : username of the user
     * @return User
     * @throws Exception if something went wrong
     */
    Teacher findByUsername(String username) throws Exception;

    /**
     * Returns all users
     * @return all users
     */
    List<Teacher> findAll();

    // Deletion

    /**
     * Delete a user from the database
     * @param username : username of the user to delete
     * @throws Exception if something went wrong
     */
    void delete(String username) throws Exception;

    /**
     * Format database of all user
     */
    void deleteAll();

}

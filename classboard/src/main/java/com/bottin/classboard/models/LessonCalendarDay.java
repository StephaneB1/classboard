package com.bottin.classboard.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonCalendarDay.java
 * Description : Model for lesson display per days
 */
@AllArgsConstructor
@Data
public class LessonCalendarDay {
    private List<Lesson> lessons;
    private int dayOfMonth;
    private boolean inMonth;

    public LessonCalendarDay(int dayOfMonth, boolean inMonth) {
        this.dayOfMonth = dayOfMonth;
        this.inMonth = inMonth;
        this.lessons = new ArrayList<>();
    }

    public void addLesson(Lesson lesson) {
        this.lessons.add(lesson);
    }
}

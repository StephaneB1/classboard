package com.bottin.classboard.models.events.activities;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentActivityContent;
import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.ActivityType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.swing.*;
import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ActivityPeerInstruction.java
 * Description : Model for a peer instruction activity in a lesson
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public class ActivityPeerInstruction extends Activity {

    private static final int MAX_GROUP_SIZE = 4;
    private static final int MIN_GROUP_SIZE = 2;

    // Parameters for the front end
    private static final String JSON_QUESTION_KEY  = "question";
    private static final String JSON_OPTIONS_KEY   = "options";
    private static final String JSON_SOLUTIONS_KEY = "solutions";
    private static final String JSON_SCORES_KEY    = "scores";

    private String question = "";
    private String options = ""; // JSON array of options
    private String solutions = ""; // JSON array of solutions,
    // eg. [true, false] : option 1 is true, option 2 is false

    private String scores = ""; // JSON array of scores per round (0.5, 0.75, ...)

    @Column(name="discussion_groups", columnDefinition="LONGTEXT")
    private String discussionGroups = ""; // JSON array of groups of students per round
                                          // ([[AA, BB], [CC, DD, EE], ...])
    @Transient
    private List<List<GroupMember>> groups;

    public ActivityPeerInstruction(ActivityPeerInstruction clone) {
        super(clone.getLesson(), ActivityType.PEER_INSTRUCTION);

        this.question = clone.getQuestion();
        this.options = clone.getOptions();
        this.solutions = clone.getSolutions();
        this.discussionGroups = clone.getDiscussionGroups();
        this.scores = clone.getScores();
    }

    public ActivityPeerInstruction(Lesson lesson, JSONObject content) throws Exception {
        super(lesson, ActivityType.PEER_INSTRUCTION);

        if(!content.has(JSON_QUESTION_KEY) || !(content.get(JSON_QUESTION_KEY) instanceof String)  ||
           !content.has(JSON_OPTIONS_KEY) || !(content.get(JSON_OPTIONS_KEY) instanceof JSONArray) ||
           !content.has(JSON_SOLUTIONS_KEY) || !(content.get(JSON_SOLUTIONS_KEY) instanceof JSONArray))
            throw new Exception("Invalid JSON");

        this.question = content.getString(JSON_QUESTION_KEY);
        this.options = content.getJSONArray(JSON_OPTIONS_KEY).toString();
        this.solutions = content.getJSONArray(JSON_SOLUTIONS_KEY).toString();
    }

    public float getCurrentAverageScore() {
        float result = 0.0f;
        float totalStudents = getLesson().getStudents().size();

        for(StudentActivityContent answer : getAnswers()) {
            result += getAnswersScore(answer) / totalStudents;
        }

        return result;
    }

    public List<Float> getAllScores() {
        List<Float> result = new ArrayList<>();

        for(StudentActivityContent answer : getAnswers()) {
            result.add(getAnswersScore(answer));
        }

        return result;
    }

    public List<String> getOptionsList() {
        List<String> result = new ArrayList<>();
        JSONArray parsedOptions = new JSONArray(options);

        for (int i = 0; i < parsedOptions.length(); i++) {
            result.add(parsedOptions.getString(i));
        }

        return result;
    }

    private float getAnswersScore(StudentActivityContent answer) {
        return getGroupAnswersScore(new ArrayList<>(Arrays.asList(answer)));
    }

    public float getGroupAnswersScore(List<StudentActivityContent> answersList) {
        float result = 0.0f;
        JSONArray sols = new JSONArray(solutions);

        for(StudentActivityContent answerObj : answersList) {
            JSONArray answers = new JSONArray(answerObj.getContent());
            for(int i = 0; i < answers.length(); i++) {
                if (answers.getBoolean(i) == sols.getBoolean(i)) {
                    result += 100f / (answers.length() * answersList.size());
                }
            }
        }

        return result;
    }

    public void generatePeerDiscussionGroups() {
        List<StudentActivityContent> answers = this.getAnswers();
        if(answers.size() < 2)
            return;

        // First, we get the total group with sizes so that : 1 < group size < 5
        int total_groups = (answers.size() + MAX_GROUP_SIZE - 1) / MAX_GROUP_SIZE;
        this.groups = new ArrayList<>();
        for(int i = 0; i < total_groups; i++) {
            this.groups.add(new ArrayList<>());
        }

        // To make sure there is a good distribution within groups, we order
        // the students by their answer's average (eg. [100%, 98%, 78%, ... 20%, 17%, 0%])
        answers.sort(Comparator.comparing(this::getAnswersScore));
        Collections.reverse(answers);

        // and then we place them one by one in a different group each time
        int group_index = 0;
        for (StudentActivityContent answer : answers) {
            GroupMember student = new GroupMember(
                    answer.getStudent().getId(),
                    answer.getStudent().getFullName(),
                    getAnswersScore(answer)
            );
            this.groups.get(group_index).add(student);
            group_index = (group_index + 1) % total_groups;
        }

        convertGroupsToDiscussion();
    }

    public void resetDiscussionGroups() {
        this.discussionGroups = "";
    }

    public List<List<String>> getDiscussionGroupsNames() {
        if(this.discussionGroups.isEmpty())
            return new ArrayList<>();

        List<List<String>> result = new ArrayList<>();
        generateGroupList();

        for(int i = 0; i < this.groups.size(); i++) {
            result.add(new ArrayList<>());
            for (GroupMember student : groups.get(i)) {
                String name = student.getName();
                if(student.getScore() > 70)
                    name = "$" + name;
                result.get(i).add(name);
            }
        }

        return result;
    }

    public List<List<String>> getDiscussionGroupsIds() {
        if(this.discussionGroups.isEmpty())
            return new ArrayList<>();

        List<List<String>> result = new ArrayList<>();
        generateGroupList();

        for(int i = 0; i < groups.size(); i++) {
            result.add(new ArrayList<>());
            for(GroupMember student : groups.get(i))
                result.get(i).add(student.getId());
        }

        return result;
    }

    public List<GroupMember> getGroupMembersList(String studentId) {
        if(this.discussionGroups.isEmpty())
            return new ArrayList<>();

        List<GroupMember> members = new ArrayList<>();
        boolean found = false;
        generateGroupList();

        for (List<GroupMember> group : this.groups) {
            // Find the group the student is in
            for (GroupMember student : group)
                if (student.getId().equals(studentId)) {
                    found = true;
                    break;
                }

            // Add the other members from that group
            if (found)
                for (GroupMember student : group)
                    if (!student.getId().equals(studentId))
                        members.add(student);
        }

        return members;
    }

    public List<String> getGroupMembersIdList(String studentId) {
        List<GroupMember> members = getGroupMembersList(studentId);
        List<String> result = new ArrayList<>();

        for(GroupMember member : members)
            result.add(member.getId());

        return result;
    }

    public String getGroupMembers(String studentId) {
        List<GroupMember> members = getGroupMembersList(studentId);
        String result = "";

        for(GroupMember member : members)
            result += member.getName() + " ";

        return result;
    }

    private void generateGroupList() {
        JSONArray tmpGroups = new JSONArray(this.discussionGroups);
        this.groups = new ArrayList<>();
        for(int i = 0; i < tmpGroups.length(); i++) {
            this.groups.add(new ArrayList<>());
            JSONArray members = tmpGroups.getJSONArray(i);
            for(int j = 0; j < members.length(); j++) {
                JSONObject member = members.getJSONObject(j);
                this.groups.get(i).add(new GroupMember(
                        member.getString("id"),
                        member.getString("name"),
                        (float) member.getDouble("score")
                ));
            }
        }
    }

    private void convertGroupsToDiscussion() {
        JSONArray tmpDiscussionGroup = new JSONArray();
        for(List<GroupMember> group : this.groups) {
            JSONArray members = new JSONArray();
            for(GroupMember member : group) {
                members.put(member.toJson());
            }
            tmpDiscussionGroup.put(members);
        }

        this.discussionGroups = tmpDiscussionGroup.toString();
    }

    @Override
    public String getResponse() {
        JSONObject result = new JSONObject();
        result.put("id", this.getId());
        result.put("type", this.getSubtype());
        result.put("title", this.getSubtype().getTitle());
        result.put("question", question);
        result.put("options", options);
        result.put("solutions", solutions);
        return result.toString();
    }

    @Override
    public String getEndResponse() {
        JSONObject result = new JSONObject();
        result.put("type", getSubtype());
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActivityPeerInstruction)) return false;
        return this.getId() != null && this.getId().equals(((ActivityPeerInstruction) o).getId());
    }

    static class GroupMember {
        private final String name;
        private final String id;
        private final float score;

        public GroupMember(String id, String name, float score) {
            this.name = name;
            this.id = id;
            this.score = score;
        }

        public String getId() {
            return id;
        }

        public float getScore() {
            return score;
        }

        public String getName() {
            return name;
        }

        public JSONObject toJson() {
            JSONObject student = new JSONObject();
            student.put("id", id);
            student.put("name", name);
            student.put("score", (double) score);
            return student;
        }
    }
}

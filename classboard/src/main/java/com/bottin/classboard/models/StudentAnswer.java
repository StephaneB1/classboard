package com.bottin.classboard.models;

import com.bottin.classboard.models.events.Exercise;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.json.JSONObject;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentAnswer.java
 * Description : Model for a student's answer for an exercise
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class StudentAnswer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String answer;

    @CreationTimestamp
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY)
    private Exercise exercise;

    public StudentAnswer(Student student, Exercise exercise, String answer) {
        this.student  = student;
        this.exercise = exercise;
        this.answer   = answer;
    }

    public String getAnswerResponse() {
        // We send the answer to everyone
        JSONObject result = new JSONObject();
        result.put("type", exercise.getExerciseType());
        result.put("answer", answer);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentAnswer)) return false;
        return this.id != null && this.id.equals(((StudentAnswer) o).getId());
    }

    @Override
    public String toString() {
        return "StudentAnswer" + id;
    }
}

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : register.js
 * Description : Reactive helpers when registering
 */

const password = document.getElementById('pass1');
const passwordConfirmation = document.getElementById('pass2');

function onChangePasswordInput() {
    if(password.value.length >= 8) {
        passwordConfirmation.disabled = false;
        passwordConfirmation.title = "Password must be at least 8 characters";
    } else {
        passwordConfirmation.disabled = true;
        passwordConfirmation.title = "Please fill in this field.";
    }
}
package com.bottin.classboard.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

import java.time.LocalDateTime;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Message.java
 * Description : Model for basic messages in a lesson (private or broadcast)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private String content;
    private MessageType type;
    private String sender_id;
    private String recipient_id;
    private LocalDateTime date;

    public String toJson() {
        JSONObject result = new JSONObject();
        result.put("content", content);
        result.put("type", type);
        result.put("sender_id", sender_id);
        result.put("recipient_id", recipient_id);
        result.put("date", date);
        return result.toString();
    }
}

package com.bottin.classboard.repositories.activities;

import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ActivityVideoRepository.java
 * Description : Repository for video activities
 */
@Repository
public interface ActivityPeerInstructionRepository extends JpaRepository<ActivityPeerInstruction, Long> {
}

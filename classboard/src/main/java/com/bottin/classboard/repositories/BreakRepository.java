package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.events.BreakEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : BreakRepository.java
 * Description : Repository for breaks
 */
@Repository
public interface BreakRepository extends JpaRepository<BreakEvent, Long> {

    @Query("SELECT b FROM BreakEvent b WHERE b.lesson.id = ?1 AND b.endDate IS NULL")
    Optional<BreakEvent> findOnGoingBreak(Long lesson_id);

}

package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentActivityContent;
import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ActivityService.java
 * Description : Service interface for activities
 */
@Service
public interface ActivityService {

    // Creation
    Activity save(Activity a) throws Exception;

    // Update
    Activity update(Activity a) throws Exception;

    // Search
    Activity findById(Long id) throws Exception;
    ActivityVideo findVideoById(Long id) throws Exception;
    ActivityPeerInstruction findPeerInstructionById(Long id) throws Exception;
    List<ActivityVideo> findAll();

    // Deletion
    void delete(Long id) throws Exception;

    // Utils
    StudentActivityContent addNewPeerInstructionAnswer(Student student, Activity activity, JSONArray answers) throws Exception ;
    StudentActivityContent addNewVideoProgress(Student student, Activity activity, int progress) throws Exception ;

}

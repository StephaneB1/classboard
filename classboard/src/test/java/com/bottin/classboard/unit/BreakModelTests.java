package com.bottin.classboard.unit;

import com.bottin.classboard.models.events.BreakEvent;
import com.bottin.classboard.models.Lesson;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BreakModelTests {

    private static final int MINUTES = 10;

    @Test
    public void canCreateABreak() {
        BreakEvent newBreak = new BreakEvent(new Lesson(), MINUTES);
        assertEquals(MINUTES, newBreak.getDurationMinutes());
    }
}

package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.Exercise;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseService.java
 * Description : Service interface for exercises
 */
@Service
public interface ExerciseService {

    // Creation
    Exercise save(Exercise e) throws Exception;

    // Update
    Exercise update(Exercise e) throws Exception;

    // Search
    Exercise findById(Long id) throws Exception;
    ExerciseTrueFalse findTrueFalseById(Long id) throws Exception;
    ExerciseMultipleChoice findMultipleChoiceById(Long id) throws Exception;

    // Deletion
    void delete(Long id) throws Exception;

    // Utils
    StudentAnswer addNewTrueFalseAnswer(Student student, ExerciseTrueFalse exercise, boolean answer) throws Exception;
    StudentAnswer addNewMultipleChoiceAnswer(Student student, ExerciseMultipleChoice exercise, JSONArray answers) throws Exception;
    int getTotalCorrectAnswers(Exercise exercise);
    int getTotalWrongAnswers(Exercise exercise);
    List<String> getAnswers(Exercise exercise);
}

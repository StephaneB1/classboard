package com.bottin.classboard.models.events.exercises;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.Exercise;
import com.bottin.classboard.models.events.ExerciseType;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseMultipleChoice.java
 * Description : Model for multiple choice exercises in a lesson
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public class ExerciseMultipleChoice extends Exercise {

    // Parameters for the front end
    private static final String JSON_QUESTION_KEY  = "question";
    private static final String JSON_OPTIONS_KEY   = "options";
    private static final String JSON_SOLUTIONS_KEY = "solutions";

    private String question;
    private String options; // JSON array of options
    private String solutions; // JSON array of solutions,
                              // eg. [true, false] : option 1 is true, option 2 is false

    public ExerciseMultipleChoice(Lesson lesson, JSONObject content) throws Exception {
        super(lesson, ExerciseType.MULTIPLE_CHOICE);

        if(!content.has(JSON_QUESTION_KEY) || !(content.get(JSON_QUESTION_KEY) instanceof String)  ||
           !content.has(JSON_OPTIONS_KEY) || !(content.get(JSON_OPTIONS_KEY) instanceof JSONArray) ||
           !content.has(JSON_SOLUTIONS_KEY) || !(content.get(JSON_SOLUTIONS_KEY) instanceof JSONArray))
            throw new Exception("Invalid JSON");

        this.question = content.getString(JSON_QUESTION_KEY);
        this.options = content.getJSONArray(JSON_OPTIONS_KEY).toString();
        this.solutions = content.getJSONArray(JSON_SOLUTIONS_KEY).toString();
    }

    public List<String> getOptionsList() {
        List<String> result = new ArrayList<>();
        JSONArray parsedOptions = new JSONArray(options);

        for (int i = 0; i < parsedOptions.length(); i++) {
            result.add(parsedOptions.getString(i));
        }

        return result;
    }

    @Override
    public String getResponse() {
        JSONObject result = new JSONObject();
        result.put("id", getId());
        result.put("title", getExerciseType().getTitle());
        result.put("type", getExerciseType());
        result.put("question", question);
        result.put("options", options);
        return result.toString();
    }

    @Override
    public String getSolutionResponse() {
        // We send the results to everyone
        JSONObject result = new JSONObject();
        result.put("type", getExerciseType());
        result.put("question", question);
        result.put("options", options);
        result.put("solutions", solutions);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExerciseTrueFalse)) return false;
        return this.getId() != null && this.getId().equals(((ExerciseTrueFalse) o).getId());
    }
}

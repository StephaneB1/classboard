package com.bottin.classboard.controllers;

import com.bottin.classboard.models.*;
import com.bottin.classboard.services.interfaces.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : DashboardApiController.java
 * Description : Controller for the feedbacks chart.
 */
@RestController
@RequestMapping("/feedbacks")
@SessionAttributes("loggedInUsername")
public class DashboardApiController {

    private static final int NOW_SECONDS_LENGTH           = 60;
    private static final DateTimeFormatter labelFormatter = DateTimeFormatter.ofPattern("HH:mm");

    private final FeedbackService feedbackService;
    private final LessonService lessonService;

    @Autowired
    public DashboardApiController(LessonService lessonService, FeedbackService feedbackService) {
        this.lessonService   = lessonService;
        this.feedbackService = feedbackService;
    }

    @GetMapping(path = "/{lesson_id}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getLessonFeedbacks(@PathVariable Long lesson_id,
                                                     HttpSession session) {

        try {
            // Get lesson and logged in teacher
            String username = (String) session.getAttribute("loggedInUsername");
            Lesson lesson = lessonService.findById(lesson_id);

            if(!lesson.getTeacher().getUsername().equals(username))
                return ResponseEntity.badRequest().body("Authorization failure");

            // Return all feedbacks from the lesson
            return ResponseEntity.ok(getGraphData(lesson.getCreatedDate(), lesson.getEndDate(),
                    lesson.getStudents()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping(path = "/{lesson_id}/now", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getLessonFeedbacksNow(@PathVariable Long lesson_id,
                                                        HttpSession session) {

        try {
            // Get lesson and logged in teacher
            String username = (String) session.getAttribute("loggedInUsername");
            Lesson lesson = lessonService.findById(lesson_id);

            if(!lesson.getTeacher().getUsername().equals(username))
                return ResponseEntity.badRequest().body("Authorization failure");

            // Get graph data between now and a minute ago
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime start = lesson.getCreatedDate();
            Duration duration = Duration.between(start, now);
            if(duration.toSeconds() > NOW_SECONDS_LENGTH) {
                start = now.minusSeconds(NOW_SECONDS_LENGTH);
            }

            return ResponseEntity.ok(getGraphData(start, LocalDateTime.now(), lesson.getStudents()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public String getGraphData(LocalDateTime startTime, LocalDateTime endTime, List<Student> students) {

        LocalDateTime now = LocalDateTime.now();
        // System.out.println("================== " + now.truncatedTo(ChronoUnit.SECONDS).toString() + " =====================");

        JSONObject result               = new JSONObject();
        JSONArray labels                = new JSONArray();
        JSONArray dataMotivationJson    = new JSONArray();
        JSONArray dataVitalityJson      = new JSONArray();
        JSONArray dataClarityJson       = new JSONArray();
        JSONArray dataInteractivityJson = new JSONArray();

        // First label on the left, then add one every minute
        labels.put(startTime.format(labelFormatter));

        // Get all feedbacks from each students as a map with the time of the feedback
        // and the value in the range of [0; 100 * totalStudents]
        ArrayList<Map<LocalDateTime, Float>> feedbacksData = getStudentsFeedbackData(students);
        float[] currentValues = new float[4];
        Arrays.fill(currentValues, 50.0f);

        // Set the start value depending on the last feedback value before the start time
        for(int i = 0; i < feedbacksData.size(); i++) {
            for (Map.Entry<LocalDateTime, Float> entry : feedbacksData.get(i).entrySet()) {
                if(entry.getKey().truncatedTo(ChronoUnit.SECONDS).isBefore(
                        startTime.truncatedTo(ChronoUnit.SECONDS))) {
                    currentValues[i] += entry.getValue();
                }
            }
        }

        // Generate the chart feedback data from the start of the lesson to now
        LocalDateTime currentTime = startTime.truncatedTo(ChronoUnit.SECONDS);
        while(currentTime.isBefore(now.truncatedTo(ChronoUnit.SECONDS))) {

            // Add time label every second to get a proportionate display
            labels.put(getTimeLabel(startTime));

            for(int i = 0; i < feedbacksData.size(); i++) {
                if(feedbacksData.get(i).containsKey(currentTime)) {
                    currentValues[i] += feedbacksData.get(i).get(currentTime);

                    // Add the data into the different categories (normalized)
                    currentValues[i] = currentValues[i];
                }
            }

            dataMotivationJson.put(currentValues[0]);
            dataVitalityJson.put(currentValues[1]);
            dataClarityJson.put(currentValues[2]);
            dataInteractivityJson.put(currentValues[3]);

            // Go to the next second
            currentTime = currentTime.plusSeconds(1);
        }

        // Add everything in the response json
        result.put("labels", labels);
        result.put("data_motivation", dataMotivationJson);
        result.put("data_vitality", dataVitalityJson);
        result.put("data_clarity", dataClarityJson);
        result.put("data_interactivity", dataInteractivityJson);

        return result.toString();
    }

    private ArrayList<Map<LocalDateTime, Float>> getStudentsFeedbackData(List<Student> students) {
        ArrayList<Map<LocalDateTime, Float>> result = new ArrayList<>();
        for (int i = 0; i < 4; i++)
            result.add(new HashMap<>());

        // Get all feedback values per students and per time (truncated to the second)
        for(Student student : students) {
            ArrayList<Map<LocalDateTime, Float>> studentFeedbacks = student.getFeedbackVariationMap(students.size());

            // Add all the values into the global map (and add them up if feedbacks at the same time)
            for(int i = 0; i < studentFeedbacks.size(); i++) {
                final int index = i;
                studentFeedbacks.get(i).forEach((key, value) -> result.get(index).merge(key, value, Float::sum));
            }
        }

        return result;
    }

    private String getTimeLabel(LocalDateTime t) {
        return t.getSecond() == 0 ? t.format(labelFormatter) : "";
    }
}

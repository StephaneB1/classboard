/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : common-teacher.js
 * Description : Utils for the web application for the teacher (client side)
 */


const ongoing_notification = {
    break: document.getElementById('ongoing-break'),
    exercise: document.getElementById('ongoing-exercise'),
    activity: document.getElementById('ongoing-activity')
}

function deleteMultipleChoiceOption(id) {
    const options = exercise_edits.multipleChoice.optionsList.children;

    // List to rebuild options with proper index
    let postOptionsChecked = [];
    let postOptionsText    = [];

    // Save options except the one we want to delete
    let i;
    for (i = 0; i < options.length; i++) {
        if(options[i].id !== id) {
            postOptionsChecked.push(document.getElementById(options[i].id + "-check").checked);
            postOptionsText.push(document.getElementById(options[i].id + "-text").value);
        }
    }

    // Delete all
    exercise_edits.multipleChoice.optionsList.innerHTML = "";

    // Rebuild without deleted option (taking into account reverse-column display)
    for (i = postOptionsChecked.length - 1; i >= 0; i--) {
        addNewMultipleChoiceOption(postOptionsText[i], postOptionsChecked[i], true);
    }
}

function startMultipleChoice(asPeerInstruction) {

    const question = exercise_edits.multipleChoice.questionInput.value;

    if(question === "") {
        // TODO : displayErrorMessage("...") in common utils like a small alert on the corner of the screen
        return;
    }


    const optionsDiv = exercise_edits.multipleChoice.optionsList.children;

    if(optionsDiv.length <= 0)
        return; // TODO : Display error message

    let solutions = [];
    let options = [];
    let i;
    for (i = optionsDiv.length - 1; i >= 0; i--) {
        solutions.push(document.getElementById(optionsDiv[i].id + "-check").checked);
        const option = document.getElementById(optionsDiv[i].id + "-text").value;
        if(option === "") {
            return; // TODO : Display error message
        } else {
            options.push(option);
        }
    }

    // Display loader
    if(asPeerInstruction) {
        activity_edits.peerInstruction.self.style.display = "none";
        ongoing_notification.activity.style.display = "block";
    } else {
        exercise_edits.multipleChoice.self.style.display = "none";
        ongoing_notification.exercise.style.display = "block";
    }
    document.getElementById('loader').style.display = "flex";

    sendMessageToStudentsFromTeacher(asPeerInstruction ? 'ACTIVITY_START' : 'EXERCISE_START',
        JSON.stringify({
            type: asPeerInstruction ? "PEER_INSTRUCTION" : "MULTIPLE_CHOICE",
            question: question,
            options: options,
            solutions: solutions
    }));
}

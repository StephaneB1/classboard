// Free drawing path
var path;

function onMouseDown(event) {
    // Create a new path and set its stroke color to black:
    path = new Path({
        segments: [event.point],
        strokeColor: document.getElementById('currentColor').value,
        strokeWidth: document.getElementById('penSize').value,
        strokeCap: 'round',
        strokeJoin: 'round',
        fullySelected: false
    });
}

// While the user drags the mouse, points are added to the path
// at the position of the mouse:
function onMouseDrag(event) {
    path.add(event.point);
}

// When the mouse is released, we simplify the path:
function onMouseUp(event) {
    // When the mouse is released, simplify it:
    path.simplify(10);

    // Send the simplified segment to all students
    sendDrawPathSegmentsToStudents(path.exportJSON());
}

function getSVG() {
    return path.exportSVG( { asString:true } );
}
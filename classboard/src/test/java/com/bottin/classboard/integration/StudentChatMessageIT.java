package com.bottin.classboard.integration;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentChatMessage;
import com.bottin.classboard.repositories.StudentChatMessageRepository;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.services.implementations.StudentChatMessageServiceImpl;
import com.bottin.classboard.services.implementations.StudentServiceImpl;
import com.bottin.classboard.services.interfaces.StudentChatMessageService;
import com.bottin.classboard.services.interfaces.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
//@ActiveProfiles("dev") // For local tests only
class StudentChatMessageIT {

	@TestConfiguration
	static class OrganisationServiceImplTestContextConfiguration {
		@Bean
		public StudentService studentService(StudentRepository studentRepository) {
			return new StudentServiceImpl(studentRepository);
		}

		@Bean
		public StudentChatMessageService chatMessageService(StudentRepository studentRepository,
															StudentChatMessageRepository chatMessageRepository) {
			return new StudentChatMessageServiceImpl(chatMessageRepository, studentRepository);
		}
	}

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private StudentChatMessageRepository studentChatMessageRepository;

	@Autowired
	private StudentChatMessageService chatMessageService;

	@Autowired
	private StudentService studentService;

	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(studentRepository).isNotNull();
		assertThat(studentChatMessageRepository).isNotNull();
		assertThat(chatMessageService).isNotNull();
		assertThat(studentService).isNotNull();
	}

	@Test
	void canSendMessageToAnotherStudent() throws Exception {
		Student sender = studentService.save(new Student("Magnificent", "Koala"));
		Student receiver =  studentService.save(new Student("Impressive", "Wombat"));

		// Student A sends a message to student B
		StudentChatMessage chatMsg = chatMessageService.sendMessage(sender, receiver, "Hello student B !");

		assertThat(chatMsg.getMessage()).isEqualTo("Hello student B !");
		assertThat(chatMsg.getFrom()).isEqualTo(sender);
		assertThat(chatMsg.getTo()).isEqualTo(receiver);
	}
}

package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Feedback;
import com.bottin.classboard.models.FeedbackType;
import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackService.java
 * Description : Service interface for feedbacks
 */
@Service
public interface FeedbackService {

    // Creation

    Feedback save(Feedback u) throws Exception;

    // Update

    Feedback update(Feedback u) throws Exception;

    // Search

    Feedback findById(Long id) throws Exception;
    float getLastFeedbackValueFromStudent(FeedbackType type, String studentId) throws Exception;

    List<Feedback> findAll();

    // Deletion

    void delete(Long id) throws Exception;

    // Utils

    List<Feedback> getFeedbacksByDate(Lesson lesson);

}

package com.bottin.classboard.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ErrorController.java
 * Description : Controller to handle any errors
 */
@Controller
public class ErrorController {

    @GetMapping("/error}")
    public String joinLessonForm(Model model) {
        model.addAttribute("error", model.getAttribute("message"));
        return "error";
    }

}

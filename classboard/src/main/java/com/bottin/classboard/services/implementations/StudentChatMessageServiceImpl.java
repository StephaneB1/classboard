package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentChatMessage;
import com.bottin.classboard.repositories.StudentChatMessageRepository;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.services.interfaces.StudentChatMessageService;
import com.bottin.classboard.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentServiceImpl.java
 * Description : Service implementation for students
 */
@Service
public class StudentChatMessageServiceImpl implements StudentChatMessageService {

    // Repositories
    private final StudentChatMessageRepository studentChatMessageRepository;
    private final StudentRepository studentRepository;

    @Autowired
    public StudentChatMessageServiceImpl(StudentChatMessageRepository studentChatMessageRepository,
                                         StudentRepository studentRepository) {
        this.studentChatMessageRepository = studentChatMessageRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public StudentChatMessage sendMessage(Student from, Student to, String message) throws Exception {
        if(from == null)
            throw new Exception("No sender");

        if(to == null)
            throw new Exception("No recipient");

        StudentChatMessage chatMessage = studentChatMessageRepository.save(new StudentChatMessage(from, to, message));

        studentRepository.save(from);
        studentRepository.save(to);

        return chatMessage;
    }

    @Override
    public void update(StudentChatMessage s) throws Exception {
        if(studentChatMessageRepository.findById(s.getId()).isEmpty())
            throw new Exception("StudentChat <" + s.getId() + "> doesn't exist.");

        studentChatMessageRepository.save(s);
    }

    @Override
    public StudentChatMessage findById(String id) throws Exception {
        if(studentChatMessageRepository.findById(id).isEmpty())
            throw new Exception("StudentChat <" + id + "> doesn't exist.");

        return studentChatMessageRepository.findById(id).get();
    }

    @Override
    public List<StudentChatMessage> findAll() {
        return studentChatMessageRepository.findAll();
    }

    @Override
    public void delete(String id) throws Exception {
        if(studentChatMessageRepository.findById(id).isEmpty())
            throw new Exception("StudentChat <" + id + "> doesn't exist.");

        studentChatMessageRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        studentChatMessageRepository.deleteAll();
    }

}

package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentServiceImpl.java
 * Description : Service implementation for students
 */
@Service
public class StudentServiceImpl implements StudentService {

    // Repositories
    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student save(Student s) throws Exception {

        if(s.getFirstName().isEmpty())
            throw new Exception("First name cannot be empty");

        if(s.getLastName().isEmpty())
            throw new Exception("Last name cannot be empty");

        return studentRepository.save(s);
    }

    @Override
    public void update(Student s) throws Exception {
        if(studentRepository.findById(s.getId()).isEmpty())
            throw new Exception("Student <" + s.getId() + "> doesn't exist.");

        studentRepository.save(s);
    }

    @Override
    public Student findById(String id) throws Exception {
        if(studentRepository.findById(id).isEmpty())
            throw new Exception("Student <" + id + "> doesn't exist.");

        return studentRepository.findById(id).get();
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public void delete(String id) throws Exception {
        if(studentRepository.findById(id).isEmpty())
            throw new Exception("Student <" + id + "> doesn't exist.");

        studentRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        studentRepository.deleteAll();
    }

    @Override
    public void leaveLesson(String id) throws Exception {
        if(studentRepository.findById(id).isEmpty())
            throw new Exception("Student <" + id + "> doesn't exist.");

        // Set the lesson to null
        Student student = studentRepository.findById(id).get();
        student.setLesson(null);

        // Remove from neighbour group if it had one
        Student neighbour = student.getNeighbour();
        if(neighbour != null) {
            neighbour.setNeighbour(null);
            studentRepository.save(neighbour);
        }

        studentRepository.save(student);
    }

}

package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentActivityContent;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.Exercise;
import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import com.bottin.classboard.repositories.*;
import com.bottin.classboard.repositories.activities.ActivityPeerInstructionRepository;
import com.bottin.classboard.repositories.activities.ActivityVideoRepository;
import com.bottin.classboard.services.interfaces.ActivityService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseServiceImpl.java
 * Description : Service implementation for exercises
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    private final ActivityVideoRepository activityVideoRepository;
    private final ActivityPeerInstructionRepository activityPeerInstructionRepository;
    private final StudentRepository studentRepository;
    private final StudentActivityContentRepository studentActivityContentRepository;

    @Autowired
    public ActivityServiceImpl(ActivityVideoRepository activityVideoRepository,
                               StudentRepository studentRepository,
                               StudentActivityContentRepository studentActivityContentRepository,
                               ActivityPeerInstructionRepository activityPeerInstructionRepository) {
        this.activityVideoRepository = activityVideoRepository;
        this.studentRepository = studentRepository;
        this.studentActivityContentRepository = studentActivityContentRepository;
        this.activityPeerInstructionRepository = activityPeerInstructionRepository;
    }

    @Override
    public Activity save(Activity a) throws Exception {
        return saveActivity(a);
    }

    @Override
    public Activity update(Activity a) throws Exception {
        // Check that the activity exists
        findById(a.getId());
        return saveActivity(a);
    }

    @Override
    public Activity findById(Long id) throws Exception {
        boolean found = false;
        Activity activity = null;

        // Look into video
        try {
            activity = this.findVideoById(id);
            found = true;
        } catch (Exception ignored) {}

        // Look into peer instruction
        try {
            activity = this.findPeerInstructionById(id);
            found = true;
        } catch (Exception ignored) {}

        if(!found)
            throw new Exception("Activity <" + id + "> doesn't exist.");

        return activity;
    }

    @Override
    public ActivityVideo findVideoById(Long id) throws Exception {
        if(activityVideoRepository.findById(id).isEmpty())
            throw new Exception("Activity Video <" + id + "> doesn't exist.");

        return activityVideoRepository.findById(id).get();
    }

    @Override
    public ActivityPeerInstruction findPeerInstructionById(Long id) throws Exception {
        if(activityPeerInstructionRepository.findById(id).isEmpty())
            throw new Exception("Activity Peer Instruction <" + id + "> doesn't exist.");

        return activityPeerInstructionRepository.findById(id).get();
    }

    @Override
    public List<ActivityVideo> findAll() {
        return activityVideoRepository.findAll();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(activityVideoRepository.findById(id).isEmpty())
            throw new Exception("Activity <" + id + "> doesn't exist.");

        activityVideoRepository.deleteById(id);
    }

    @Override
    public StudentActivityContent addNewPeerInstructionAnswer(Student student, Activity activity, JSONArray answers) throws Exception {
        if(activityPeerInstructionRepository.findById(activity.getId()).isEmpty())
            throw new Exception("Activity <" + activity.getId() + "> doesn't exist.");

        if(studentRepository.findById(student.getId()).isEmpty())
            throw new Exception("Student <" + student.getId() + "> doesn't exist.");

        return saveNewContent(student, activity, answers.toString());
    }

    @Override
    public StudentActivityContent addNewVideoProgress(Student student, Activity activity, int progress) throws Exception {
        if(activityVideoRepository.findById(activity.getId()).isEmpty())
            throw new Exception("Activity <" + activity.getId() + "> doesn't exist.");

        if(studentRepository.findById(student.getId()).isEmpty())
            throw new Exception("Student <" + student.getId() + "> doesn't exist.");

        JSONObject content = new JSONObject();
        content.put("progress", progress);
        content.put("student", student.getId());

        return saveNewContent(student, activity, content.toString());
    }

    private StudentActivityContent saveNewContent(Student student, Activity activity, String content) throws Exception {
        StudentActivityContent newContent  = studentActivityContentRepository.save(
                new StudentActivityContent(student, activity, content)
        );

        studentRepository.save(student);
        saveActivity(activity);
        return newContent;
    }

    private Activity saveActivity(Activity activity) throws Exception {
        switch(activity.getSubtype()) {
            case VIDEO:
                return activityVideoRepository.save((ActivityVideo) activity);
            case PEER_INSTRUCTION:
                return activityPeerInstructionRepository.save((ActivityPeerInstruction) activity);
            default:
                throw new Exception("Activity type <" + activity.getSubtype() + "> doesn't exist.");
        }
    }
}

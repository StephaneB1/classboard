/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : break-dashboard.js
 * Description : Client-side handler for the breaks' dashboards
 */

const break_announcement = {
    self: document.getElementById('break-announcement'),
    customInput: document.getElementById('custom-break-time'),
    otherText: document.getElementById('break-other'),
    otherButton: document.getElementById('other-preset')
};

const break_confirmation = {
    self: document.getElementById('break-confirmation'),
    time: document.getElementById('break-time'),
};

const break_in_progress = {
    self: document.getElementById('break-in-progress'),
    time_left: document.getElementById('time-left'),
    end_time: document.getElementById('end-time'),
    cancelBtn: document.getElementById('cancel-break')
};

let breakTime = 0;

function init() {
    // Init dark or light mode
    initTheme();

    // Display break countdown
    if(break_in_progress.end_time !== null) {
        const endTime = new Date(break_in_progress.end_time.value);
        showBreakInProgress(endTime.getHours() + ":" + endTime.getMinutes()
            + ":" + endTime.getSeconds(), function() {
            location.reload();
        });
    }

    // Connect to the app's websockets
    connect().then(function () {
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function openEditableInput() {
    break_announcement.customInput.style.display = 'block';
}

function closeEditableInput() {
    break_announcement.customInput.style.display = 'none';
}

function verifyBreakTime() {
    const value = break_announcement.customInput.value;

    if(value < 0) {
        break_announcement.otherText.innerText = "Invalid.";
    } else {
        break_announcement.otherText.innerText = "Go for " + value + " min.";
        break_announcement.otherButton.onclick = function() {
            selectBreakTime(value);
        }
    }
}

function selectBreakTime(value) {
    breakTime = value;
    break_announcement.self.style.display = 'none';
    break_confirmation.self.style.display = 'flex';
    break_confirmation.time.innerText = value + " minutes break ?";
}

function cancelBreakEdit() {
    break_announcement.self.style.display = 'block';
    break_confirmation.self.style.display = 'none';
}

function cancelBreak() {
    document.getElementById('loader').style.display = "flex";
    document.getElementById('page-content').style.display = "none";

    // End the on going break
    sendMessageToStudentsFromTeacher('BREAK_END', '{}');
}

function confirmBreak() {

    // Display loader and notification
    break_confirmation.self.style.display = 'none';
    document.getElementById('loader').style.display = 'flex';
    ongoing_notification.break.style.display = "block";

    // display break in progress popup to students (teacher will receive it as well)
    sendMessageToStudentsFromTeacher('BREAK_START', JSON.stringify({
        minutes: parseInt(breakTime)
    }));
}


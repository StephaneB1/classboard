package com.bottin.classboard.controllers;

import com.bottin.classboard.models.*;
import com.bottin.classboard.models.Message;
import com.bottin.classboard.models.events.*;
import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import com.bottin.classboard.services.interfaces.*;
import com.bottin.classboard.utils.ClassroomGroupsUtils;
import com.bottin.classboard.utils.Utils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentController.java
 * Description : Controller for the students' dashboard
 */
@Controller
@SessionAttributes("student_id")
public class StudentDashboardController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private final LessonService lessonService;
    private final BreakService breakService;
    private final StudentService studentService;
    private final ExerciseService exerciseService;
    private final ActivityService activityService;
    private final LiveboardService liveboardService;
    private final QuestionService questionService;
    private final StudentChatMessageService studentChatMessageService;

    @Autowired
    public StudentDashboardController(LessonService lessonService, StudentService studentService,
                                      BreakService breakService, ExerciseService exerciseService,
                                      LiveboardService liveboardService, QuestionService questionService,
                                      ActivityService activityService, StudentChatMessageService studentChatMessageService) {
        this.lessonService    = lessonService;
        this.studentService   = studentService;
        this.breakService     = breakService;
        this.exerciseService  = exerciseService;
        this.liveboardService = liveboardService;
        this.questionService  = questionService;
        this.activityService  = activityService;
        this.studentChatMessageService = studentChatMessageService;
    }

    @GetMapping("/student")
    public String joinLessonForm(@RequestParam(required = false) String tab,
                                 Model model, HttpSession session) {

        try {
            // Get the student from the current session
            String id = (String) session.getAttribute("student_id");
            Student student = studentService.findById(id);

            // Reverse question list so that the new ones are at the bottom
            // of the chat
            List<Question> questions = student.getLesson().getQuestions();
            Collections.reverse(questions);

            // Tab
            String selectedTab = "Questions";
            if(tab != null && !tab.isEmpty())
                selectedTab = tab;
            model.addAttribute("tab", selectedTab);

            // Current event
            Event event = student.getLesson().getOngoingEvent();
            if(event != null) {
                model.addAttribute("event", event);
            }

            model.addAttribute("student", student);
            model.addAttribute("lessonQuestions", questions);
            model.addAttribute("studentGroups", ClassroomGroupsUtils.getStudentGroups(student.getLesson()));
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("message", "The student couldn't be loaded");
            return "error";
        }

        return "student-dashboard";
    }

    @MessageMapping("/students-messages/{lesson_id}")
    public void message(@DestinationVariable Long lesson_id, Message message) throws Exception {
        System.out.println("Received a message of type [" + message.getType() + "] : " + message.getContent());

        boolean sendToAll = true;
        Lesson lesson = lessonService.findById(lesson_id);
        Message response = Utils.getBasicResponse(message);

        // Message parameters
        JSONObject content     = new JSONObject(message.getContent());
        LocalDateTime sendDate = message.getDate();

        switch(message.getType()) {
            case STUDENT_UPDATE:
                // Classroom display update
                response.setContent(content.toString());
                break;
            case BREAK_START:
                // Save the break in database
                BreakEvent newBreak = breakService.save(new BreakEvent(lesson, content.getInt("minutes")));

                // Inform students that a break was announced while sending the end time
                response.setContent(newBreak.getResponse());
                break;
            case BREAK_END:
                // Update the current break's end date
                BreakEvent endedBreak = breakService.findOnGoingBreak(lesson);
                endedBreak.setEndDate(sendDate);
                breakService.save(endedBreak);

                // Inform students that the break has ended
                response.setContent(endedBreak.getResponse());
                break;
            case EXERCISE_START:
                // Save the exercise in database
                ExerciseType type = ExerciseType.valueOf(content.getString("type"));
                Exercise exercise = null;
                switch(type) {
                    case TRUE_FALSE:
                        exercise = exerciseService.save(new ExerciseTrueFalse(lesson, content));
                        break;
                    case MULTIPLE_CHOICE:
                        exercise = exerciseService.save(new ExerciseMultipleChoice(lesson, content));
                        break;
                }

                if(exercise != null)
                    response.setContent(exercise.getResponse());
                break;
            case ANSWER:
                // A student gave an answer to an exercise
                long id = content.getLong("exercise_id");
                Exercise answeredEx = exerciseService.findById(id);
                Student from = studentService.findById(message.getSender_id());

                // Save the answer and send the response
                StudentAnswer answer = null;
                switch(answeredEx.getExerciseType()) {
                    case TRUE_FALSE:
                        // todo : reuse answeredEx with cast ?
                        answer = exerciseService.addNewTrueFalseAnswer(from,
                                exerciseService.findTrueFalseById(id), content.getBoolean("answer"));
                        break;
                    case MULTIPLE_CHOICE:
                        answer = exerciseService.addNewMultipleChoiceAnswer(from,
                                exerciseService.findMultipleChoiceById(id), content.getJSONArray("answers"));
                        break;
                }

                if(answer != null) {
                    response.setContent(answer.getAnswerResponse());
                }
                break;
            case EXERCISE_END:
                Exercise endedExercise = exerciseService.findById(content.getLong("id"));

                // Set the end date
                endedExercise.setEndDate(sendDate);
                exerciseService.save(endedExercise);

                // Format into solutions
                response.setContent(endedExercise.getSolutionResponse());
                break;
            case QUESTION:
                // A student asked a question
                Student student = studentService.findById(message.getSender_id());
                Question question = questionService.save(new Question(lesson, student, sendDate, content));
                response.setContent(question.getResponse());
                break;
            case QUESTION_ANSWERED:
                // Set answer to the answered question in the database
                JSONObject contentQuestion = new JSONObject(message.getContent());
                Question questionAnswered = questionService.findById(contentQuestion.getLong("id"));
                questionAnswered.setAnswer(contentQuestion.getString("answer"));
                questionAnswered.setAnswered(true);
                questionService.save(questionAnswered);

                response.setContent(message.getContent());
                break;
            case BOARD_START:
                liveboardService.save(new Liveboard(lesson));
                break;
            case BOARD_PATH:
                // Received a path from the teacher
                //Liveboard board = liveboardService.findById(contentPathJson.getLong("id"));
                String path = content.getString("path");

                JSONObject pathResponse = new JSONObject();
                pathResponse.put("path", path);

                //liveboardService.addPath(new BoardPath(board, path));
                response.setContent(pathResponse.toString());
                break;
            case BOARD_END:
                Liveboard finalBoard = liveboardService.findById(content.getLong("id"));
                finalBoard.setEndDate(message.getDate());

                // Save svg image to database
                if(content.getBoolean("save")) {
                    String svg = content.getString("svg");
                    System.out.println("Saving svg : " + svg);
                }

                liveboardService.save(finalBoard);
                break;
            case ACTIVITY_START:
                // Save the activity in database
                ActivityType activityType = ActivityType.valueOf(content.getString("type"));
                switch(activityType) {
                    case VIDEO:
                        activityService.save(new ActivityVideo(lesson, content));
                        break;
                    case PEER_INSTRUCTION:
                        activityService.save(new ActivityPeerInstruction(lesson, content));
                        break;
                }
                break;
            case ACTIVITY_STUDENT_UPDATE:
                Activity updatedActivity = activityService.findById(content.getLong("id"));
                Student content_from = studentService.findById(message.getSender_id());

                // Save the content and send the response
                StudentActivityContent studentContent = null;
                switch(updatedActivity.getSubtype()) {
                    case VIDEO:
                        studentContent = activityService.addNewVideoProgress(content_from,
                                updatedActivity, content.getInt("progressStep"));
                        break;
                    case PEER_INSTRUCTION:
                        studentContent = activityService.addNewPeerInstructionAnswer(content_from,
                                updatedActivity, content.getJSONArray("answers"));
                        break;
                }

                if(studentContent != null) {
                    response.setContent(studentContent.getContentResponse());
                }
                break;
            case ACTIVITY_TEACHER_UPDATE:
                Activity upActivity = activityService.findById(content.getLong("id"));

                // Save the update
                switch(upActivity.getSubtype()) {
                    case PEER_INSTRUCTION:
                        ActivityPeerInstruction peerInstruction = (ActivityPeerInstruction) upActivity;
                        // Form peer discussion groups and save configuration
                        peerInstruction.generatePeerDiscussionGroups();
                        activityService.update(upActivity);

                        // Send update to each group (students) separately
                        /*sendToAll = false;
                        List<List<String>> groups = peerInstruction.getDiscussionGroupsIds();
                        for(List<String> group : groups) {
                            for(String studentId : group) {
                                response.setContent("Welcome " + studentId);
                                simpMessagingTemplate.convertAndSendToUser(studentId,
                                        "/queue/messages",
                                        response);
                            }
                        }*/
                        break;
                }
                break;
            case ACTIVITY_RESTART:
                // End current activity
                Activity restartedActivity = activityService.findById(content.getLong("id"));
                restartedActivity.setEndDate(message.getDate());
                activityService.save(restartedActivity);

                // Clone and save a new activity
                switch(restartedActivity.getSubtype()) {
                    case PEER_INSTRUCTION:
                        ((ActivityPeerInstruction) restartedActivity).resetDiscussionGroups();
                        activityService.save(new ActivityPeerInstruction(
                                (ActivityPeerInstruction) restartedActivity));
                        break;
                }
                break;
            case ACTIVITY_END:
                Activity endedActivity = activityService.findById(content.getLong("id"));
                endedActivity.setEndDate(message.getDate());
                activityService.save(endedActivity);
                response.setContent(endedActivity.getEndResponse());
                break;
        }

        if(sendToAll) {
            simpMessagingTemplate.convertAndSend("/lesson/" + lesson_id + "/students",
                    response);
        }
    }

    @MessageMapping("/chat")
    public void sendSpecific(Message message,
                             Principal principal) throws Exception {
        System.out.println("Received a message of type [" + message.getType() + "] : "
                + message.getContent() + ", from " + principal.getName());

        Message response = Utils.getBasicResponse(message);
        Student to = null;

        switch(message.getType()) {
            case CHAT_MESSAGE:
                Student from = studentService.findById(principal.getName());
                to = from.getNeighbour();

                // Save in database
                studentChatMessageService.sendMessage(from, to, message.getContent());

                response.setContent(message.getContent());
                break;
            case PI_CHAT_MESSAGE:
                JSONObject content = new JSONObject(message.getContent());
                response.setContent(content.getString("msg"));
                ActivityPeerInstruction activity = activityService.findPeerInstructionById(content.getLong("id"));

                // Send to the other members of the group of the activity
                List<String> membersId = activity.getGroupMembersIdList(principal.getName());
                for(String id : membersId)
                    if(!id.equals(principal.getName()))
                        simpMessagingTemplate.convertAndSendToUser(id,
                                "/queue/messages", response.toJson());
                break;
            case QUESTION_ANSWERED:
                to = studentService.findById(message.getRecipient_id());
                response.setContent(message.getContent());
                break;
        }

        if(to != null) {
            simpMessagingTemplate.convertAndSendToUser(to.getId(),
                    "/queue/messages", response.toJson());
        }
    }
}

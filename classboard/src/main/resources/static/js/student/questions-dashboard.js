/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : questions-dashboard.js
 * Description : Client-side handler for the students' questions panel
 */

const questions_panel = {
    questionInput: document.getElementById('question-input'),
    feedbackPopup: document.getElementById('question-feedback-popup'),
    feedbackPopupQuestion: document.getElementById('answered-question-text'),
    feedbackPopupAnswer: document.getElementById('answered-answer-text'),
}

let answeredQuestionId = -1;
let answeredQueue      = [];

function init() {
    // Connect to the app's websockets
    connect().then(function () {

        // Remove loader and display dashboard after everything is loaded
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function sendNewQuestion() {
    //let nameInput = document.getElementById('question-name-input');

    // todo
    // Get display name for that question
    /*let displayName = ""
    if(!anon && nameInput.value !== "") {
        displayName = nameInput.value;
    }*/

    // Send question + interactivity feedback
    sendMessageToStudents(lesson_id, student_id,'QUESTION',
        JSON.stringify({
            anon: true,
            question: questions_panel.questionInput.value,
            name: 'anon',
        }));
    sendClarityFeedback('neg');

    // Clear question text
    questions_panel.questionInput.value = "";
}

function addNewQuestionToChat(id, question, asSender) {
    // Update HTML chat
    let questionsContainer = document.getElementById('questions-container');
    let questionContainerDiv = document.createElement("div");
    let questionDiv = document.createElement("div");
    let answerDiv = document.createElement("div");
    questionContainerDiv.className = (asSender ? "message-right" : "message-left");
    questionContainerDiv.id = id;
    answerDiv.className = "answer";
    questionDiv.innerText = question;
    answerDiv.innerText = "";
    answerDiv.style.display = "none";
    questionContainerDiv.append(questionDiv);
    questionContainerDiv.append(answerDiv);
    questionsContainer.prepend(questionContainerDiv);
}

function updateAnsweredQuestionQueue() {
    // We display the next answered question
    if(answeredQueue.length > 0) {
        answeredQuestionId = answeredQueue[0].id;
        questions_panel.feedbackPopupQuestion.innerText = "\"" + answeredQueue[0].question + "\"";
        questions_panel.feedbackPopupAnswer.innerText = "\"" + answeredQueue[0].answer + "\"";
        questions_panel.feedbackPopup.style.display = "flex";
    } else {
        questions_panel.feedbackPopup.style.display = "none";
    }
}

function updateQuestionWithAnswer(id, answer) {
    let questionsDiv = document.getElementById('questions-container').getElementsByTagName('div');

    let i;
    for(i = 0; i < questionsDiv.length; i++) {
        let questionDiv = questionsDiv[i];
        if(parseInt(questionDiv.id) === id) {
            const answerDiv = questionDiv.getElementsByTagName('div')[1];
            answerDiv.innerText = "A: " + answer;
            answerDiv.style.display = "block";
            break;
        }
    }
}

function confirmAnsweredQuestion() {
    // Send confirmation to teacher
    sendQuestionConfirmation(answeredQuestionId, true);

    // Remove current confirmation and update answer queue
    answeredQueue.splice(0, 1);
    updateAnsweredQuestionQueue();

    // Send feedbacks
    sendClarityFeedback('pos');
}

function denyAnsweredQuestion() {
    // Send confirmation to teacher
    sendQuestionConfirmation(answeredQuestionId,false);

    // Remove current confirmation and update answer queue
    answeredQueue.splice(0, 1);
    updateAnsweredQuestionQueue();
}

function sendQuestionConfirmation(questionId, understood) {
    sendMessageToLesson("QUESTION_CONFIRMATION", JSON.stringify({
        question_id: questionId,
        understood: understood
    }));
}


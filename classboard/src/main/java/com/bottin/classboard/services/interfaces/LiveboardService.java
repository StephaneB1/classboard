package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.events.Liveboard;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LiveboardService.java
 * Description : Service interface for liveboards
 */
@Service
public interface LiveboardService {


    // Creation

    Liveboard save(Liveboard l) throws Exception;

    // Update

    void update(Liveboard l) throws Exception;

    // Search

    Liveboard findById(Long id) throws Exception;

    List<Liveboard> findAll();

    // Deletion

    void delete(Long id) throws Exception;

    void deleteAll();

}

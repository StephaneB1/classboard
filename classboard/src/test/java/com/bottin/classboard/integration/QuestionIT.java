package com.bottin.classboard.integration;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.LessonRepository;
import com.bottin.classboard.repositories.QuestionRepository;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.implementations.LessonServiceImpl;
import com.bottin.classboard.services.implementations.QuestionServiceImpl;
import com.bottin.classboard.services.interfaces.LessonService;
import com.bottin.classboard.services.interfaces.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
//@ActiveProfiles("dev") // For local tests only
class QuestionIT {

	@TestConfiguration
	static class OrganisationServiceImplTestContextConfiguration {
		@Bean
		public QuestionService questionService(QuestionRepository questionRepository) {
			return new QuestionServiceImpl(questionRepository);
		}

		@Bean
		public LessonService lessonService(LessonRepository lessonRepository, TeacherRepository teacherRepository) {
			return new LessonServiceImpl(lessonRepository, teacherRepository);
		}
	}

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private LessonRepository lessonRepository;

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private LessonService lessonService;

	@Autowired
	private QuestionService questionService;

	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(teacherRepository).isNotNull();
		assertThat(lessonRepository).isNotNull();
		assertThat(questionRepository).isNotNull();
		assertThat(questionService).isNotNull();
		assertThat(lessonService).isNotNull();
	}

	@Test
	void canInsertQuestionInDatabase() {
		Teacher teacher = new Teacher("is", "this", "living", "free");
		Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));
		Student student = new Student("Amazing", "Wombat");


	}
}

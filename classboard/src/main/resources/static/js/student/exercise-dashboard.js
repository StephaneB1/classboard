/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : exercise-dashboard.js
 * Description : Client-side handler for the students' exercise panel
 */

const common_items = {
    feedbackPanel: document.getElementById('feedback_panel'),
    sendBtn: document.getElementById('sendBtn'),
    id: document.getElementById('id').value,
    totalStudents: document.getElementById('total-participants').value,
}

const truefalse_panel = {
    trueBtn: document.getElementById('true'),
    falseBtn: document.getElementById('false'),
    totalTrue: document.getElementById('true-results'),
    totalFalse: document.getElementById('false-results'),
    bgFalse: document.getElementById('false-results-bg'),
    bgTrue: document.getElementById('true-results-bg'),
    answer: -1,
}

const multiplechoice_panel = {
    optionsList: document.getElementById('multiplechoice-options-results'),
    answers: []
}

function selectTrueFalseAnswer(value) {
    truefalse_panel.answer = value ? 1 : 0;

    if(truefalse_panel.answer) {
        truefalse_panel.trueBtn.classList.add("true-selected");
        truefalse_panel.falseBtn.classList.remove("false-selected");
    } else {
        truefalse_panel.trueBtn.classList.remove("true-selected");
        truefalse_panel.falseBtn.classList.add("false-selected");
    }

    common_items.sendBtn.style.display = "block";
}

function sendTrueFalseAnswer() {
    if(truefalse_panel.answer < 0)
        return

    sendMessageToStudents(lesson_id, student_id, "ANSWER", JSON.stringify({
        exercise_id: common_items.id,
        answer: truefalse_panel.answer > 0
    }));

    common_items.sendBtn.disabled = true;
    common_items.sendBtn.innerText = "Your answer has been sent! The results will appear once the exercise is over.";
    truefalse_panel.trueBtn.disabled = true;
    truefalse_panel.falseBtn.disabled = true;
}

function sendMultipleChoiceAnswer() {
    if(multiplechoice_panel.answers.length <= 0)
        return

    sendMessageToStudents(lesson_id, student_id, "ANSWER", JSON.stringify({
        exercise_id: common_items.id,
        answers: multiplechoice_panel.answers
    }));

    common_items.sendBtn.disabled = true;
    common_items.sendBtn.innerText = "Your answer has been sent! The results will appear once the exercise is over.";
    const options = multiplechoice_panel.optionsList.children;
    let i;
    for(i = 0; i < options.length; i++) {
        document.getElementById('option' + i + '-check').disabled = true;
    }
}


function updateMultipleChoiceAnswer() {
    const options = multiplechoice_panel.optionsList.children;

    // Reset answers before updating
    multiplechoice_panel.answers = [];

    let i;
    for(i = 0; i < options.length; i++) {
        multiplechoice_panel.answers.push(document.getElementById('option' + i + '-check').checked);
    }
}

function addTrueFalseAnswer(answer) {
    if(answer === 'true') {
        const totalTrue = parseInt(truefalse_panel.totalTrue.innerText) + 1;
        const percentage = 100 * totalTrue / common_items.totalStudents;
        truefalse_panel.totalTrue.innerText = "" + totalTrue;
        truefalse_panel.bgTrue.style.width = percentage + "%";
    } else {
        const totalFalse = parseInt(truefalse_panel.totalFalse.innerText) + 1;
        const percentage = 100 * totalFalse / common_items.totalStudents;
        truefalse_panel.totalFalse.innerText = "" + totalFalse;
        truefalse_panel.bgFalse.style.width = percentage + "%";
    }
}

function addMultipleChoiceAnswers(answers) {
    if(answers.length !== multiplechoice_panel.optionsList.children.length)
        return // todo : display errors

    let i;
    for(i = 0; i < answers.length; i++) {
        if(answers[i]) {
            const totalText = document.getElementById('multiplechoice-total-option-' + i);
            const total = parseInt(totalText.innerText) + 1;
            const percentage = 100 * total / common_items.totalStudents;
            totalText.innerText = "" + total;
            document.getElementById('multiplechoice-total-option-bar-' + i)
                .style.width = percentage + "%";
        }
    }
}

function displayTrueFalseResults(solution) {
    const studentAnswer = truefalse_panel.answer > 0;
    let failed = solution && !studentAnswer || !solution && studentAnswer;

    common_items.sendBtn.style.color = failed ? "red" : "var(--interactivitycolor)";
    common_items.sendBtn.innerText = failed ? "Sorry you got it wrong..." : "You got it right!";
}

function displayMultipleChoiceResults(solutionsJson) {
    const solutions = JSON.parse(solutionsJson);
    let score = 0;

    let i;
    for(i = 0; i < solutions.length; i++) {
        const optionBg = document.getElementById('multiplechoice-total-option-bar-' + i);
        const optionText = document.getElementById('multiplechoice-option-text-' + i);
        const totalText = document.getElementById('multiplechoice-total-option-' + i);
        const totalPercentage = parseInt(totalText.innerText) * 100 / common_items.totalStudents;
        const antiAnswers = common_items.totalStudents - parseInt(totalText.innerText);
        const totalAntiPercentage = antiAnswers * 100 / common_items.totalStudents;

        if(solutions[i]) {
            optionBg.style.borderTopColor = "var(--interactivitycolor)";
            optionText.innerHTML = optionText.innerText + " " +
                "<div style='display: inline; color: var(--interactivitycolor)'>CORRECT</div> (" +
                Math.round(totalPercentage) + "% got it right)";
        } else {
            optionBg.style.borderTopColor = "red";
            optionText.innerHTML = optionText.innerText + " " +
                "<div style='display: inline; color: red'>WRONG</div> (" +
                Math.round(totalAntiPercentage) + "% got it right)";
        }

        // Count personal final score & total correct
        if(solutions[i] === multiplechoice_panel.answers[i]) {
            score += 100 / solutions.length;
        }
    }

    common_items.sendBtn.style.color = "var(--themeShadeSuperLight)";
    common_items.sendBtn.innerText = "Here's your final score : " + Math.round(score) + "%";
}

function sendExerciseFeedback(index) {
    switch(index) {
        case 1:
            sendFeedbacks(["M", "C"], true);
            break;
        case 2:
            sendFeedbacks(["C"], true);
            break;
        case 3:
            sendFeedbacks(["C"], false);
            break;
    }

    location.reload();
}
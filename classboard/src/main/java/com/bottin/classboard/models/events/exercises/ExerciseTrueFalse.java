package com.bottin.classboard.models.events.exercises;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.Exercise;
import com.bottin.classboard.models.events.ExerciseType;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseTrueFalse.java
 * Description : Model for true / false exercises in a lesson
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public class ExerciseTrueFalse extends Exercise {

    // Parameters for the front end
    private static final String JSON_QUESTION_KEY = "question";
    private static final String JSON_SOLUTION_KEY = "solution";

    private String question;
    private boolean solution;

    public ExerciseTrueFalse(Lesson lesson, JSONObject content) throws Exception {
        super(lesson, ExerciseType.TRUE_FALSE);

        if(!content.has(JSON_QUESTION_KEY) || !(content.get(JSON_QUESTION_KEY) instanceof String) ||
           !content.has(JSON_SOLUTION_KEY) || !(content.get(JSON_SOLUTION_KEY) instanceof Boolean))
            throw new Exception("Invalid JSON");

        this.question = content.getString(JSON_QUESTION_KEY);
        this.solution = content.getBoolean(JSON_SOLUTION_KEY);
    }

    public boolean getSolution() {
        return solution;
    }

    @Override
    public String getResponse() {
        JSONObject result = new JSONObject();
        result.put("id", getId());
        result.put("title", getExerciseType().getTitle());
        result.put("type", getExerciseType());
        result.put("question", question);
        return result.toString();
    }

    @Override
    public String getSolutionResponse() {
        // We send the results to everyone
        JSONObject result = new JSONObject();
        result.put("type", getExerciseType());
        result.put("question", question);
        result.put("solution", solution);
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExerciseTrueFalse)) return false;
        return this.getId() != null && this.getId().equals(((ExerciseTrueFalse) o).getId());
    }
}

package com.bottin.classboard.utils;

import com.bottin.classboard.models.logs.Log;

import java.util.Comparator;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : FeedbackDateComparator.java
 * Description : Utils to compare dates between feedbacks
 */
public class LogDateComparator implements Comparator<Log> {
    @Override
    public int compare(Log o1, Log o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.events.Exercise;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import com.bottin.classboard.repositories.exercises.ExerciseMultipleChoiceRepository;
import com.bottin.classboard.repositories.exercises.ExerciseTrueFalseRepository;
import com.bottin.classboard.repositories.StudentAnswerRepository;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.services.interfaces.ExerciseService;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ExerciseServiceImpl.java
 * Description : Service implementation for exercises
 */
@Service
public class ExerciseServiceImpl implements ExerciseService {

    private final ExerciseTrueFalseRepository exerciseTrueFalseRepository;
    private final ExerciseMultipleChoiceRepository exerciseMultipleChoiceRepository;
    private final StudentRepository studentRepository;
    private final StudentAnswerRepository studentAnswerRepository;

    @Autowired
    public ExerciseServiceImpl(ExerciseTrueFalseRepository exerciseTrueFalseRepository,
                               StudentRepository studentRepository,
                               StudentAnswerRepository studentAnswerRepository,
                               ExerciseMultipleChoiceRepository exerciseMultipleChoiceRepository) {
        this.exerciseTrueFalseRepository = exerciseTrueFalseRepository;
        this.studentRepository = studentRepository;
        this.studentAnswerRepository = studentAnswerRepository;
        this.exerciseMultipleChoiceRepository = exerciseMultipleChoiceRepository;
    }

    @Override
    public Exercise save(Exercise e) throws Exception {
        return saveExercise(e);
    }

    @Override
    public Exercise update(Exercise e) throws Exception {
        Exercise exercise = findById(e.getId());

        return saveExercise(exercise);
    }

    @Override
    public Exercise findById(Long id) throws Exception {
        boolean found = false;
        Exercise exercise = null;

        // Look into true false
        try {
            exercise = this.findTrueFalseById(id);
            found = true;
        } catch (Exception ignored) {}

        // Look into multiple choice
        try {
            exercise = this.findMultipleChoiceById(id);
            found = true;
        } catch (Exception ignored) {}

        if(!found)
            throw new Exception("Exercise <" + id + "> doesn't exist.");

        return exercise;
    }

    @Override
    public ExerciseTrueFalse findTrueFalseById(Long id) throws Exception {
        if(exerciseTrueFalseRepository.findById(id).isEmpty())
            throw new Exception("Exercise True/False <" + id + "> doesn't exist.");

        return exerciseTrueFalseRepository.findById(id).get();
    }

    @Override
    public ExerciseMultipleChoice findMultipleChoiceById(Long id) throws Exception {
        if(exerciseMultipleChoiceRepository.findById(id).isEmpty())
            throw new Exception("Exercise Multiple Choice <" + id + "> doesn't exist.");

        return exerciseMultipleChoiceRepository.findById(id).get();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(exerciseTrueFalseRepository.findById(id).isEmpty())
            throw new Exception("Exercise <" + id + "> doesn't exist.");

        exerciseTrueFalseRepository.deleteById(id);
    }

    @Override
    public StudentAnswer addNewTrueFalseAnswer(Student student, ExerciseTrueFalse exercise,
                                               boolean answer) throws Exception {
        if(exerciseTrueFalseRepository.findById(exercise.getId()).isEmpty())
            throw new Exception("True/false exercise <" + exercise.getId() + "> doesn't exist.");

        if(studentRepository.findById(student.getId()).isEmpty())
            throw new Exception("Student <" + student.getId() + "> doesn't exist.");


        return saveNewAnswer(student, exercise, answer ? "true" : "false");
    }

    @Override
    public StudentAnswer addNewMultipleChoiceAnswer(Student student, ExerciseMultipleChoice exercise,
                                               JSONArray answers) throws Exception {
        if(exerciseMultipleChoiceRepository.findById(exercise.getId()).isEmpty())
            throw new Exception("Multiple Choice exercise <" + exercise.getId() + "> doesn't exist.");

        if(studentRepository.findById(student.getId()).isEmpty())
            throw new Exception("Student <" + student.getId() + "> doesn't exist.");

        return saveNewAnswer(student, exercise, answers.toString());
    }

    @Override
    public int getTotalCorrectAnswers(Exercise exercise) {
        return 0;
        //return exerciseRepository.getTotalCorrectAnswers(exercise.getId(), exercise.getSolution());
    }

    @Override
    public int getTotalWrongAnswers(Exercise exercise) {
        return 0;
        //return exerciseRepository.getTotalWrongAnswers(exercise.getId(), exercise.getSolution());
    }

    @Override
    public List<String> getAnswers(Exercise exercise) {
        return exerciseTrueFalseRepository.getAnswers(exercise.getId());
    }

    private Exercise saveExercise(Exercise exercise) throws Exception {
        switch(exercise.getExerciseType()) {
            case TRUE_FALSE:
                return exerciseTrueFalseRepository.save((ExerciseTrueFalse) exercise);
            case MULTIPLE_CHOICE:
                return exerciseMultipleChoiceRepository.save((ExerciseMultipleChoice) exercise);
            default:
                throw new Exception("Exercise type <" + exercise.getExerciseType() + "> doesn't exist.");
        }
    }

    private StudentAnswer saveNewAnswer(Student student, Exercise exercise, String answer) throws Exception {
        StudentAnswer newAnswer  = studentAnswerRepository.save(
                new StudentAnswer(student, exercise, answer)
        );

        studentRepository.save(student);
        saveExercise(exercise);
        return newAnswer;
    }
}

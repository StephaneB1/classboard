/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : exercise-dashboard.js
 * Description : Client-side handler for exercises' dashboards
 */

// Charts
let trueFalseChart;
let multipleChoiceChart;
let totalAnswers = 0;
let answerDisplayed = false;

const exercise_announcement = {
    self: document.getElementById('exercise-announcement'),
}

const exercise_edits = {
    trueFalse: {
        self: document.getElementById('exercise-edit-truefalse'),
        questionInput: document.getElementById('truefalse-question'),
        trueBtn: document.getElementById('true'),
        falseBtn: document.getElementById('false'),
        selectedAnswer: -1
    },
    multipleChoice: {
        self: document.getElementById('exercise-edit-multiplechoice'),
        questionInput: document.getElementById('multiplechoice-question'),
        optionsList: document.getElementById('multiplechoice-options')
    }
}

const exercise_results = {
    trueFalse: {
        self: document.getElementById('exercise-results-truefalse'),
        question: document.getElementById('truefalse-question-result'),
        totalFalse: document.getElementById('false-results'),
        bgFalse: document.getElementById('false-results-bg'),
        titleFalse: document.getElementById('false-title'),
        totalTrue: document.getElementById('true-results'),
        bgTrue: document.getElementById('true-results-bg'),
        titleTrue: document.getElementById('true-title'),
        showAnswer: document.getElementById('truefalse-show-answer')
    },
    multipleChoice: {
        self: document.getElementById('exercise-results-multiplechoice'),
        question: document.getElementById('multiplechoice-question-result'),
        showAnswer: document.getElementById('multiplechoice-show-answer')
    },
    id: document.getElementById('id').value,
    solution: document.getElementById('solution').value,
    type: document.getElementById('type').value
}

function init() {
    // Init dark or light mode
    initTheme();

    connect().then(function() {

        // Remove loader
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function selectExercise(index) {
    exercise_announcement.self.style.display = 'none';

    switch(index) {
        // TRUE / FALSE
        case 0:
            exercise_edits.trueFalse.self.style.display = 'flex';
            break;
        // MULTIPLE CHOICE
        case 1:
            exercise_edits.multipleChoice.self.style.display = 'flex';
            break;
    }
}

function cancelEdit() {
    exercise_edits.trueFalse.self.style.display = "none";
    exercise_edits.multipleChoice.self.style.display = "none";
    exercise_announcement.self.style.display = 'flex';
}

function selectTrueFalse(value) {
    if(value) {
        exercise_edits.trueFalse.trueBtn.classList.add("true-selected");
        exercise_edits.trueFalse.falseBtn.classList.remove("false-selected");
        exercise_edits.trueFalse.selectedAnswer = 1;
    } else {
        exercise_edits.trueFalse.trueBtn.classList.remove("true-selected");
        exercise_edits.trueFalse.falseBtn.classList.add("false-selected");
        exercise_edits.trueFalse.selectedAnswer = 0;
    }
}

function startTrueFalse() {
    if(exercise_edits.trueFalse.selectedAnswer === -1) {
        // TODO : displayErrorMessage("...") in common utils like a small alert on the corner of the screen
        return;
    }

    // Send true false data to students
    const question = exercise_edits.trueFalse.questionInput.value;
    const solution = exercise_edits.trueFalse.selectedAnswer === 1;

    if(question === "") {
        // TODO : displayErrorMessage("...") in common utils like a small alert on the corner of the screen
        return;
    }

    // Display loader & notification
    exercise_edits.multipleChoice.self.style.display = "none";
    document.getElementById('loader').style.display = "flex";
    ongoing_notification.exercise.style.display = "block";

    sendMessageToStudentsFromTeacher('EXERCISE_START', JSON.stringify({
        "type": "TRUE_FALSE",
        "question": question,
        "solution": solution
    }));
}

/**
 * Add a new answer to the chart display
 * @param type   : type of exercise
 * @param answer : String answer as a list of answers [A1, A2, ...]
 */
function addNewAnswer(type, answer) {
    console.log(answer);
    switch(type) {
        case "TRUE_FALSE":
            if(answer === 'true') {
                const totalTrue = parseInt(exercise_results.trueFalse.totalTrue.innerText) + 1;
                const percentage = 100 * totalTrue / totalStudents;
                exercise_results.trueFalse.totalTrue.innerText = "" + totalTrue;
                exercise_results.trueFalse.bgTrue.style.width = percentage + "%";
            } else {
                const totalFalse = parseInt(exercise_results.trueFalse.totalFalse.innerText) + 1;
                const percentage = 100 * totalFalse / totalStudents;
                exercise_results.trueFalse.totalFalse.innerText = "" + totalFalse;
                exercise_results.trueFalse.bgFalse.style.width = percentage + "%";
            }
            break;
        case "MULTIPLE_CHOICE":
            const parsedAnswers = JSON.parse(answer);
            let i;
            for(i = 0; i < parsedAnswers.length; i++) {
                console.log("Adding answer : " + parsedAnswers[i]);
                if(parsedAnswers[i]) {
                    const totalText = document.getElementById('multiplechoice-total-option-' + i);
                    const total = parseInt(totalText.innerText) + 1;
                    const percentage = 100 * total / totalStudents;
                    totalText.innerText = "" + total;
                    document.getElementById('multiplechoice-total-option-bar-' + i)
                        .style.width = percentage + "%";
                }
            }
            break;
    }

    // Update total answers count
    //totalAnswers++;
    //document.getElementById('total-answers').innerText = totalAnswers +
    //    " answer" + (totalAnswers === 1 ? "" : "s");
}

function endExercise() {
    sendMessageToStudentsFromTeacher('EXERCISE_END', JSON.stringify({
        id: exercise_results.id
    }));
}

function switchAnswerDisplay() {
    answerDisplayed = !answerDisplayed;

    switch(exercise_results.type) {
        case "TRUE_FALSE":
            if(answerDisplayed) {
                if(exercise_results.solution === 'true') {
                    exercise_results.trueFalse.bgFalse.style.borderTopColor = "var(--themeShadeBackground)";
                    exercise_results.trueFalse.titleFalse.style.color = "var(--themeShadeBackground)";
                } else {
                    exercise_results.trueFalse.bgTrue.style.borderBottomColor = "var(--themeShadeBackground)";
                    exercise_results.trueFalse.titleTrue.style.color = "var(--themeShadeBackground)";
                }
                exercise_results.trueFalse.showAnswer.innerText = "Hide answer";
            } else {
                exercise_results.trueFalse.bgFalse.style.borderTopColor = "red";
                exercise_results.trueFalse.titleFalse.style.color = "red";
                exercise_results.trueFalse.bgTrue.style.borderBottomColor = "var(--interactivitycolor)";
                exercise_results.trueFalse.titleTrue.style.color = "var(--interactivitycolor)";
                exercise_results.trueFalse.showAnswer.innerText = "Show answer";
            }
            break;
        case "MULTIPLE_CHOICE":
            const answers = JSON.parse(exercise_results.solution);

            if(answerDisplayed) {
                let i;
                for(i = 0; i < answers.length; i++) {
                    const optionBg = document.getElementById('multiplechoice-total-option-bar-' + i);
                    const optionText = document.getElementById('multiplechoice-option-text-' + i);

                    if(answers[i]) {
                        optionBg.style.borderTopColor = "var(--interactivitycolor)";
                        optionText.innerHTML = optionText.innerText + " " +
                            "<div style='display: inline; color: var(--interactivitycolor)'>CORRECT</div>";
                    } else {
                        optionBg.style.borderTopColor = "red";
                        optionText.innerHTML = optionText.innerText + " " +
                            "<div style='display: inline; color: red'>WRONG</div>";
                    }
                }

                exercise_results.multipleChoice.showAnswer.innerText = "Hide answer";
            } else {
                let i;
                for(i = 0; i < answers.length; i++) {
                    const optionBg = document.getElementById('multiplechoice-total-option-bar-' + i);
                    const optionText = document.getElementById('multiplechoice-option-text-' + i);
                    optionBg.style.borderTopColor = null;
                    optionText.innerText = optionText.innerText.substring(0,
                        optionText.innerText.indexOf(answers[i] ? "CORRECT" : "WRONG"));
                    exercise_results.multipleChoice.showAnswer.innerText = "Hide answer";
                }
            }
            break;
    }
}
    /**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : activity-dashboard.js
 * Description : Client-side handler for activities' dashboards
 */

// Video progress per students
let videoProgressMap = new Map();

const activity_announcement = {
    self: document.getElementById('activity-announcement'),
}

const activity_edits = {
    video: {
        self: document.getElementById('activity-edit-video'),
        videoInput: document.getElementById('video-link'),
    },
    peerInstruction: {
        self: document.getElementById('activity-edit-peer-instruction'),
    }
}

// Multiple Choice of peer instruction
const exercise_edits = {
    multipleChoice: {
        questionInput: document.getElementById('multiplechoice-question'),
        optionsList: document.getElementById('multiplechoice-options')
    }
}

const activity_results = {
    video: {
        self: document.getElementById('activity-results-video')
    },
    peerInstruction: {
        self: document.getElementById('activity-results-peer-instruction'),
        resultsContainer: document.getElementById('pi-results'),
        resultsAvg: document.getElementById('results-average'),
        resultsAvgTitle: document.getElementById('pi-average'),
        totalAnswers: document.getElementById('totalAnswered'),
        resultsTitle: document.getElementById('activity-results-title'),
        nextStepBtns: [
            document.getElementById('step-a-btn'),
            document.getElementById('step-b-btn'),
            document.getElementById('step-c-btn'),
        ],
        nextStepLabels: [
            document.getElementById('step-a'),
            document.getElementById('step-b'),
            document.getElementById('step-c'),
        ],
        solution: ""
    },
    id: document.getElementById('id').value,
    type: document.getElementById('type').value,
    totalStudents: document.getElementById('totalStudents').value
}

function init() {
    // Init dark or light mode
    initTheme();

    connect().then(function() {

        // Remove loader
        document.getElementById('loader').style.display = "none";
        document.getElementById('page-content').style.display = "flex";
    });
}

function cancelEdit() {
    activity_edits.video.self.style.display = "none";
    activity_edits.peerInstruction.self.style.display = "none";
    activity_announcement.self.style.display = 'flex';
}

function selectActivity(index) {
    activity_announcement.self.style.display = 'none';

    switch(index) {
        // YOUTUBE VIDEO
        case 0:
            activity_edits.video.self.style.display = 'flex';
            break;
        // PEER INSTRUCTION
        case 1:
            activity_edits.peerInstruction.self.style.display = 'flex';
            break;
    }
}

function startVideo() {

    const link = activity_edits.video.videoInput.value;

    if(link === "") {
        return; // todo display error message
    }

    // Display loader & notification
    activity_edits.video.self.style.display = "none";
    document.getElementById('loader').style.display = "flex";
    ongoing_notification.activity.style.display = "block";

    sendMessageToStudentsFromTeacher('ACTIVITY_START', JSON.stringify({
        "type": "VIDEO",
        "link": link
    }));
}

/**
 * Add a new answer to the chart display
 * @param type   : type of exercise
 * @param update : String update for the on going activity
 */
function addNewUpdate(student, type, update) {
    const updateObj = JSON.parse(update);

    switch(type) {
        case "VIDEO":
            videoProgressMap.set(student, updateObj.progress);
            console.log(videoProgressMap);
            console.log(videoProgressMap.size);
            // reset widths
            let progressBar;
            for(progressBar = 1; progressBar <= 3; progressBar++) {
                document.getElementById('yt-prog-' + progressBar).style.width = '0';
            }

            videoProgressMap.forEach(function (value, key) {
                const currentWidth = parseInt(document.getElementById('yt-prog-' + value).style.width);
                document.getElementById('yt-prog-' + value).style.width =
                    (currentWidth + (100 / activity_results.totalStudents)) + "%";
            });
            break;
        case "PEER_INSTRUCTION":
            // Set the solution if not already
            if (activity_results.peerInstruction.solution === "") {
                activity_results.peerInstruction.solution = JSON.parse(document.getElementById('solutions').value);
                console.log(activity_results.peerInstruction.solution);
            }

            // Update total answer
            const totalAnswer = parseInt(activity_results.peerInstruction.totalAnswers.innerText) + 1;
            activity_results.peerInstruction.totalAnswers.innerText = totalAnswer + " ";
            if(totalAnswer >= activity_results.totalStudents) {
                activity_results.peerInstruction.resultsTitle.innerText =
                    "Done! Everyone answered the question."
            }

            // Compute and add new result average
            let totalCorrect = 0;
            let i;
            for (i = 0; i < updateObj.length; i++) {
                console.log(updateObj[i]);
                if(activity_results.peerInstruction.solution[i] === updateObj[i]) {
                    totalCorrect++;
                }
            }

            addPeerInstructionAvgResult(totalAnswer, totalCorrect * 100 / updateObj.length);
            break;
    }
}

function addPeerInstructionAvgResult(totalAnswers, newAveragePercentage) {
    const currentAvg = parseInt(activity_results.peerInstruction.resultsAvgTitle.innerText);
    const studentAvg = newAveragePercentage + "%";
    const newAvg = ((currentAvg * (totalAnswers - 1) + newAveragePercentage) / totalAnswers).toFixed(2);
    const newAvgTxt = newAvg + "%";

    // Add individual average
    const newAverage = document.createElement("div");
    newAverage.style.left = studentAvg;
    activity_results.peerInstruction.resultsContainer.appendChild(newAverage);

    // Update global average
    activity_results.peerInstruction.resultsAvg.style.left = newAvgTxt;
    activity_results.peerInstruction.resultsAvgTitle.innerText = newAvgTxt;

    // Update buttons
    let i;
    for(i = 0; i < activity_results.peerInstruction.nextStepBtns.length; i++) {
        if( (newAvg < 30 && i === 0) ||
            (newAvg >= 30 && newAvg < 70 && i === 1) ||
            (newAvg >= 70 && i === 2)) {
            activity_results.peerInstruction.nextStepBtns[i].style.boxShadow = "0 0 10px var(--vitalitycolor)";
            activity_results.peerInstruction.nextStepLabels[i].style.color = "var(--vitalitycolor)";
        } else {
            activity_results.peerInstruction.nextStepBtns[i].style.boxShadow = null;
            activity_results.peerInstruction.nextStepLabels[i].style.color = null;
        }
    }
}

function restartPeerInstruction() {
    sendMessageToStudentsFromTeacher('ACTIVITY_RESTART', JSON.stringify({
        id: activity_results.id
    }));
}

function startPeerDiscussion() {
    sendMessageToStudentsFromTeacher('ACTIVITY_TEACHER_UPDATE', JSON.stringify({
        id: activity_results.id
    }));
}

function endActivity() {
    sendEndOfActivityToStudents();
}

function sendEndOfActivityToStudents() {
    stompClient.send(student_channel_out + lesson_id, {}, JSON.stringify({
        'content': JSON.stringify({
            id: activity_results.id
        }),
        'type': 'ACTIVITY_END',
        'date': new Date()
    }));
}
package com.bottin.classboard.unit;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Question;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.Teacher;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LessonModelTests {

    @Test
    public void canCreateALesson() {
        Lesson lesson = new Lesson(new Teacher(), "Xylomancy",
                LocalDateTime.now().plusMinutes(30));

        assertEquals("Xylomancy", lesson.getSubject());
    }
}

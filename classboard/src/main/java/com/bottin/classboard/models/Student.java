package com.bottin.classboard.models;

import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.Exercise;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.beans.Transient;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Student.java
 * Description : Model for students
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
public class Student {

    @Id
    private String id;

    private String firstName;
    private String lastName;

    private String sessionToken;

    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="student", orphanRemoval = true)
    private List<Question> questions;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lesson lesson;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="student", orphanRemoval = true)
    private List<Feedback> feedbacks;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<StudentAnswer> answers;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<StudentActivityContent> activityContents;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "from")
    private List<StudentChatMessage> sendChat;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "to")
    private List<StudentChatMessage> receivedChat;

    @OneToOne
    private Student neighbour;

    public Student() {
        this.id = UUID.randomUUID().toString();
        this.questions = new ArrayList<>();
        this.feedbacks = new ArrayList<>();
        this.sendChat = new ArrayList<>();
        this.receivedChat = new ArrayList<>();
    }

    public Student(String firstName, String lastName) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(Lesson lesson, String firstName, String lastName, String sessionToken) {
        this(firstName, lastName);
        this.sessionToken = sessionToken;
        this.lesson = lesson;
    }

    public String getFullName() {
        if(lastName == null)
            return firstName;

        return firstName + " " + lastName;
    }

    public String getFormattedJoinedDate() {
        return createdDate.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public void addNewAnswer(StudentAnswer answer) {
        if(this.answers == null)
            this.answers = new ArrayList<>();

        this.answers.add(answer);
    }

    public void addNewMessage(StudentChatMessage message, boolean asSender) {
        if(asSender)
            this.sendChat.add(message);
        else
            this.receivedChat.add(message);
    }

    public List<StudentChatMessage> getChatMessages() {
        List<StudentChatMessage> result = sendChat;
        result.addAll(receivedChat);

        // Sort by date
        result.sort((o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate()));

        return result;
    }

    public boolean hasAnswered(Exercise exercise) {
        for(StudentAnswer answer : answers)
            if(answer.getExercise().equals(exercise))
                return true;

        return false;
    }

    public boolean hasSendContent(Activity activity) {
        for(StudentActivityContent content : activityContents)
            if (content.getActivity().equals(activity))
                return true;

        return false;
    }

    public ArrayList<Map<LocalDateTime, Float>> getFeedbackVariationMap(int totalStudents) {
        ArrayList<Map<LocalDateTime, Float>> result = new ArrayList<>();
        for (int i = 0; i < 4; i++)
            result.add(new HashMap<>());

        float[] previousValues = new float[8]; // a value for each type and sign (positive / negative)
        Arrays.fill(previousValues, 50.0f);

        for(Feedback feedback : feedbacks) {

            int index = feedback.getType().ordinal() + (feedback.isPositive() ? 0 : result.size());

            float nextNormalizedFeedbackValue = feedback.isPositive() ?
                    Math.min(100, previousValues[index] / (2.0f * totalStudents) + 50.0f) :
                    Math.max(0, (previousValues[index] - 100) / (2.0f * totalStudents) + 50.0f);

            result.get(feedback.getType().ordinal()).put(
                    feedback.getCreatedDate(), nextNormalizedFeedbackValue - previousValues[index]
            );

            previousValues[index] = nextNormalizedFeedbackValue;
        }

        // System.out.println("[" + id + "] " + result.toString());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        return this.id != null && this.id.equals(((Student) o).getId());
    }

    @Override
    public String toString() {
        return "Student" + id;
    }
}

package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.TeacherRepository;
import com.bottin.classboard.services.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : TeacherServiceImpl.java
 * Description : Service implementation for teachers
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    // Repositories
    private final TeacherRepository teacherRepository;

    @Autowired
    public TeacherServiceImpl(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public void save(Teacher t) throws Exception {

        if(t.getUsername().isEmpty())
            throw new Exception("Username cannot be empty");

        if(teacherRepository.findById(t.getUsername()).isPresent())
            throw new Exception("Username already taken");

        teacherRepository.save(t);
    }

    @Override
    public void update(Teacher t) throws Exception {
        checkUserStatus(t);
        teacherRepository.save(t);
    }

    @Override
    public Teacher findByUsername(String username) throws Exception {
        checkUserStatus(username);
        return teacherRepository.findById(username).get();
    }

    @Override
    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    public void delete(String username) throws Exception {
        checkUserStatus(username);

        // Finally remove user, goodbye ;_;
        teacherRepository.deleteById(username);
    }

    @Override
    public void deleteAll() {
        teacherRepository.deleteAll();
    }

    /**
     * Check the teacher data integrity
     * @param teacher : teacher to check
     * @throws Exception if wrong data
     */
    private void checkUserStatus(Teacher teacher) throws Exception {
        if(teacher == null)
            throw new Exception("Teacher is null.");

        checkUserStatus(teacher.getUsername());
    }

    /**
     * Check the teacher data integrity
     * @param username : username of the teacher to check
     * @throws Exception if wrong data
     */
    private void checkUserStatus(String username) throws Exception {
        if(username == null || username.isEmpty())
            throw new Exception("Username is null or empty.");

        if(teacherRepository.findById(username).isEmpty())
            throw new Exception("Teacher <" + username + "> doesn't exist.");
    }
}

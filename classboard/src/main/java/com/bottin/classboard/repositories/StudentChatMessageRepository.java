package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.StudentChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentChatMessageRepository.java
 * Description : Repository for students' chat messages
 */
@Repository
public interface StudentChatMessageRepository extends JpaRepository<StudentChatMessage, String> {

}

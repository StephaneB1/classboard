/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : teacher-comms.js
 * Description : Client-side handler for all websocket communications of the teacher
 */

// STOMP Connections
// - Websockets
const app_websocket          = "/classboard-websocket";
const app_students_websocket = "/classboard-websocket-students";
const app_chat_websocket     = "/classboard-websocket-chat";
// - Channels
const teacher_channel_out = "/app/messages/";
const student_channel_out = "/app/students-messages/";
const chat_channel_out    = "/app/chat";
const teacher_channel_in  = "/lesson/teacher";
// - Clients
let stompClient = null;
let stompStudentsClient = null;

/**
 * Connect to the STOMP
 */
function connect() {
    // Connect to each websocket in order and at the end resolve the promise
    return new Promise(function(resolve, reject) {
        connectTeacherChannel().then(function () {
            connectStudentsChannel().then(function() {
                connectStudentsChatChannel().then(function () {
                    resolve();
                })
            })
        });
    });
}

function connectTeacherChannel() {

    return new Promise(function(resolve, reject) {
        // Lesson main websocket
        let socket = new SockJS(app_websocket);
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            stompClient.subscribe(teacher_channel_in, function (msg) {
                const message = JSON.parse(msg.body);
                switch(message.type) {
                    case "SWITCH_ACCESS":
                        location.reload();
                        break;
                    case "STUDENT_JOINED":
                        addNewStudent(message.sender_id, message.content);
                        break;
                    case "STUDENT_LEFT":
                        // TODO : removeStudent(message.sender_id); (not reliable enough)
                        break;
                    case "QUESTION_CONFIRMATION":
                        const content = JSON.parse(message.content);
                        let i;
                        for(i = 0; i < questions.length; i++) {
                            if(questions[i].question_id === content.question_id) {
                                if(content.understood) {
                                    // If the question was understood we remove it
                                    questions.splice(i, 1);
                                } else {
                                    // Else we reset it as not answered
                                    questions[i].answered = false;
                                }
                            }
                        }
                        goToNextQuestion();
                        break;
                }
            });

            resolve();
        });
    });
}

function connectStudentsChannel() {

    return new Promise(function(resolve, reject) {
        // Lesson students websocket
        let socketStudents = new SockJS(app_students_websocket);
        stompStudentsClient = Stomp.over(socketStudents);
        stompStudentsClient.connect({}, function (frame) {
            // Subscribes to the students channel of the lesson
            stompStudentsClient.subscribe(students_channel_in, function (msg) {
                const message = JSON.parse(msg.body);
                const content = JSON.parse(message.content);
                switch(message.type) {
                    // Hard reload (when it doesn't ruin the user experience)
                    case "BREAK_START":
                    case "EXERCISE_START":
                    case "ACTIVITY_START":
                    case "ACTIVITY_RESTART":
                    case "ACTIVITY_TEACHER_UPDATE":
                        location.reload();
                        break;
                    case "BREAK_END":
                    case "BOARD_END":
                    case "ACTIVITY_END":
                    case "EXERCISE_END":
                        let url = window.location.href;

                        // We replace the tab with the home tab (redirection after the end of something)
                        if(url.indexOf("?") > 0) {
                            url = url.substring(0, url.indexOf("?"));
                        }

                        window.open(url, "_self");
                        break;
                    // Live updates (when hard reload would ruin the user experience)
                    case "STUDENT_UPDATE":
                        if(typeof updateClassroomDisplay === "function"){
                            updateClassroomDisplay(content.groups, true);
                        }
                        break;
                    case "QUESTION":
                        addNewQuestion(content);
                        break;
                    case "ANSWER":
                        addNewAnswer(content.type, content.answer);
                        break;
                    case "ACTIVITY_STUDENT_UPDATE":
                        addNewUpdate(content.student_id, content.type, content.content);
                        break;
                    case "BOARD_START":
                        displayDrawingBoard();
                        break;
                }
            });
        });

        resolve();
    });
}

function connectStudentsChatChannel() {
    return new Promise(function(resolve, reject) {
        // Chat websocket
        let socketChat = new SockJS(app_chat_websocket);
        stompChatClient = Stomp.over(socketChat);
        stompChatClient.connect({}, function (frame) {
            resolve();
        });
    });
}

/**
 * Sends message to student channel
 * @param type      : type of message
 * @param content   : content of message
 */
function sendMessageToStudentsFromTeacher(type, content) {
    sendMessageToStudents(lesson_id, -1, type, content);
}

/**
 * Sends message to the lesson (teacher)
 * @param lesson_id : String lesson id
 * @param type      : type of the message
 * @param content   : content of the message
 */
function sendMessageToSelf(type, content) {
    stompClient.send(teacher_channel_out + lesson_id, {}, JSON.stringify({
        'content': content,
        'type': type,
        'sender_id': -1,
        'date': new Date()
    }));
}
package com.bottin.classboard.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentChatMessage.java
 * Description : Model for students' chat messages
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public class StudentChatMessage {

    @Id
    private String id;

    private String message;

    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student from;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student to;

    public StudentChatMessage(Student from, Student to, String message) {
        this.id = UUID.randomUUID().toString();
        this.from = from;
        this.to = to;
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentChatMessage)) return false;
        return this.id != null && this.id.equals(((StudentChatMessage) o).getId());
    }

    @Override
    public String toString() {
        return "StudentChatMessage" + id;
    }
}

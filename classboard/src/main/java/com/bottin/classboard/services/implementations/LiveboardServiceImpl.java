package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.events.Liveboard;
import com.bottin.classboard.repositories.LiveboardRepository;
import com.bottin.classboard.services.interfaces.LiveboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LiveboardServiceImpl.java
 * Description : Service implementation for liveboards
 */
@Service
public class LiveboardServiceImpl implements LiveboardService {

    // Repositories
    private final LiveboardRepository liveboardRepository;

    @Autowired
    public LiveboardServiceImpl(LiveboardRepository liveboardRepository) {
        this.liveboardRepository = liveboardRepository;
    }

    @Override
    public Liveboard save(Liveboard l) throws Exception {
        return liveboardRepository.save(l);
    }

    @Override
    public void update(Liveboard l) throws Exception {
        if(liveboardRepository.findById(l.getId()).isEmpty())
            throw new Exception("Liveboard <" + l.getId() + "> doesn't exist.");

        liveboardRepository.save(l);
    }

    @Override
    public Liveboard findById(Long id) throws Exception {
        if(liveboardRepository.findById(id).isEmpty())
            throw new Exception("Liveboard <" + id + "> doesn't exist.");

        return liveboardRepository.findById(id).get();
    }

    @Override
    public List<Liveboard> findAll() {
        return liveboardRepository.findAll();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(liveboardRepository.findById(id).isEmpty())
            throw new Exception("Lesson <" + id + "> doesn't exist.");

        liveboardRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        liveboardRepository.deleteAll();
    }


}

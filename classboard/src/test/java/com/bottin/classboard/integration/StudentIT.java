package com.bottin.classboard.integration;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.services.implementations.StudentServiceImpl;
import com.bottin.classboard.services.interfaces.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
//@ActiveProfiles("dev") // For local tests only
class StudentIT {

	@TestConfiguration
	static class OrganisationServiceImplTestContextConfiguration {

		@Bean
		public StudentService studentService(StudentRepository studentRepository) {
			return new StudentServiceImpl(studentRepository);
		}
	}

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private StudentService studentService;

	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(studentRepository).isNotNull();
		assertThat(studentService).isNotNull();
	}

	@Test
	void canInsertStudentInDatabase() {
		Student student = new Student("Magnificent", "Koala");
		assertDoesNotThrow(() -> studentService.save(student));
	}

	@Test
	void cannotInsertUnnamedStudentInDatabase() {
		Student student = new Student("", "");
		assertThrows(Exception.class, () -> studentService.save(student));
	}

	@Test
	public void whenDeleteAllFromService_thenRepositoryShouldBeEmpty() {
		studentService.deleteAll();
		assertThat(studentRepository.count()).isEqualTo(0);
	}
}

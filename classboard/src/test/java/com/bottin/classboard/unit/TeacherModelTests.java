package com.bottin.classboard.unit;

import com.bottin.classboard.models.Teacher;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TeacherModelTests {
    private final String username  = "nico";
    private final String password  = "12345";
    private final String firstName = "Nicolas";
    private final String lastName  = "Bourbaki";
    
    @Test
    public void canCreateATeacherProfile() {
        Teacher teacher = new Teacher(username, password, firstName, lastName);

        assertEquals(teacher.getUsername(), username);
        assertEquals(teacher.getPassword(), password);
        assertEquals(teacher.getFirstName(), firstName);
        assertEquals(teacher.getLastName(), lastName);
        assertNull(teacher.getLessons());
    }

    @Test
    public void teachersWithSameUsernameAreEqual() {
        Teacher teacher1 = new Teacher(username, password, firstName, lastName);
        Teacher teacher2 = new Teacher(username, password, firstName, lastName);

        assertEquals(teacher1, teacher2);
    }
}

package com.bottin.classboard.models.events.activities;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.ActivityType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.Map;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ActivityVideo.java
 * Description : Model for video activity in a lesson
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public class ActivityVideo extends Activity {

    private static final String JSON_LINK_KEY = "link";

    private String link;

    public ActivityVideo(Lesson lesson, String link) {
        super(lesson, ActivityType.VIDEO);
        this.link = link;
    }

    public ActivityVideo(Lesson lesson, JSONObject content) throws Exception {
        super(lesson, ActivityType.VIDEO);

        if(!content.has(JSON_LINK_KEY) || !(content.get(JSON_LINK_KEY) instanceof String))
            throw new Exception("Invalid JSON");


        this.link = content.getString(JSON_LINK_KEY);
    }

    @Override
    public String getResponse() {
        return null;
    }

    @Override
    public String getEndResponse() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActivityVideo)) return false;
        return this.getId() != null && this.getId().equals(((ActivityVideo) o).getId());
    }
}

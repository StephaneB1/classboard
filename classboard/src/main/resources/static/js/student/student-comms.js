/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : student-comms.js
 * Description : Client-side handler for all websocket communications of the students
 */

// STOMP Connections
// - Websockets
const app_websocket          = "/classboard-websocket";
const app_students_websocket = "/classboard-websocket-students";
const app_chat_websocket     = "/classboard-websocket-chat";
// - Channels
const teacher_channel_out = "/app/messages/";
const student_channel_out = "/app/students-messages/";
const chat_channel_out    = "/app/chat";
const chat_channel_in     = "/user/queue/messages";
// - Clients
let stompClient         = null;
let stompStudentsClient = null;
let stompChatClient     = null;


/**
 * Connect to the STOMP
 */
function connect() {
    // Connect to each websocket in order and at the end resolve the promise
    return new Promise(function(resolve, reject) {
        connectTeacherChannel().then(function () {
            connectStudentsChannel().then(function() {
                connectStudentsChatChannel().then(function () {
                    resolve();
                })
            })
        });
    });
}

function connectTeacherChannel() {

    return new Promise(function(resolve, reject) {
        // Lesson main websocket
        let socket = new SockJS(app_websocket);
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            // Inform that a new student has arrived
            sendMessageToLesson('STUDENT_JOINED', '{}');
            resolve();
        });
    });
}

function connectStudentsChannel() {

    return new Promise(function(resolve, reject) {
        // Lesson students websocket
        let socketStudents = new SockJS(app_students_websocket);
        stompStudentsClient = Stomp.over(socketStudents);
        stompStudentsClient.connect({}, function (frame) {
            // Subscribes to the students channel of the lesson
            stompStudentsClient.subscribe(students_channel_in, function (msg) {
                const message = JSON.parse(msg.body);
                const content = JSON.parse(message.content);
                switch(message.type) {
                    case "STUDENT_UPDATE":
                        // Get the student groups
                        updateClassroomDisplay(content.groups, false);

                        // If the student is now in a group, update neighbour chat
                        let groups = content.groups;
                        let i;
                        for(i = 0; i < groups.length; i++) {
                            console.log(groups[i]);
                            if(groups[i].length >= 2) {
                                const studentA = groups[i][0];
                                const studentB = groups[i][1];

                                if(studentA.name === student_fullname) {
                                    document.getElementById('no-neighbour-text').style.display = "none";
                                    document.getElementById('neighbour-chat').style.display = "flex";
                                    document.getElementById('welcome-chat').innerText = "Now chatting with " + studentB.name;
                                    break;
                                } else if (studentB.name === student_fullname) {
                                    document.getElementById('no-neighbour-text').style.display = "none";
                                    document.getElementById('neighbour-chat').style.display = "flex";
                                    document.getElementById('welcome-chat').innerText = "Now chatting with " + studentA.name;
                                    break;
                                }
                            }
                        }
                        break;
                    case "BREAK_START":
                    case "BREAK_END":
                    case "ACTIVITY_RESTART":
                    case "ACTIVITY_END":
                    case "BOARD_START":
                    case "BOARD_END":
                        location.reload();
                        break;
                    case "ACTIVITY_START":
                    case "EXERCISE_START":
                        sendInteractivityFeedback("pos");
                        location.reload();
                        break;
                    case "ANSWER":
                        switch(content.type) {
                            case "TRUE_FALSE": addTrueFalseAnswer(content.answer); break;
                            case "MULTIPLE_CHOICE": addMultipleChoiceAnswers(JSON.parse(content.answer)); break;
                        }
                        break;
                    case "EXERCISE_END":
                        switch(content.type){
                            case "TRUE_FALSE":
                                displayTrueFalseResults(content.solution);
                                break;
                            case "MULTIPLE_CHOICE":
                                displayMultipleChoiceResults(content.solutions);
                                break;
                        }

                        // Enable exercise feedback
                        document.getElementById('feedback_panel').style.display = "block";
                        break;
                    case "ACTIVITY_STUDENT_UPDATE":
                        updateActivity(content.type, content.content);
                        break;
                    case "ACTIVITY_TEACHER_UPDATE":
                        location.reload();
                        break;
                    case "QUESTION":
                        // Add question in questions chat list
                        const asSender = content.student_id === student_id;
                        addNewQuestionToChat(content.question_id, content.question, asSender);
                        break;
                    case "QUESTION_ANSWERED":
                        // Update the chat with the new answer
                        updateQuestionWithAnswer(content.id, content.answer);
                        break;
                    case "BOARD_PATH":
                        board_funcs.addNewPath(content.path);
                        break;
                }
            });

        });

        resolve();
    });
}

function connectStudentsChatChannel() {
    return new Promise(function(resolve, reject) {
        // Chat websocket
        let socketChat = new SockJS(app_chat_websocket);
        stompChatClient = Stomp.over(socketChat);
        stompChatClient.connect({}, function (frame) {
            // Subscribes to a secured private channel for personal messages
            stompChatClient.subscribe(chat_channel_in, function (msg) {
                const message = JSON.parse(msg.body);

                // Received private message
                switch(message.type) {
                    case "CHAT_MESSAGE":
                    case "PI_CHAT_MESSAGE":
                        // Show new messages in chat
                        showNewMessage(message.content, false);
                        break;
                    case "QUESTION_ANSWERED":
                        // Add the answer to the answered queue
                        const content = JSON.parse(message.content);

                        // Add to answered queue for student
                        answeredQueue.push(content);

                        // Update queue display (to get feedback)
                        updateAnsweredQuestionQueue();
                        sendInteractivityFeedback('pos');
                        break;
                }
            });

            resolve();
        });
    });
}

function sendMessageToLesson(type, content) {
    stompClient.send(teacher_channel_out + lesson_id, {}, JSON.stringify({
        'content': content,
        'type': type,
        'sender_id': student_id,
        'date': new Date()
    }));
}

function sendFeedbacks(scales, pos) {
    scales.forEach(scale => {
        console.log("Sending feedback " + scale + " : positive = " + pos);
       switch(scale) {
           case "M":
               sendMotivationFeedback(pos ? "pos" : "neg");
               break;
           case "V":
               sendVitalityFeedback(pos ? "pos" : "neg");
               break;
           case "C":
               sendClarityFeedback(pos ? "pos" : "neg");
               break;
           case "I":
               sendInteractivityFeedback(pos ? "pos" : "neg");
               break;
       }
    });
}

function sendMotivationFeedback(sign) {
    sendFeedback("MOTIVATION", sign);
}

function sendVitalityFeedback(sign) {
    sendFeedback("VITALITY", sign);
}

function sendClarityFeedback(sign) {
    sendFeedback("CLARITY", sign);
}

function sendInteractivityFeedback(sign) {
    sendFeedback("INTERACTIVITY", sign);
}

function sendFeedback(type, sign) {
    sendMessageToLesson("FEEDBACK", JSON.stringify({
       type: type,
       sign: sign
    }));
}
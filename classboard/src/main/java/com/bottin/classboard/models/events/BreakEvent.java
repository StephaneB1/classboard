package com.bottin.classboard.models.events;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.json.JSONObject;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Break.java
 * Description : Model for breaks in a lesson
 */
@Entity
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class BreakEvent extends Event {

    // Parameters for the front end
    private static final String JSON_MINUTES_KEY = "minutes";

    private int durationMinutes;

    public BreakEvent(Lesson lesson, int minutes) {
        super(lesson, EventType.BREAK);
        this.durationMinutes = minutes;
    }

    public BreakEvent(Lesson lesson, JSONObject content) throws Exception {
        super(lesson, EventType.BREAK);

        if(!content.has(JSON_MINUTES_KEY) || !(content.get(JSON_MINUTES_KEY) instanceof Integer))
            throw new Exception("Invalid JSON");

        this.durationMinutes = content.getInt("minutes");
    }

    public LocalDateTime getPlannedEndDate() {
        return getStartDate().plusMinutes(durationMinutes);
    }

    @Override
    public List<Log> getLogs() {
        List<Log> logs = new ArrayList<>();

        // Start
        logs.add(new Log(getStartDate(),
                "BREAK STARTED",
                "Break of " + durationMinutes + "min. started",
                "var(--creationLogColor)"));

        // Student content
        // TODO

        // End
        if(getEndDate() != null) {
            logs.add(new Log(getEndDate(),
                    "BREAK ENDED",
                    "Break of " + durationMinutes + "min. ended",
                    "var(--endExerciseLogColor)"));
        }

        return logs;
    }

    @Override
    public String getResponse() {
        JSONObject response = new JSONObject();
        LocalDateTime endDate = getPlannedEndDate();
        response.put("endTime", endDate.getHour() + ":" + endDate.getMinute() + ":" + endDate.getSecond());
        return response.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BreakEvent)) return false;
        return this.getId() != null && this.getId().equals(((BreakEvent) o).getId());
    }
}

package com.bottin.classboard.unit;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.Student;
import com.bottin.classboard.models.StudentActivityContent;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.models.events.Activity;
import com.bottin.classboard.models.events.BreakEvent;
import com.bottin.classboard.models.events.activities.ActivityPeerInstruction;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActivityModelTests {

    private static final String ACTIVITY_VIDEO_LINK = "https://www.youtube.com/watch?v=PSAPGjnaKCA";

    private static final String ACTIVITY_PEER_INSTRUCTION_JSON = "{" +
            "question: \"A conceptual question\"," +
            "options: [\"OptionA\", \"OptionB\", \"OptionC\"]," +
            "solutions: [true, false, false]" +
            "}";

    // Test data
    private static final Teacher teacher = new Teacher("John123", "123", "John", "Doe");
    private static final Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

    @Test
    public void canCreateAVideoActivity() throws Exception {
        ActivityVideo video = new ActivityVideo(lesson, ACTIVITY_VIDEO_LINK);

        assertEquals(lesson, video.getLesson());
        assertEquals(ACTIVITY_VIDEO_LINK, video.getLink());
    }

    @Test
    public void canCreateAPeerInstructionActivity() throws Exception {
        ActivityPeerInstruction pi = new ActivityPeerInstruction(lesson, new JSONObject(ACTIVITY_PEER_INSTRUCTION_JSON));

        assertEquals(lesson, pi.getLesson());
        assertEquals("A conceptual question", pi.getQuestion());
        JSONArray options = new JSONArray(pi.getOptions());
        JSONArray solutions = new JSONArray(pi.getSolutions());
        assertEquals("OptionA", options.get(0));
        assertEquals(true,       solutions.get(0));
        assertEquals("OptionB", options.get(1));
        assertEquals(false,      solutions.get(1));
        assertEquals("OptionC", options.get(2));
        assertEquals(false,       solutions.get(2));
    }

    @Test
    public void canGenerateDistributedGroupsForPeerDiscussion() throws Exception {
        // Adding 10 students to the lesson
        for (int i = 0; i < 10; i++)
            lesson.getStudents().add(new Student(lesson,
                    "Magnificent",
                    "Elephant",
                    "xxxxxxxx"));

        ActivityPeerInstruction pi = new ActivityPeerInstruction(lesson, new JSONObject(ACTIVITY_PEER_INSTRUCTION_JSON));

        // All students answers the quiz randomly
        for (int i = 0; i < 10; i++)
            pi.getAnswers().add(new StudentActivityContent(lesson.getStudents().get(i), pi, getRandomPossibleSolution()));

        // We generate the groups
        pi.generatePeerDiscussionGroups();
        JSONArray groups_list = new JSONArray(pi.getDiscussionGroups());

        // We then verify that the difference between the group sizes isn't more than one
        int[] sizes = new int[groups_list.length()];
        for (int i = 0; i < sizes.length; i++) {
            JSONArray group_answers = groups_list.getJSONArray(i);
            sizes[i] = group_answers.length();
        }

        assertThat(maxDiff(sizes, sizes.length)).isLessThan(2);
    }

    private String getRandomPossibleSolution() {
        Random rand = new Random();

        // To simplify, we assume there is only one right answer (one true)
        List<String> possible_solutions = new ArrayList<>();
        possible_solutions.add("[true,false,false]");
        possible_solutions.add("[false,true,false]");
        possible_solutions.add("[false,false,true]");
        possible_solutions.add("[false,false,false]");

        return possible_solutions.get(rand.nextInt(possible_solutions.size()));
    }

    /* The function assumes that there are at least two
   elements in array.
   The function returns a negative value if the array is
   sorted in decreasing order.
   Returns 0 if elements are equal  */
    // Source : https://www.geeksforgeeks.org/maximum-difference-between-two-elements/
    int maxDiff(int[] arr, int arr_size)
    {
        int max_diff = arr[1] - arr[0];
        int min_element = arr[0];
        int i;
        for (i = 1; i < arr_size; i++)
        {
            if (arr[i] - min_element > max_diff)
                max_diff = arr[i] - min_element;
            if (arr[i] < min_element)
                min_element = arr[i];
        }
        return max_diff;
    }
}

package com.bottin.classboard.repositories;

import com.bottin.classboard.models.StudentActivityContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentActivityContentRepository.java
 * Description : Repository for students' content in an activity
 */
@Repository
public interface StudentActivityContentRepository extends JpaRepository<StudentActivityContent, Long> {
}

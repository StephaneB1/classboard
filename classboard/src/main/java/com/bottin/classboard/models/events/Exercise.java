package com.bottin.classboard.models.events;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.StudentAnswer;
import com.bottin.classboard.models.events.activities.ActivityVideo;
import com.bottin.classboard.models.events.exercises.ExerciseMultipleChoice;
import com.bottin.classboard.models.events.exercises.ExerciseTrueFalse;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Exercise.java
 * Description : Model for exercises in a lesson
 */
@Entity
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public abstract class Exercise extends Event {

    private ExerciseType exerciseType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "exercise", orphanRemoval = true)
    private List<StudentAnswer> answers;

    public abstract String getSolutionResponse();

    public Exercise(Lesson lesson, ExerciseType type) {
        super(lesson, EventType.EXERCISE);
        this.exerciseType = type;
    }

    public void addNewAnswer(StudentAnswer answer) {
        this.answers.add(answer);
    }

    public int getTotalAnswerMatch(String match) {
        int result = 0;

        for(StudentAnswer answer : answers) {
            if(answer.getAnswer().equals(match))
                result++;
        }

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Exercise)) return false;
        return this.getId() != null && this.getId().equals(((Exercise) o).getId());
    }

    @Override
    public List<Log> getLogs() {
        List<Log> logs = new ArrayList<>();

        // Start
        logs.add(new Log(getStartDate(),
                "EXERCISE STARTED",
                "Exercise " + getExerciseType().getTitle() + " started",
                "var(--creationLogColor)"));

        // Student answers
        for(StudentAnswer answer : answers) {
            String exDetails = "";
            switch(exerciseType) {
                case TRUE_FALSE:
                    exDetails = ((ExerciseTrueFalse) answer.getExercise()).getQuestion();
                    break;
                case MULTIPLE_CHOICE:
                    exDetails = ((ExerciseMultipleChoice) answer.getExercise()).getQuestion();
                    break;
            }
            logs.add(new Log(answer.getCreatedDate(),
                    "STUDENT ANSWER",
                    "Student " + answer.getStudent().getFullName() +
                            " answered to <" + exDetails + "> with <" + answer.getAnswer() + ">",
                    "var(--creationLogColor)"));
        }

        // End
        if(getEndDate() != null) {
            logs.add(new Log(getStartDate(),
                    "EXERCISE ENDED",
                    "Exercise " + getExerciseType().getTitle() + " ended",
                    "var(--endExerciseLogColor)"));
        }

        return logs;
    }
}

package com.bottin.classboard.controllers;

import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.LessonCalendarDay;
import com.bottin.classboard.models.Teacher;
import com.bottin.classboard.services.interfaces.TeacherService;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : ProfileController.java
 * Description : Controller for the teacher's profile page (lessons manager)
 */
@Controller
@SessionAttributes("loggedInUsername")
public class ProfileController {

    private final TeacherService teacherService;
    private final PasswordEncoder encoder;
    private final AuthenticationProvider authProvider;

    @Autowired
    public ProfileController(TeacherService teacherService, PasswordEncoder encoder,
                             AuthenticationProvider authProvider) {
        this.teacherService = teacherService;
        this.encoder = encoder;
        this.authProvider = authProvider;
    }

    @GetMapping("/profile")
    public String profile(Model model, HttpSession session) {

        try {
            String username = (String) session.getAttribute("loggedInUsername");
            Teacher teacher = teacherService.findByUsername(username);

            // Get the month to display the lessons
            LocalDateTime now = LocalDateTime.now();

            // Generate the calendar days for the current month
            List<LessonCalendarDay> calendarDays = new ArrayList<>();
            LocalDateTime firstOfMonth = now.withDayOfMonth(1);
            int firstDayOfWeek = firstOfMonth.getDayOfWeek().getValue();
            int totalDaysInMonth = now.getMonth().length(now.toLocalDate().isLeapYear());
            for(int i = 1; i <= 35; i++) {
                int day = i - firstDayOfWeek + 1;
                calendarDays.add(new LessonCalendarDay(day,
                        day > 0 && day <= totalDaysInMonth));
            }

            // Add the lesson for the month
            for(Lesson lesson : teacher.getLessons())
                if(lesson.getCreatedDate().getMonth() == now.getMonth())
                    calendarDays.get(firstDayOfWeek - 2 + lesson.getCreatedDate().getDayOfMonth()).addLesson(lesson);

            model.addAttribute("teacher", teacher);
            model.addAttribute("month", now.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()));
            model.addAttribute("calendarDays", calendarDays);

            // Welcome text
            int hourNow = now.getHour();

            if(hourNow < 12) {
                model.addAttribute("welcomeText", "Good morning");
            } else if (hourNow < 18) {
                model.addAttribute("welcomeText", "Good afternoon");
            } else {
                model.addAttribute("welcomeText", "Good evening");
            }

        } catch (Exception e) {
            model.addAttribute("message", "The profile couldn't be loaded");
            e.printStackTrace();
            return "error";
        }

        return "profile";
    }

    @GetMapping("/register")
    public String register(Model model) {
        return "register";
    }

    @PostMapping("/register")
    public String register(@RequestParam(name="username") String username,
                           @RequestParam(name="password") String password,
                           @RequestParam(name="confirmPassword") String confirmPassword,
                           @RequestParam(name="firstName") String firstName,
                           @RequestParam(name="lastName") String lastName,
                           Model model) {

        if(username.isEmpty()){
            model.addAttribute("error", "No username");
            return "register";
        }

        if(!password.equals(confirmPassword)){
            model.addAttribute("error", "Passwords don't match");
            return "register";
        }

        if(password.length() < 8) {
            model.addAttribute("error", "Password need to have at least 8 characters");
            return "register";
        }

        // Check if username exists already
        boolean alreadyExists = false;
        try {
            teacherService.findByUsername(username);
            alreadyExists = true;
        } catch (Exception ignored) {}

        if(alreadyExists) {
            model.addAttribute("error", "This username is already used.");
            return "register";
        }

        // Create new teacher with encoded password & save to database
        try {
            teacherService.save(new Teacher(username, encoder.encode(password), firstName, lastName));
            return "redirect:/login";
        } catch (Exception e){
            model.addAttribute("message", "The teacher couldn't be saved");
            return "error";
        }
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    // Login
    @PostMapping({"/login"})
    public String login(@RequestParam(name="username") String username,
                        @RequestParam(name="password") String password,
                        Model model) {

        try {
            if(encoder.matches(password, teacherService.findByUsername(username).getPassword())) {
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
                Authentication auth = authProvider.authenticate(token);
                SecurityContext sc = SecurityContextHolder.getContext();
                sc.setAuthentication(auth);
                model.addAttribute("loggedInUsername", username);
                return "redirect:/profile";
            } else {
                model.addAttribute("error", "Wrong password or username");
            }
        } catch (Exception e) {
            model.addAttribute("error", "Wrong password or username");
        }

        return "login";
    }
}

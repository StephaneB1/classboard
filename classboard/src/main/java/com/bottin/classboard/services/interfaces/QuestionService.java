package com.bottin.classboard.services.interfaces;

import com.bottin.classboard.models.Question;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : QuestionService.java
 * Description : Service interface for questions
 */
@Service
public interface QuestionService {

    // Creation

    Question save(Question u) throws Exception;

    // Update

    Question update(Question u) throws Exception;

    // Search

    Question findById(Long id) throws Exception;

    List<Question> findAll();

    // Deletion

    void delete(Long id) throws Exception;

    void deleteAll();
}

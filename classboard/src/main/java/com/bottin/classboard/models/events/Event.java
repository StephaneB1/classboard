package com.bottin.classboard.models.events;

import com.bottin.classboard.models.Communicable;
import com.bottin.classboard.models.Lesson;
import com.bottin.classboard.models.logs.Log;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : Event.java
 * Description : Model for events in a lesson
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data // toString, equals, hashCode, getters and setters.
@NoArgsConstructor
public abstract class Event implements Communicable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @UpdateTimestamp
    private LocalDateTime lastUpdatedDate;
    @CreationTimestamp
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lesson lesson;

    EventType type;

    public Event(Lesson lesson, EventType type) {
        this.lesson = lesson;
        this.type = type;
    }

    public boolean isOnGoing() {
        return endDate == null;
    }

    public abstract List<Log> getLogs();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        return this.id != null && this.id.equals(((Event) o).getId());
    }

    @Override
    public String toString() {
        return "Event" + id;
    }
}

package com.bottin.classboard.seeder;

import com.bottin.classboard.models.Student;
import com.bottin.classboard.repositories.StudentRepository;
import com.bottin.classboard.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : DatabaseSeeder.java
 * Description : Seeder (pre-population) for classboard's database
 */
@Component
public class DatabaseSeeder {

    private final StudentRepository studentRepository;

    @Autowired
    public DatabaseSeeder(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seed_adjectives();
        seed_animals();
        seed_anon_student();
    }

    private void seed_adjectives() {

        // Resource file of all genres (cleaned and organized)
        InputStream adjectives;
        try {
            adjectives = new ClassPathResource("static/adjectives.txt").getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(adjectives, StandardCharsets.UTF_8));

            // Save each animal separately from the reader
            String adjective;
            while ((adjective = br.readLine()) != null) {
                Utils.addNewAdjective(adjective);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void seed_animals() {

        // Resource file of all genres (cleaned and organized)
        InputStream animals;
        try {
            animals = new ClassPathResource("static/animals.txt").getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(animals, StandardCharsets.UTF_8));

			// Save each animal separately from the reader
			String animal;
			while ((animal = br.readLine()) != null) {
				Utils.addNewAnimal(animal);
			}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void seed_anon_student() {
        Student jason = new Student();
        jason.setId("ANONTOKENID");
        jason.setFirstName("Jason");
        jason.setLastName("AI");
        studentRepository.save(jason);
    }
}

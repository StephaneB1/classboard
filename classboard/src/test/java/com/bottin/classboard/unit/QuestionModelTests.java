package com.bottin.classboard.unit;

import com.bottin.classboard.models.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class QuestionModelTests {

    @Test
    public void canCreateAQuestion() {
        Question question = new Question(new Lesson(), new Student(),
                "You want answers? You want answers?", LocalDateTime.now(),
                false);

        assertEquals("You want answers? You want answers?", question.getQuestion());
    }
}

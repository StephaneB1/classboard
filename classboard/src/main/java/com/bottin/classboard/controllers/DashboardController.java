package com.bottin.classboard.controllers;

import com.bottin.classboard.models.*;
import com.bottin.classboard.models.Message;
import com.bottin.classboard.models.events.*;
import com.bottin.classboard.models.logs.*;
import com.bottin.classboard.services.interfaces.*;
import com.bottin.classboard.utils.ClassroomGroupsUtils;
import com.bottin.classboard.utils.LogDateComparator;
import com.bottin.classboard.utils.Utils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : DashboardController.java
 * Description : Controller for the teacher's dashboard
 */
@Controller
@SessionAttributes("loggedInUsername")
public class DashboardController {

    private final TeacherService teacherService;
    private final LessonService lessonService;
    private final StudentService studentService;
    private final QuestionService questionService;
    private final FeedbackService feedbackService;
    private final ExerciseService exerciseService;
    private final LiveboardService liveboardService;
    private final ActivityService activityService;

    @Autowired
    public DashboardController(TeacherService teacherService, LessonService lessonService,
                               StudentService studentService, QuestionService questionService,
                               FeedbackService feedbackService, ExerciseService exerciseService,
                               LiveboardService liveboardService, ActivityService activityService) {
        this.teacherService   = teacherService;
        this.lessonService    = lessonService;
        this.studentService   = studentService;
        this.questionService  = questionService;
        this.feedbackService  = feedbackService;
        this.exerciseService  = exerciseService;
        this.liveboardService = liveboardService;
        this.activityService  = activityService;
    }

    @GetMapping("/dashboards/{lesson_id}")
    public String classDashboard(@PathVariable Long lesson_id,
                                 @RequestParam(required = false) String tab,
                                 Model model, HttpSession session) {

        try {
            // Get the lesson and the logged in teacher
            String username = (String) session.getAttribute("loggedInUsername");
            Lesson lesson = lessonService.findById(lesson_id);

            if(!lesson.getTeacher().getUsername().equals(username)) {
                model.addAttribute("message", "The connected user is not the teacher");
                return "error";
            }

            // Select the dashboard tab
            String selectedTab = "Home";
            if(tab != null && !tab.isEmpty()) {
                selectedTab = tab;
            }
            model.addAttribute("tab", selectedTab);

            // Lesson
            model.addAttribute("lesson", lesson);
            model.addAttribute("logs", getLogs(lesson));
            model.addAttribute("studentGroups", ClassroomGroupsUtils.getStudentGroups(lesson));

            // Questions
            model.addAttribute("questions", getOngoingQuestions(lesson));

            // Current event
            Event event = lesson.getOngoingEvent();
            if(event != null) {
                model.addAttribute("event", event);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "dashboard";
    }

    @PostMapping("/dashboards")
    public String createNewClass(@RequestParam String subject,
                                 @RequestParam int endHour,
                                 @RequestParam int endMinute,
                                 Model model, HttpSession session) {
        Lesson lesson = null;
        try {
            String username = (String) session.getAttribute("loggedInUsername");
            Teacher teacher = teacherService.findByUsername(username);
            LocalDateTime endTime = LocalDate.now().atTime(endHour, endMinute);
            lesson = lessonService.save(new Lesson(teacher, subject, endTime));

            model.addAttribute("lesson", lesson);
        } catch (Exception e) {
            model.addAttribute("message", "The lesson couldn't be created.");
            e.printStackTrace();
        }

        return lesson == null ? "error" : "redirect:/dashboards/" + lesson.getId();
    }

    @GetMapping("/dashboards/{lesson_id}/liveboards/{liveboard_id}")
    public String classLiveboard(@PathVariable Long lesson_id,
                                 @PathVariable Long liveboard_id,
                                 Model model, HttpSession session) {

        try {
            String username = (String) session.getAttribute("loggedInUsername");
            Lesson lesson = lessonService.findById(lesson_id);
            Liveboard board = liveboardService.findById(liveboard_id);

            if(!lesson.getTeacher().getUsername().equals(username)) {
                model.addAttribute("message", "The connected user is not the teacher");
                return "error";
            }

            model.addAttribute("board", board);
            model.addAttribute("lesson", lesson);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "liveboard";
    }

    @PostMapping("/dashboards/{lesson_id}/liveboards")
    public String classLiveboard(@PathVariable Long lesson_id,
                                 Model model, HttpSession session) {

        Lesson lesson = null;
        Liveboard liveboard = null;

        try {
            lesson = lessonService.findById(lesson_id);
            liveboard = liveboardService.save(new Liveboard(lesson));
        } catch (Exception e) {
            model.addAttribute("message", "The live board couldn't be created");
            e.printStackTrace();
        }

        return (liveboard == null || lesson == null) ? "error" :
                "redirect:/dashboards/" + lesson.getId() + "/liveboards/" + liveboard.getId();
    }

    @MessageMapping("/messages/{lesson_id}")
    @SendTo("/lesson/teacher")
    public Message message(@DestinationVariable Long lesson_id, Message message) throws Exception {
        System.out.println("Received a message of type [" + message.getType() + "] : " + message.getContent());

        // Message data
        Lesson lesson           = lessonService.findById(lesson_id);
        String senderId         = message.getSender_id();
        JSONObject content      = new JSONObject(message.getContent());
        LocalDateTime sendDate  = message.getDate();
        Student student;

        // Response data
        Message response = Utils.getBasicResponse(message);
        JSONObject responseJson = new JSONObject();

        switch(message.getType()) {
            case SWITCH_ACCESS:
                // The teacher changed the access of the lesson
                boolean currentAccess = lesson.isOpen();
                lesson.setOpen(!currentAccess);

                lessonService.update(lesson);
                response.setContent(lesson.isOpen() ? "true" : "false");
                break;
            case STUDENT_JOINED:
                // A student joined or left the lesson
                student = studentService.findById(senderId);

                // Response parameters
                String studentName = student.getFirstName() + " " + student.getLastName();
                response.setContent(studentName);
                break;
            case QUESTION_CONFIRMATION:
                // Student confirmation for an answer to their question
                if(content.getBoolean("understood")) {
                    Question questionConfirmed = questionService.findById(content.getLong("question_id"));
                    questionConfirmed.setAnswered(true);
                }

                response.setContent(content.toString());
                break;
            case FEEDBACK:
                // A student or the app (for example interactivity when
                // starting an exercise) gave a feedback
                student = studentService.findById(senderId == null ? "ANONTOKENID" : senderId);

                // Parse the content to get the feedback data
                String type = content.getString("type");
                boolean sign = content.getString("sign").equals("pos");

                // Save the feedback in the database
                feedbackService.save(new Feedback(lesson, student, FeedbackType.valueOf(type), sign));
                break;
        }

        return response;
    }

    @GetMapping("/dashboards/{lesson_id}/logs")
    @ResponseBody
    public void downloadRawLogs(@PathVariable long lesson_id, HttpServletResponse resp) {
        StringBuilder logs= new StringBuilder();

        try {
            Lesson lesson = lessonService.findById(lesson_id);

            for(Log log : getLogs(lesson)) {
                String format = log.getDate() + "[" + log.getType() + "] : " + log.getDescription();
                logs.append(format).append('\n');
            }

            OutputStream out = resp.getOutputStream();
            resp.setContentType("text/plain; charset=utf-8");
            resp.addHeader("Content-Disposition","attachment; filename=\"" + LocalDateTime.now() + "_" + lesson.getSubject() + ".txt\"");
            out.write(logs.toString().getBytes(StandardCharsets.UTF_8));
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getOngoingQuestions(Lesson lesson) {

        JSONArray questions = new JSONArray();
        for(Question question : lesson.getQuestions()) {
            if(!question.isAnswered()) {
                JSONObject questionJson = new JSONObject();
                questionJson.put("question", question.getQuestion());
                questionJson.put("question_id", question.getId());
                questionJson.put("student_id", question.getStudent().getId());
                questionJson.put("name", question.isAnonymous() ?
                        "Anonymous" : question.getStudent().getFullName());
                questionJson.put("timestamp", Utils.getTimeLabelFrom(LocalDateTime.now(),
                        question.getSendDate(), true));
                questions.put(questionJson);
            }
        }

        return questions.toString();
    }

    private List<Log> getLogs(Lesson lesson) {
        List<Log> result = new ArrayList<>();

        // Lesson creation log
        result.add(new LessonCreationLog(lesson));

        // Todo : add lesson opening and closings

        // Student joining
        for(Student student : lesson.getStudents())
            result.add(new StudentJoinLog(student));

        // Events
        for(Event event : lesson.getEvents())
            result.addAll(event.getLogs());

        // Questions
        for(Question question : lesson.getQuestions()) {
            result.add(new NewQuestionLog(question));
            if(question.getAnswer() != null && !question.getAnswer().isEmpty())
                result.add(new QuestionAnsweredLog(question));
        }

        // Feedbacks
        for(Feedback feedback : lesson.getFeedbacks())
             result.add(new FeedbackLog(feedback));

        // Order them in chronological order
        result.sort(new LogDateComparator());

        return result;
    }
}


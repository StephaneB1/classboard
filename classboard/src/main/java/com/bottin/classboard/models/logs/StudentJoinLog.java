package com.bottin.classboard.models.logs;

import com.bottin.classboard.models.Student;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : StudentJoinLog.java
 * Description : Model for logging all students joining a lesson
 */
public class StudentJoinLog extends Log {
    public StudentJoinLog(Student student) {
        super(student.getCreatedDate(),
                "STUDENT JOINED",
                student.getFullName() + " joined.",
                "var(--studentJoinedLogColor)");
    }
}

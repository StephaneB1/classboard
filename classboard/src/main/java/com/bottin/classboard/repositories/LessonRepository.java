package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LessonRepository.java
 * Description : Repository for lessons
 */
@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {
    @Query("SELECT l FROM Lesson l WHERE l.teacher = ?1")
    List<Lesson> findByUser(String username);

    @Query("SELECT l FROM Lesson l WHERE l.entryEndpoint = ?1")
    Optional<Lesson> findByEndpoint(String endpoint);

    @Query("SELECT CASE WHEN COUNT(l) > 0 THEN TRUE ELSE FALSE END FROM Lesson l WHERE l.entryCode = ?1")
    boolean doesCodeExist(String code);

    @Query("SELECT CASE WHEN COUNT(l) > 0 THEN TRUE ELSE FALSE END FROM Lesson l WHERE l.entryEndpoint = ?1")
    boolean doesEndpointExist(String endpoint);

    @Query("SELECT s.firstName, s.lastName FROM Student s WHERE s.lesson.id = ?1")
    List<String> getAttendanceList(Long id);
}

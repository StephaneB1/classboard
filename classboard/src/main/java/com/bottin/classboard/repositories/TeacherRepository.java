package com.bottin.classboard.repositories;

import com.bottin.classboard.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : TeacherRepository.java
 * Description : Repository for teachers
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String> {
}

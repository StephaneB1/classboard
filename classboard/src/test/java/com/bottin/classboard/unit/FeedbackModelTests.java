package com.bottin.classboard.unit;

import com.bottin.classboard.models.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FeedbackModelTests {

    // Test data
    private static final Teacher teacher = new Teacher("John123", "123", "John", "Doe");
    private static final Lesson lesson = new Lesson(teacher, "Algorithms", LocalDateTime.now().plusHours(1));

    @Test
    public void canCreateAFeedback() {
        Feedback feedback = new Feedback(lesson, new Student(),
                FeedbackType.MOTIVATION, true);

        assertEquals(feedback.getType(), FeedbackType.MOTIVATION);
    }
}

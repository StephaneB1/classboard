package com.bottin.classboard.services.implementations;

import com.bottin.classboard.models.Question;
import com.bottin.classboard.repositories.QuestionRepository;
import com.bottin.classboard.services.interfaces.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : QuestionServiceImpl.java
 * Description : Service implementation for questions
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    // Repositories
    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question save(Question q) throws Exception {

        if(q.getQuestion().isEmpty())
            throw new Exception("Question can't be empty");

        return questionRepository.save(q);
    }

    @Override
    public Question update(Question q) throws Exception {
        if(questionRepository.findById(q.getId()).isEmpty())
            throw new Exception("Question <" + q.getId() + "> doesn't exist.");

        return questionRepository.save(q);
    }

    @Override
    public Question findById(Long id) throws Exception {
        if(questionRepository.findById(id).isEmpty())
            throw new Exception("Question <" + id + "> doesn't exist.");

        return questionRepository.findById(id).get();
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public void delete(Long id) throws Exception {
        if(questionRepository.findById(id).isEmpty())
            throw new Exception("Question <" + id + "> doesn't exist.");

        questionRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        questionRepository.deleteAll();
    }

}

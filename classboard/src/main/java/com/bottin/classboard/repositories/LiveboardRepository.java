package com.bottin.classboard.repositories;

import com.bottin.classboard.models.events.Liveboard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ==========================================================================
 * = HEIG-VD, BACHELOR THESIS OF 2021                                       =
 * ==========================================================================
 * Subject     : enhancing online teaching through hyper interactive
 *               website application
 * Author      : Stéphane Bottin
 * Supervisor  : Marcel Graf
 *
 * File        : LiveboardRepository.java
 * Description : Repository for liveboards
 */
@Repository
public interface LiveboardRepository extends JpaRepository<Liveboard, Long> {
}
